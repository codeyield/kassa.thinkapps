<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reward extends CI_Controller {

// Токен безопасности для отправки форм
protected static $csrf;

// Используемые параметры конфига
protected static $twigconfig;
protected static $alerts;
protected static $resp;

protected static $thanks_url;
protected static $reward;
protected static $promocode;


    public function __construct() {
        parent::__construct();

        // CSRF токен безопасности для пользовательских форм
        self::$csrf = array('name' => $this->security->get_csrf_token_name(), 'hash' => $this->security->get_csrf_hash());
        
        $CI =& get_instance();
        $base_url = $CI->config->item('base_url');    // Значение из глобального конфига

        // Загрузка кастомного конфига и необходимых параметров
        $CI->config->load('ukonfig', TRUE);
        $ukonfig = $CI->config->item('ukonfig');

        self::$twigconfig = $ukonfig['twigconfig'];
        self::$alerts     = $ukonfig['alerts'];
        self::$resp       = $ukonfig['resp'];
        
        self::$thanks_url = $base_url . $ukonfig['thanks']['url'];
        self::$reward     = $ukonfig['reward'];
        self::$promocode  = $ukonfig['promocode'];

        // Получаем параметры запуска приложения от ВК или из сессии
        $this->load->model('vkauth');
        $this->vkauth->get();
        
        // Тарифы и биллинг загружаем всегда, даже если биллинг выключен
        $this->load->model('plan');
        $this->load->model('billing');
    }


	/**
	 * http://thinkapps.kassa/reward
	 */
	public function index() {
        
        // Проверка на режим обслуживания до загрузок основных моделей и запросов к БД
        if (MAINTENANCE) {
            header("Location:/");       // Редирект на главную
            exit;                       // Принудительное завершение работы фреймворка
        }
        
        $this->load->model('settings');
        $settings = $this->settings->get();
        
        // Проверяем наступление даты оплаты, продляем тариф, списываем сумму за платный тариф
        if (BILLING && $this->vkauth->is_valid_gid() && $this->settings->is_installed())
            $this->plan->update_auto();
        
        // Пройдена авторизация ВК и юзер является админом сообщества (если есть админ, то gid заведомо валиден)
        if ($this->vkauth->is_auth && $this->vkauth->is_admin) {

            $this->load->library('twig', self::$twigconfig);
            $this->load->library('alert');
            
            $alerts = $this->alert->add(
                // Напоминание о комиссиях при разных способах оплаты
                self::$alerts['reward_attn_fee'],
                // Предупреждение о наличии незачисленных оплат (в т.ч. отправленных с протекцией)
                ($this->billing->has_unaccepted() ? self::$alerts['reward_has_unaccepted'] : NULL)
            );

            // Метка платежа в формате 'gid_uid' для отправки с формой оплаты. Парсится в контроллере /income/yamoney
            $label = $this->vkauth->gid .'_'. $this->vkauth->uid;

            $this->twig->display('reward', array(
                'environment' => ENVIRONMENT,
                'billing'     => BILLING,
                'vkdata'      => $this->vkauth->get(),
                'settings'    => $settings, 
                'balance'     => $this->billing->get_amount(),
                'nextprice'   => $this->plan->get_next_price(),
                'label'       => $label,
                'thanks_url'  => self::$thanks_url,
                'reward'      => self::$reward,
                'promocode'   => self::$promocode,
                'csrf'        => self::$csrf,
                'alerts'      => $alerts
            ));
        }
        // Авторизация пройдена, но юзер не админ
        elseif ($this->vkauth->is_auth) {
            header("Location:/");       // Редирект на главную
            exit;                       // Принудительное завершение работы фреймворка
        }
    
        // Если не пройдена авторизация ВК - ничего не выводим в браузер
	}
    
    
	/**
	 * Ajax контроллер для проверки и погашения промо-кода
	 * @return HTTP 200 JSON-массив ответа в формате: 
	 *  boolean $resp['response'] Успешно (при наличии этого поля)
	 *  boolean $resp['error']    Ошибка (при наличии этого поля)
	 *  string  $resp['text']     Текст сообщения для показа юзеру
	 */
	public function push_promo() {
        
        if (MAINTENANCE OR !$this->input->is_ajax_request() OR !$this->vkauth->is_auth)
            die();

        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('codename', '', "required|min_length[".self::$promocode['minlen']."]|max_length[".self::$promocode['maxlen']."]");
        
        $resp = [];
        if ($this->form_validation->run()) {
            
            // Обновляем настройки тарифа для сообщества
            $this->load->model('promo');
            $result = $this->promo->push($this->input->post('codename'));
            
            $resp = $this->promo->resp;  // Модель сохраняет в это свойство массив ответа или массив ошибки
            
            // Дополняем $resp данными, которые необходимо обновить на странице
            if ($result) {
                $resp['nextprice'] = $this->plan->get_next_price();     // Стоимость оплаты тарифа на следующий период
                $resp['balance']   = $this->billing->get_amount();      // Сумма на балансе
            }
        }
        else
            // Нет такого промо-кода или срок его действия истёк
            $resp = self::$resp['prerr_noexists'];
        
        // Отправляем строку ответа в ajax-скрипт
        echo json_encode($resp);
    }
    
}