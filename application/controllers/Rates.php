<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rates extends CI_Controller {

// Токен безопасности для отправки форм
protected static $csrf;

// Используемые параметры конфига
protected static $twigconfig;
protected static $alerts;
protected static $resp;

protected static $plansheet;


    public function __construct() {
        parent::__construct();

        // CSRF токен безопасности для пользовательских форм
        self::$csrf = array('name' => $this->security->get_csrf_token_name(), 'hash' => $this->security->get_csrf_hash());

        // Загрузка кастомного конфига и необходимых параметров
        $CI =& get_instance();
        $CI->config->load('ukonfig', TRUE);
        $ukonfig = $CI->config->item('ukonfig');

        self::$twigconfig  = $ukonfig['twigconfig'];
        self::$alerts      = $ukonfig['alerts'];
        self::$resp        = $ukonfig['resp'];

        self::$plansheet   = $ukonfig['plans']['sheet'];

        // Получаем параметры запуска приложения от ВК или из сессии
        $this->load->model('vkauth');
        $this->vkauth->get();
        
        // Модель с тарифами загружаем вне зависимости от состояния биллинга
        $this->load->model('plan');
        $this->load->model('billing');
    }


	/**
	 * http://thinkapps.kassa/rates
	 */
	public function index() {
        
        // Проверка на режим обслуживания до загрузок основных моделей и запросов к БД
        if (MAINTENANCE) {
            header("Location:/");       // Редирект на главную
            exit;                       // Принудительное завершение работы фреймворка
        }
        
        $this->load->model('settings');
        $settings = $this->settings->get();
        
        // Проверяем наступление даты оплаты, продляем тариф, списываем сумму за платный тариф
        if (BILLING && $this->vkauth->is_valid_gid() && $this->settings->is_installed())
            $this->plan->update_auto();
        
        // Пройдена авторизация ВК и юзер является админом сообщества (если есть админ, то gid заведомо валиден)
        if ($this->vkauth->is_auth && $this->vkauth->is_admin) {

            $this->load->library('twig', self::$twigconfig);
            $this->load->library('alert');
            
            $this->twig->display('rates', array(
                'environment' => ENVIRONMENT,
                'billing'     => BILLING,
                'vkdata'      => $this->vkauth->get(),
                'settings'    => $settings, 
                'balance'     => $this->billing->get_amount(),
                'plansheet'   => self::$plansheet,
                'planset'     => $this->plan->get_planset(),
                'nextprice'   => $this->plan->get_next_price(),
                'trial_id'    => $this->plan->trial_id(),
                'csrf'        => self::$csrf,
                //'alerts'      => $alerts
            ));
        }
        // Авторизация пройдена, но юзер не админ
        elseif ($this->vkauth->is_auth) {
            header("Location:/");       // Редирект на главную
            exit;                       // Принудительное завершение работы фреймворка
        }
    
        // Если не пройдена авторизация ВК - ничего не выводим в браузер
	}
    
    
	/**
	 * Ajax контроллер обработки формы с выбором тарифа
	 * @return HTTP 200 JSON-массив ответа в формате: 
	 *  boolean $resp['response']    Успешно
	 *  boolean $resp['error']       Ошибка
	 *  string  $resp['text']        Текст сообщения для показа юзеру
	 */
	public function save_plan() {
        
        if (MAINTENANCE OR !$this->input->is_ajax_request() OR !$this->vkauth->is_auth)
            die();

        $this->load->library('form_validation');

        // Строка со списком кодов всех тарифов, КРОМЕ триального
        $plan_ids = implode(',', $this->plan->get_all_ids_exclude_trial());
        
        $this->form_validation->set_rules('plan', '', "required|in_list[{$plan_ids}]|max_length[25]");
        
        $resp = [];
        if ($this->form_validation->run()) {
            
            // Обновляем настройки тарифа для сообщества
            $result = $this->plan->update_by_user($this->input->post('plan'));
            
            $resp = $this->plan->resp;  // Модель отдаёт в resp корректный ответ с описанием ошибки
            
            // Дополняем resp другими данными, которые необходимо обновить на странице
            if ($result) {
                $resp['planset']   = $this->plan->get_planset();        // Данные обновлённого тарифа
                $resp['nextprice'] = $this->plan->get_next_price();     // Стоимость оплаты тарифа на следующий период
                $resp['balance']   = $this->billing->get_amount();      // Сумма на балансе
            }
        }
        else
            $resp = self::$resp['error_something'];
        
        // Отправляем строку ответа в ajax-скрипт
        echo json_encode($resp);
    }
    
    
	/**
	 * Ajax контроллер обработки формы, возвращающий данные для модального окна подтверждения юзеру о смене тарифа
	 * @return HTTP 200 JSON-массив ответа в формате: 
	 *  boolean $data['confirm'] TRUE - диалог ДА/ОТМЕНА; FALSE - окно с кнопкой ОК.
	 *  string  $data['text']    Текст сообщения в модальном окне для подтвеждения юзеру
	 */
	public function confirm_plan() {
        
        if (MAINTENANCE OR !$this->input->is_ajax_request() OR !$this->vkauth->is_auth)
            die();

        $this->load->library('form_validation');

        // Строка со списком кодов всех тарифов, КРОМЕ триального
        $plan_ids = implode(',', $this->plan->get_all_ids_exclude_trial());
        
        $this->form_validation->set_rules('plan', '', "required|in_list[{$plan_ids}]|max_length[25]");
        
        if ($this->form_validation->run()) {
            
            // Получаем массив с данными для подтверждения операции юзеру
            $data = $this->plan->confirm_plan($this->input->post('plan'));

            // Отправляем строку ответа в ajax-скрипт
            echo json_encode($data);
        }
        
        // При невалидных данных ошибку выведет та часть ajax-скрипта, которая обрабатывает ответ от контроллера rates/save_plan
    }
    
    
    /**
     * ОТЛАДОЧНАЯ ЗАГЛУШКА!
     * Заменяет в БД текущий и следующий тариф случайным валидным значением для отладки
     */
    public function debug() {
        $plans = array_keys(self::$plansheet);
        $keys = array_rand($plans, 2);

        $this->db
            ->set(array('plan' => $plans[$keys[0]], 'nextplan' => $plans[$keys[1]]))
            ->where('gid', $this->vkauth->gid)
            ->update('settings');
        
        echo "Now plan: ".$plans[$keys[0]]."\r\n<br>Next plan: ".$plans[$keys[1]];
    }
    
}