<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

// Используемые параметры конфига
protected static $twigconfig;
protected static $alerts;

protected static $affprog;
protected static $thanks_url;


    public function __construct() {
        parent::__construct();
        
        $CI =& get_instance();
        $base_url = $CI->config->item('base_url');    // Значение из глобального конфига

        // Загрузка кастомного конфига и необходимых параметров
        $CI->config->load('ukonfig', TRUE);
        $ukonfig = $CI->config->item('ukonfig');

        self::$twigconfig  = $ukonfig['twigconfig'];
        self::$alerts      = $ukonfig['alerts'];

        self::$affprog     = $ukonfig['affprog'];
        self::$thanks_url  = $base_url . $ukonfig['thanks']['url'];

        // Получаем параметры запуска приложения от ВК или из сессии
        $this->load->model('vkauth');
        $this->vkauth->get();
    }
    
    
	/**
	 * http://thinkapps.kassa/?api_id=0&api_url=0&api_result=0&sign=0&access_token=0&referrer=unknown&viewer_id=55555&group_id=11111&viewer_type=4
	 * viewer_type=1  - Запуск как юзер
	 * viewer_type=4  - Запуск как админ
	 */
	public function index() {
        
        // Проверку на режим обслуживания выполняем до загрузок моделей и запросов к БД
        if (MAINTENANCE) {
            echo $this->load->view('maintenance.html', NULL, TRUE);
            exit;           // Принудительное завершение работы фреймворка
        }
        
        $this->load->model('settings');
        $settings = $this->settings->get(TEXT_RESTORE_HTML);
        
        $this->load->model('tabs');
        $tabs = $this->tabs->get(TEXT_RESTORE_HTML);
        
        // Если биллинг включен и приложение установлено в сообществе
        if (BILLING && $this->vkauth->is_valid_gid() && $this->settings->is_installed()) {
            $this->load->model('plan');
            $this->plan->update_auto();     // Проверяем наступление даты оплаты, продляем тариф, списываем сумму за платный тариф
            $tabs = $this->tabs->limit_by_plan(TEXT_RESTORE_HTML);  // Ограничиваем число и параметры вкладок по текущему тарифу
        }

        // Пройдена авторизация ВК
        if ($this->vkauth->is_auth) {
            
            $this->load->library('twig', self::$twigconfig);
            $this->twig->addGlobal('environment', ENVIRONMENT);
            $this->twig->addGlobal('billing', BILLING);
            $this->load->library('alert');

            // Приложение запущено не со страницы сообщества или не установлено в этом сообществе
            if (!$this->vkauth->is_valid_gid() || !$this->settings->is_installed()) {
                $this->twig->display('promo', array(
                    'vkdata' => $this->vkauth->get(),
                ));
            }
            // Приложение запущено юзером со страницы сообщества, но выключено его админом
            elseif ($this->vkauth->is_valid_gid() && !$this->vkauth->is_admin && !$this->settings->is_appon()) {
                $this->twig->display('trouble', array(
                    'alerts' => array(self::$alerts['appoff'])
                ));
            }
            // Нормальное использование приложения по назначению юзером или админом (как превью-режим)
            elseif ($this->vkauth->is_valid_gid()) {
                $alerts = [];
                $this->load->is_loaded('plan') OR $this->load->model('plan');

                // Рекламу приложения показываем на бесплатных тарифах
                $planset = $this->plan->get_planset();
                $advertise = ($planset['price'] == 0);
                
                // АДМИНУ формируем алерты с сообщениями
                if ($this->vkauth->is_admin) {
                    $alerts = $this->alert->add(
                        // Обязательный уведомление о том, включено приложение для юзеров или нет
                        ($this->settings->is_appon() ? self::$alerts['preview_appon'] : self::$alerts['preview_appoff']),
                        
                        // Оповещение, что некоторые вкладки не будут показаны юзерам при превышении тарифа
                        ((BILLING AND $this->tabs->is_exceeded($tabs)) ? self::$alerts['limited_by_plan'] : NULL)
                    );
                }
                // ОБЫЧНОМУ ЮЗЕРУ ставим реферальную куку (вне зависимости от состояния партнёрки - нужно для аналитики)
                else {
                    setcookie (self::$affprog['cookiename'], $this->vkauth->gid, time() + self::$affprog['cookietime']);
                }
                
                $this->twig->display('welcome', array(
                    'settings'   => $settings, 
                    'tabs'       => $tabs, 
                    'vkdata'     => $this->vkauth->get(),
                    'thanks_url' => self::$thanks_url,
                    'advertise'  => $advertise,
                    'alerts'     => $alerts
                ));
            }
        }

        // Если не пройдена авторизация ВК - ничего не выводим в браузер
	}

}