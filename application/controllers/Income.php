<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Income extends CI_Controller {

// Используемые параметры конфига
protected static $yasecret;
protected static $paytypes;
protected static $successemail;
protected static $emailconfig;
protected static $emailset;


    public function __construct() {
        parent::__construct();

        // Загрузка кастомного конфига и необходимых параметров
        $CI =& get_instance();
        $CI->config->load('ukonfig', TRUE);
        $ukonfig = $CI->config->item('ukonfig');

        self::$yasecret     = $ukonfig['reward']['yasecret'];
        self::$paytypes     = $ukonfig['reward']['paytypes'];
        self::$successemail = $ukonfig['reward']['success_email'];
        self::$emailconfig  = $ukonfig['email']['config'];
        self::$emailset     = $ukonfig['email']['set'];
        
        $this->load->model('billing');
    }
    

    /**
     * https://kassa.thinkapps.ru/income/yamoney
     * 
     * Контроллер HTTP-уведомлений (обработки зачисления платежа) через Яндекс.Деньги
     * @url https://money.yandex.ru/doc.xml?id=526991
     * 
	 * POST-параметры, которые передает Яндекс в HTTP-уведомлении:
     *  notification_type string   Для переводов из кошелька — p2p-incoming. Для переводов с карты — card-incoming.
     *  operation_id      string   Идентификатор операции в истории счета получателя.
     *  amount            amount   Сумма, которая зачислена на счет получателя.
     *  withdraw_amount   amount   Сумма, которая списана со счета отправителя.
     *  currency          string   Код валюты — всегда 643 (рубль РФ согласно ISO 4217).
     *  datetime          datetime Дата и время совершения перевода.
     *  sender            string   Для переводов из кошелька — № счета отправителя. Для переводов с карты — пустая строка.
     *  codepro           boolean  Для переводов из кошелька — защищен кодом протекции. Для переводов с карты — всегда false.
     *  label             string   Метка платежа. Пустая строка, если ее нет.
     *  sha1_hash         string   SHA-1 hash параметров уведомления.
     *  unaccepted        boolean  Перевод еще не зачислен. Нужно освободить место в кошельке или ввести код протекции (при codepro=true).
     * 
     * Вызов для отладки на локалхосте:
     * http://thinkapps.kassa/income/yamoney?notification_type=p2p-incoming&operation_id=test&amount=10&currency=643&datetime=2017-01-01&sender=12345&codepro=0&label=test
     * Для отладки добавить:
     * В $this->yamoney():           if ((sha1($sign) == $post['sha1_hash']) || (ENVIRONMENT == 'development')) {...}
     * В $this->yamoney():           if (ENVIRONMENT == 'development') $post = $this->input->get(NULL, FALSE);
     * В $this->isset_post_params(): if (ENVIRONMENT == 'development') $params = array_keys($this->input->get(NULL, FALSE));
     */
    public function yamoney() {

        $post = $this->input->post(NULL, FALSE);
    
        // Проверяем наличие обязательных параметров
        if ($this->isset_post_params('notification_type', 'operation_id', 'amount', 'currency', 'datetime', 'sender', 'codepro', 'label')) {

            // Собираем контрольную подпись платежа по шаблону Яндекса
            $sign = $post['notification_type'] .'&'.
                    $post['operation_id'] .'&'.
                    $post['amount'] .'&'.
                    $post['currency'] .'&'.
                    $post['datetime'] .'&'.
                    $post['sender'] .'&'.
                    $post['codepro'] .'&'.
                    self::$yasecret .'&'.
                    $post['label'];

            // Подпись валидная, зачисляем платёж
            if (sha1($sign) == $post['sha1_hash']) {
                
                // ВНИМАНИЕ, КОШЕЛЁК ПЕРЕПОЛНЕН! - отправляем уведомление разработчику
                if ($post['unaccepted'] AND !$post['codepro']) {
                    $this->load->library('email');

                    $this->email->initialize(self::$emailconfig);
                    $this->email->from(self::$emailset['purse_crowded']['from']);
                    $this->email->to(self::$emailset['purse_crowded']['to']);
                    $this->email->subject(self::$emailset['purse_crowded']['subject']);
                    $this->email->message($msg = self::$emailset['purse_crowded']['message']);
                    $this->email->send();
                    
                    log_message('error', "ATTENTION, PURSE CROWDED! Email was sent with message:\n{$msg}");
                }
                
                // Валидируем метку платежа в формате 'gid_uid'. Метка собирается в контроллере reward/index и отправляется с формой.
                // При невалидной метке платёж либо НЕ связан с оплатой приложения, либо отправлен вручную. Яндексу всё равно отвечаем HTTP 200 OK.
                if (preg_match("/^(\d{2,12})_(\d{2,12})$/", $post['label'], $match)) {
                    
                    $gid = (int) $match[1];
                    $uid = (int) $match[2];
                    
                    // Платёж пришёл с протекцией или переполнен кошелек
                    $accepted = ($post['codepro'] || $post['unaccepted']);
                    
                    // Тип платежа Яндекса приводим к значению для БД
                    $type = $post['notification_type'] == 'p2p-incoming' 
                        ? self::$paytypes['yandexmoney']
                        : ($post['notification_type'] == 'card-incoming' ? self::$paytypes['card'] : NULL);
                    
                    // Платежи с яндекс-кошельков зачисляем без комиссии, остальные - по зачисленной сумме
                    $amount = $post['notification_type'] == 'p2p-incoming' ? $post['withdraw_amount'] : $post['amount'];
                    
                    // Зачисляем сумму на баланс сообщества
                    $result = $this->billing->credit($gid, $uid, $type, $post['operation_id'], $amount, $post['datetime'], $accepted, $post['sender']);

                    // Информативная часть сообщения с данными платежа
                    $premsg = "Идентификатор платежа: {$post['operation_id']}\n".
                            "Способ оплаты: {$post['notification_type']}\n".
                            "Сумма перевода: {$post['withdraw_amount']}\n".
                            "Сумма к зачислению: {$post['amount']}\n".
                            "Отметка времени: {$post['datetime']}\n".
                            "Кошелек отправителя: {$post['sender']}\n".
                            "Протекция платежа: ".($post['codepro'] ? "Да" : "Нет");
                    
                    // Уведомление об успешной оплате/зачислении (если включен success_email в конфиге)
                    if ($result AND self::$successemail) {
                        $this->load->is_loaded('email') OR $this->load->library('email');

                        $this->email->initialize(self::$emailconfig);
                        $this->email->from(self::$emailset['success_income']['from']);
                        $this->email->to(self::$emailset['success_income']['to']);
                        $this->email->subject("Зачислен платёж для #{$gid} на сумму {$post['amount']}р.");
                        $this->email->message($msg = "Зачислен входящий платёж для сообщества #{$gid}!\n\n".$premsg);
                        $tmp = $this->email->send();
                    }
                    
                    // Уведомление об ошибке
                    if (!$result) {
                        $this->load->is_loaded('email') OR $this->load->library('email');

                        $this->email->initialize(self::$emailconfig);
                        $this->email->from(self::$emailset['error_income']['from']);
                        $this->email->to(self::$emailset['error_income']['to']);
                        $this->email->subject(self::$emailset['error_income']['subject']);
                        $this->email->message($msg = "Не удалось зачислить входящий платёж для сообщества #{$gid}! Проверьте логи!\n\n".$premsg);
                        $this->email->send();
                        
                        log_message('error', "Error accept incoming payment for gid #{$gid} and email was sent with message:\n{$msg}");
                    }
                }
                
                // Наш ответ яндексу (с валидной меткой label или без неё)
                header('HTTP/1.1 200 OK');
                header('Content-Type: text/html');
                echo 'OK';
                die;
            }
            // Невалидная подпись платежа
            else {
                log_message('error', "Yandex payment invalid sign != sha1_hash from IP ".
                            $this->input->ip_address().": ".sha1($sign)." != ".$post['sha1_hash']);
                die('BAD');
            }
        }
        // Не пришли обязательные POST-параметры
        else {
            log_message('error', "Yandex payment invalid \$_POST data from IP ".$this->input->ip_address().": ".print_r($_POST, TRUE));
            die('BAD');
        }
    }
    
    
    /**
     * Проверяет, что каждый параметр, перечисленный в аргументах ф-ции, присутствует в $_POST массиве
     */
    protected function isset_post_params() {
        
        $params  = array_keys($this->input->post(NULL, FALSE));
        $numargs = func_num_args();
        
        $k = 0;
        foreach (func_get_args() as $arg) 
            if (in_array($arg, $params))
                ++$k;
        
        return ($numargs == $k);
    }
    
}