<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setup extends CI_Controller {

// Токен безопасности для отправки форм
protected static $csrf;
    
// Используемые параметры конфига
protected static $twigconfig;
protected static $alerts;
protected static $resp;
    
protected static $yapurse;
protected static $yabutton;
protected static $yanotes;
protected static $toptext;
protected static $bottomtext;

// Данные для валидации размера текста на вкладках, включая тарифо-зависимые (если биллинг включен)
protected static $textlimits;


    public function __construct() {
        parent::__construct();

        // CSRF токен безопасности для пользовательских форм
        self::$csrf = array('name' => $this->security->get_csrf_token_name(), 'hash' => $this->security->get_csrf_hash());

        // Загрузка кастомного конфига и необходимых параметров
        $CI =& get_instance();
        $CI->config->load('ukonfig', TRUE);
        $ukonfig = $CI->config->item('ukonfig');

        self::$twigconfig  = $ukonfig['twigconfig'];
        self::$alerts      = $ukonfig['alerts'];
        self::$resp        = $ukonfig['resp'];
        
        self::$yapurse     = $ukonfig['settings']['yapurse'];
        self::$yabutton    = $ukonfig['settings']['yabutton'];
        self::$yanotes     = $ukonfig['settings']['yanotes'];
        self::$toptext     = $ukonfig['settings']['toptext'];
        self::$bottomtext  = $ukonfig['settings']['bottomtext'];

        // Получаем параметры запуска приложения от ВК или из сессии
        $this->load->model('vkauth');
        $this->vkauth->get();
        
        // Модель с тарифами загружаем вне зависимости от состояния биллинга
        $this->load->model('plan');
    }
    
    
    /**
	 * http://thinkapps.kassa/setup
	 */
	public function index() {
        
        // Проверка на режим обслуживания до загрузок основных моделей и запросов к БД
        if (MAINTENANCE) {
            header("Location:/");       // Редирект на главную
            exit;                       // Принудительное завершение работы фреймворка
        }
        
        $this->load->model('settings');
        $settings = $this->settings->get();
        
        // Если биллинг включен и приложение установлено в сообществе
        if (BILLING && $this->vkauth->is_valid_gid() && $this->settings->is_installed()) {
            $this->plan->update_auto();                     // Проверяем наступление даты оплаты, продляем тариф, списываем сумму за платный тариф
            $settings = $this->settings->limit_by_plan();   // Ограничиваем размеры текстов в настройках по текущему тарифу
        }
        
        // Пройдена авторизация ВК и юзер является админом сообщества (если есть админ, то gid заведомо валиден)
        if ($this->vkauth->is_auth && $this->vkauth->is_admin) {
            
            $this->load->library('twig', self::$twigconfig);
            $this->load->library('alert');
            
            $alerts = $this->alert->add(
                 self::$alerts['setup_newbie']               // Подсказки для тех, кто впервые использует приложение
            );
            
            $this->twig->display('setup', array(
                'environment' => ENVIRONMENT,
                'billing'     => BILLING,
                'vkdata'      => $this->vkauth->get(),
                'settings'    => $settings, 
                'yapurse'     => self::$yapurse,
                'yabutton'    => self::$yabutton,
                'yanotes'     => self::$yanotes,
                'toptext'     => self::$toptext,
                'bottomtext'  => self::$bottomtext,
                'tablimits'   => $this->plan->tablimits(),
                'csrf'        => self::$csrf,
                'alerts'      => $alerts
            ));
        }
        // Авторизация пройдена, но юзер не админ
        elseif ($this->vkauth->is_auth) {
            header("Location:/");       // Редирект на главную
            exit;                       // Принудительное завершение работы фреймворка
        }
        
        // Если не пройдена авторизация ВК - ничего не выводим в браузер
	}
    
    
	/**
	 * Ajax-вызываемый контроллер обработки формы включения/выключения приложения
	 */
	public function appon() {
        
        if (MAINTENANCE OR !$this->input->is_ajax_request() OR !$this->vkauth->is_auth) 
            die();

        $this->load->model('settings');
        $appon_before   = $this->settings->is_appon();
        $isset_yapurse  = $this->settings->isset_yapurse();
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules('appon', '', "required|max_length[1]");

        $resp = [];
        if (!$appon_before && !$isset_yapurse) {
            $resp['error'] = TRUE;
            $resp['text']  = "Нельзя включить приложение, пока не указан Яндекс.Кошелек!";
        }
        elseif ($this->form_validation->run()) {

            // Значение параметра appon не используется, достаточно того, что он был отправлен
            // Переключаем состояние приложения на обратное
            $turnresult  = $this->settings->turn_appon();
            $appon_after = $this->settings->is_appon();     // При вызове без параметров настройки перечитаются из БД
            
            if (!$turnresult || ($appon_before == $appon_after)) {
                $resp = self::$resp['error_turn'];
            }
            else {
                $resp['response'] = $appon_after;
                $resp['text'] = $appon_after ? "Приложение включено!" : "Приложение выключено!";
            }
        }
        else 
            $resp = self::$resp['error_turn'];
        
        // Отправляем строку ответа в ajax-скрипт
        echo json_encode($resp);
    }


	/**
	 * Ajax-вызываемый контроллер обработки формы с платежными настройками приложения
	 * @return HTTP 200 JSON-массив ответа в формате: 
	 *  boolean $resp['response']    Успешно
	 *  boolean $resp['error']       Ошибка
	 *  string  $resp['text']        Текст сообщения для показа юзеру
	 */
	public function save_payinfo() {
        
        if (MAINTENANCE OR !$this->input->is_ajax_request() OR !$this->vkauth->is_auth) 
            die();

        $this->load->model('settings');
        $settings = $this->settings->get();

        $this->load->library('form_validation');

        // Для облегчения читаемости правил валидации
        $ypminlen = self::$yapurse['minlen'];   $ybminlen = self::$yabutton['minlen'];
        $ypmaxlen = self::$yapurse['maxlen'];   $ybmaxlen = self::$yabutton['maxlen'];
        $yanoteskeys = implode(',', array_keys(self::$yanotes['formsetup']));
        
        $this->form_validation->set_rules('yapurse',  '', "trim|integer|min_length[{$ypminlen}]|max_length[{$ypmaxlen}]");
        $this->form_validation->set_rules('yabutton', '', "required|trim|min_length[{$ybminlen}]|max_length[{$ybmaxlen}]");
        $this->form_validation->set_rules('yanote',   '', "required|in_list[{$yanoteskeys}]|max_length[25]");
        
        $resp = [];
        if ($this->form_validation->run()) {
            
            // Обновляем настройки платежных данных сообщества в БД
            $result = $this->settings->update_payinfo(
                $this->input->post('yapurse'), 
                $this->input->post('yabutton'), 
                $this->input->post('yanote') 
            );
            
            $resp = $this->settings->resp;  // Модель отдаёт в resp корректный ответ с описанием ошибки
        }
        else
            $resp = self::$resp['error_something'];
        
        // Отправляем строку ответа в ajax-скрипт
        echo json_encode($resp);
    }


	/**
	 * Ajax-вызываемый контроллер обработки формы с общими текстами с информацией сверху и снизу формы оплаты
	 * @return HTTP 200 JSON-массив ответа в формате: 
	 *  boolean $resp['response']    Успешно
	 *  boolean $resp['error']       Ошибка
	 *  string  $resp['text']        Текст сообщения для показа юзеру
	 */
	public function save_texts() {
        
        if (MAINTENANCE OR !$this->input->is_ajax_request() OR !$this->vkauth->is_auth) 
            die();

        $this->load->model('settings');
        $settings = $this->settings->get();

        $this->load->library('form_validation');

        // Получаем тарифозависимые размеры "единых" текстов для вкладок. Или дефолтные параметры, если биллинг выключен
        $tablimits = $this->plan->tablimits();
        
        // Списки валидных значений режимов вывода текстов на вкладках
        $ttkeys = implode(',', array_keys(self::$toptext['formsetup']));
        $btkeys = implode(',', array_keys(self::$bottomtext['formsetup']));

        $this->form_validation->set_rules('toptext',    '', "trim|max_length[{$tablimits['ttmax']}]");
        $this->form_validation->set_rules('bottomtext', '', "trim|max_length[{$tablimits['btmax']}]");
        $this->form_validation->set_rules('ttmode',     '', "required|in_list[{$ttkeys}]|max_length[25]");
        $this->form_validation->set_rules('btmode',     '', "required|in_list[{$btkeys}]|max_length[25]");
        
        $resp = [];
        if ($this->form_validation->run()) {
            
            // Обновляем настройки сообщества в БД
            $result = $this->settings->update_texts(
                $this->input->post('toptext'), 
                $this->input->post('bottomtext'), 
                $this->input->post('ttmode'), 
                $this->input->post('btmode')
            );
            
            $resp = $this->settings->resp;  // Модель отдаёт в resp корректный ответ с описанием ошибки
        }
        else
            $resp = self::$resp['error_something'];
        
        // Отправляем строку ответа в ajax-скрипт
        echo json_encode($resp);
    }

}
