<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Thanks extends CI_Controller {

// Используемые параметры конфига
protected static $twigconfig;
protected static $appid;

protected static $button_app;
protected static $button_group;


    public function __construct() {
        parent::__construct();

        // Загрузка кастомного конфига и необходимых параметров
        $CI =& get_instance();
        $CI->config->load('ukonfig', TRUE);
        $ukonfig = $CI->config->item('ukonfig');

        self::$twigconfig   = $ukonfig['twigconfig'];
        self::$appid        = $ukonfig['vkapp']['appid'];
        
        self::$button_app   = $ukonfig['thanks']['button_app'];
        self::$button_group = $ukonfig['thanks']['button_group'];
    }

	/**
	 * Юзается собственный формат GET-параметров для Метрики.
	 * Параметры собираются и передаются в поле 'successURL' в шаблонах inc.payform.twig и reward.twig. 
	 * К платежной форме никакого отношения не имеют, яндексу только передается URL этого контроллера с параметрами!
	 * 
	 * http://thinkapps.kassa/thanks?userid=9674014&group=148845544
	 * Параметры при ОПЛАТЕ УСЛУГ В СООБЩЕСТВАХ: 
	 *   userid   - id плательщика
	 *   group    - id группы, в которой прошла оплата
	 *   amount   - сумма платежа
	 *   comment  - примечание к платежу
	 *   referrer - ВК реферер, с которым плательщик запустил приложение
	 *   
	 * Параметры при ОПЛАТЕ ПОДПИСКИ ПРИЛОЖЕНИЯ (при этом вызов Метрики отключается):
	 *   userid   - ОТСУТСТВУЕТ (ключевой признак)
	 *   group    - id группы, в которой пополняется баланс
	 */
	public function index() {

        // Получаем содержимое $_GET
        $get = $this->input->get(NULL, FALSE);

        // Восстанавливаем URL-кодированное поле "Примечание к платежу"
        $get['comment'] = isset($get['comment']) ? urldecode($get['comment']) : NULL;
        
        $metrikaon = TRUE;
        
        // Текст на кнопке и возвратная ссылка по умолчанию
        $button  = self::$button_app;
        $backurl = "https://vk.com/app".self::$appid;       // На промо-страницу приложения

        // Проверяем наличие валидного gid в параметрах
        if (isset($get['group']) AND preg_match("/^\d{2,12}$/", $get['group'])) {
            
            // Переопределяем текст на кнопке и возвратную ссылку при ОПЛАТЕ УСЛУГ В СООБЩЕСТВАХ
            if (isset($get['userid'])) {
                // Пробуем получить название сообщества ВК API-запросом без авторизации
                $resp = file_get_contents("https://api.vk.com/method/groups.getById?format=json&v=5.65&group_id=".((int) $get['group']));
                $resp = json_decode($resp, TRUE);
                $resp = isset($resp['response'][0]) ? $resp['response'][0] : NULL;

                // Текст на кнопке и возвратная ссылка: название сообщества + ссылка на него
                $button  = isset($resp['name']) ? $resp['name'] : self::$button_group;
                $backurl = isset($resp['screen_name']) ? "https://vk.com/".$resp['screen_name'] : "https://vk.com/club".$get['group'];

                if (is_null($resp))
                    log_message('error', "Invalid response from VK API method 'groups.get': ".print_r($resp, TRUE));
            }
            // Переопределяем текст на кнопке и возвратную ссылку при ОПЛАТЕ ПОДПИСКИ ПРИЛОЖЕНИЯ
            else {
                $metrikaon = FALSE;             // Отключаем метрику для собственных оплат
                $button    = self::$button_app;
                $backurl   = "https://vk.com/app".self::$appid."_-".$get['group'];    // На приложение, установленное в сообществе
            }
        }

        // Отображаем шаблон
        $this->load->library('twig', self::$twigconfig);

        $this->twig->display('thanks', array(
            'environment' => ENVIRONMENT,
            'billing'     => BILLING,
            'data'        => $get,
            'button'      => $button,
            'backurl'     => $backurl,
            'metrikaon'   => $metrikaon,
        ));
    }

}