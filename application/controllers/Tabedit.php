<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tabedit extends CI_Controller {

// Токен безопасности для отправки форм
protected static $csrf;

// Используемые параметры конфига
protected static $twigconfig;
protected static $alerts;
protected static $resp;

protected static $yanotes;
    

    public function __construct() {
        parent::__construct();

        // CSRF токен безопасности для пользовательских форм
        self::$csrf = array('name' => $this->security->get_csrf_token_name(), 'hash' => $this->security->get_csrf_hash());

        // Загрузка кастомного конфига и необходимых параметров
        $CI =& get_instance();
        $CI->config->load('ukonfig', TRUE);
        $ukonfig = $CI->config->item('ukonfig');

        self::$twigconfig  = $ukonfig['twigconfig'];
        self::$alerts      = $ukonfig['alerts'];
        self::$resp        = $ukonfig['resp'];
        
        self::$yanotes     = $ukonfig['settings']['yanotes'];

        // Получаем параметры запуска приложения от ВК или из сессии
        $this->load->model('vkauth');
        $this->vkauth->get();
        
        // Модель с тарифами загружаем вне зависимости от состояния биллинга
        $this->load->model('plan');
    }


	/**
	 * http://thinkapps.kassa/tabedit
	 */
	public function index() {
        
        // Проверка на режим обслуживания до загрузок основных моделей и запросов к БД
        if (MAINTENANCE) {
            header("Location:/");       // Редирект на главную
            exit;                       // Принудительное завершение работы фреймворка
        }
        
        $this->load->model('settings');
        $settings = $this->settings->get();
        
        $this->load->model('tabs');
        $tabs = $this->tabs->get();
        
        // Если биллинг включен и приложение установлено в сообществе
        if (BILLING && $this->vkauth->is_valid_gid() && $this->settings->is_installed()) {
            $this->plan->update_auto();             // Проверяем наступление даты оплаты, продляем тариф, списываем сумму за платный тариф
            $tabs = $this->tabs->limit_by_plan();   // Ограничиваем число и параметры вкладок по текущему тарифу
        }
        
        // Пройдена авторизация ВК и юзер является админом сообщества (если есть админ, то gid заведомо валиден)
        if ($this->vkauth->is_auth && $this->vkauth->is_admin) {
            
            $this->load->library('twig', self::$twigconfig);
            $this->load->library('alert');
            
            $alerts = $this->alert->add(
                 self::$alerts['editor_newbie']               // Подсказки для тех, кто впервые использует приложение
            );
            
            $this->twig->display('tabedit', array(
                'environment' => ENVIRONMENT,
                'billing'     => BILLING,
                'vkdata'      => $this->vkauth->get(),
                'settings'    => $settings, 
                'tabs'        => $tabs, 
                'tablimits'   => $this->plan->tablimits(), 
                'yanotes'     => self::$yanotes,
                'csrf'        => self::$csrf,
                'alerts'      => $alerts
            ));
        }
        // Авторизация пройдена, но юзер не админ
        elseif ($this->vkauth->is_auth) {
            header("Location:/");       // Редирект на главную
            exit;                       // Принудительное завершение работы фреймворка
        }
    
        // Если не пройдена авторизация ВК - ничего не выводим в браузер
	}


	/**
	 * Рендер шаблона с телом вкладок для возврата в ajax-скрипт и последующей замены div#tabeditbody в DOM-документе
	 * @private
     * @param  integer $tid       id вкладки
     * @param  integer $parenttid id родительской вкладки (0 для вкладки 1-го уровня)
	 * @return string  Html-код отрендеренного шаблона
	 */
	protected function _tabeditbody($tid, $parenttid) {
        
        $this->load->model('settings');
        $settings = $this->settings->get();
        
        $this->load->model('tabs');
        $tabs = $this->tabs->get(TEXT_RESTORE_FORM, $tid, $parenttid);
        
        // Ограничиваем количество и параметры вкладок в соответствии с текущим тарифом
        if (BILLING) {
            $tabs = $this->tabs->limit_by_plan(TEXT_RESTORE_FORM, $tid, $parenttid);
        }

        $this->load->library('twig', self::$twigconfig);

        // Рендерим шаблон и возвращаем html-код
        return 
            $this->twig->render('tabeditbody', array(
                'settings'  => $settings, 
                'tabs'      => $tabs, 
                'tablimits' => $this->plan->tablimits(), 
                'yanotes'   => self::$yanotes,
                'csrf'      => self::$csrf,
            ));
	}


	/**
	 * Ajax-вызываемый контроллер сохранения настроек вкладки
	 * @return HTTP 200 JSON-массив ответа в формате: 
	 *  boolean $resp['response']    Успешно
	 *  boolean $resp['error']       Ошибка
	 *  string  $resp['text']        Текст сообщения для показа юзеру
	 *  string  $resp['tabeditbody'] Html-код отрендеренного шаблона со вкладками для замены div#tabeditbody в DOM-документе
	 */
	public function save() {
        
        if (MAINTENANCE OR !$this->input->is_ajax_request() OR !$this->vkauth->is_auth) 
            die();

        $tablimits = $this->plan->tablimits();

        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('tid',        '', "required|integer|greater_than_equal_to[1]|less_than_equal_to[{$tablimits['maxtabs']}]");
        $this->form_validation->set_rules('parenttid',  '', "required|integer|greater_than_equal_to[0]|less_than_equal_to[{$tablimits['maxtabs']}]");
        $this->form_validation->set_rules('sum',        '', "required|integer|greater_than_equal_to[1]|less_than_equal_to[{$tablimits['maxsum']}]");
        $this->form_validation->set_rules('editsum',    '', "integer|max_length[1]");
        $this->form_validation->set_rules('paylock',    '', "integer|max_length[1]");
        $this->form_validation->set_rules('toptext',    '', "trim|max_length[{$tablimits['ttmax']}]");
        $this->form_validation->set_rules('bottomtext', '', "trim|max_length[{$tablimits['btmax']}]");
        
        $resp = [];
        if ($this->form_validation->run()) {
            
            $tid       = (int) $this->input->post('tid');
            $parenttid = (int) $this->input->post('parenttid');

            $this->load->model('tabs');
            
            // Обновляем настройки вкладки в БД
            $result = $this->tabs->update(
                $tid, 
                $parenttid, 
                $this->input->post('sum'), 
                $this->input->post('editsum'), 
                $this->input->post('paylock'), 
                $this->input->post('toptext'), 
                $this->input->post('bottomtext')
            );
            
            $resp = $this->tabs->resp;  // Модель отдаёт в resp корректный ответ с описанием ошибки
            
            if ($result) 
                $resp['tabeditbody'] = $this->_tabeditbody($tid, $parenttid);
        }
        else
            $resp = self::$resp['error_something'];
        
        // Отправляем строку ответа в ajax-скрипт
        echo json_encode($resp);
    }


	/**
	 * Ajax-вызываемый контроллер сохранения заголовка вкладки
	 * @return HTTP 200 JSON-массив ответа в формате: 
	 *  boolean $resp['response']    Успешно
	 *  boolean $resp['error']       Ошибка
	 *  string  $resp['text']        Текст сообщения для показа юзеру
	 *  string  $resp['tabeditbody'] Html-код отрендеренного шаблона со вкладками для замены div#tabeditbody в DOM-документе
	 */
	public function savetitle() {
        
        if (MAINTENANCE OR !$this->input->is_ajax_request() OR !$this->vkauth->is_auth) 
            die();

        $tablimits = $this->plan->tablimits();

        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('tid',        '', "required|integer|greater_than_equal_to[1]|less_than_equal_to[{$tablimits['maxtabs']}]");
        $this->form_validation->set_rules('parenttid',  '', "required|integer|greater_than_equal_to[0]|less_than_equal_to[{$tablimits['maxtabs']}]");
        $this->form_validation->set_rules('title',      '', "required|trim|min_length[{$tablimits['mintitle']}]|max_length[{$tablimits['maxtitle']}]");
        
        $resp = [];
        if ($this->form_validation->run()) {
            
            $tid        = (int) $this->input->post('tid');
            $parenttid  = (int) $this->input->post('parenttid');
            
            $this->load->model('tabs');
            
            // Обновляем заголовок вкладки в БД
            $result = $this->tabs->update_title(
                $tid, 
                $parenttid, 
                $this->input->post('title')
            );
            
            $resp = $this->tabs->resp;  // Модель отдаёт в resp корректный ответ с описанием ошибки
            
            if ($result) 
                $resp['tabeditbody'] = $this->_tabeditbody($tid, $parenttid);
        }
        else
            $resp = self::$resp['error_something'];
        
        // Отправляем строку ответа в ajax-скрипт
        echo json_encode($resp);
    }


	/**
	 * Ajax-вызываемый контроллер операций над вкладками
	 * @return HTTP 200 JSON-массив ответа в формате: 
	 *  boolean $resp['response']    Успешно
	 *  boolean $resp['error']       Ошибка
	 *  string  $resp['text']        Текст сообщения для показа юзеру
	 *  string  $resp['tabeditbody'] Html-код отрендеренного шаблона со вкладками для замены div#tabeditbody в DOM-документе
	 */
	public function control() {
        
        if (MAINTENANCE OR !$this->input->is_ajax_request() OR !$this->vkauth->is_auth) 
            die();

        // Коды команд, используемые в форме
        $validcommands = array('addright', 'addleft', 'deltab', 'addlvl', 'dellvl');
        
        // Разбираем собственный формат команды
        if ($ismatched = preg_match("/^([a-z]{6,10})_(\d{1,2})-(\d{1,2})_([a-z0-9]+)$/i", $this->input->get('cmd'), $m)) {
            $command   = $m[1];
            $tid       = (int) $m[2];
            $parenttid = (int) $m[3];
            $hash      = $m[4];
        }

        $resp = [];
        if ($ismatched AND in_array($command, $validcommands) AND (self::$csrf['hash'] == $hash)) {

            $this->load->model('tabs');
            
            switch ($command) {
                
                case 'addright':
                    $result = $this->tabs->add_tab_right($tid, $parenttid);
                    break;
                
                case 'addleft':
                    $result = $this->tabs->add_tab_left($tid, $parenttid);
                    break;
                
                case 'deltab':
                    $result = $this->tabs->del_tab($tid, $parenttid);
                    break;
                
                case 'addlvl':
                    $result = $this->tabs->add_level($tid, $parenttid);
                    break;
                
                case 'dellvl':
                    $result = $this->tabs->del_level($tid, $parenttid);
                    break;
            }
            
            $resp = $this->tabs->resp;  // Модель отдаёт в resp корректный ответ с описанием ошибки

            if ($result) 
                $resp['tabeditbody'] = $this->_tabeditbody($this->tabs->newtid, $this->tabs->newparenttid);
        }
        else
            $resp = self::$resp['error_make'];
        
        echo json_encode($resp);
    }

}