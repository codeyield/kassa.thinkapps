<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alert {
    
    /**
     * Формирует массив со списком массивов алертов, пропуская те алерты, в куках которых есть инфа о пристановке показа.
     * Принимает любое число аргументов, где каждый аргумент - массив алерта из конфига.
     * @params array $alert1, $alert2, ... Массивы алертов из конфига
     * @return array Массив со списком массивов алертов
     */
    public function add() {
        $alerts = [];
        
        foreach (func_get_args() as $alert) {
            // Упрощенная валидация на наличие обязательных элементов в алерте
            if (!is_null($alert) AND isset($alert['type']) AND isset($alert['close']) AND isset($alert['text'])) {
                
                // Переносим в итоговый массив только те алерты, у которых НЕТ куки о приостановке их показа
                if ( !(isset($alert['id']) AND isset($_COOKIE[$alert['id']]) AND ($_COOKIE[$alert['id']] == 0)) )
                    $alerts[] = $alert;
            }
        }
        
        return $alerts;
    }

}
