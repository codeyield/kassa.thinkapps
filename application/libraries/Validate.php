<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Validate {

protected static $ukonfig;
protected static $charset;

    
    /**
     * @private
     * @param array $ukonfig Массив конфигурации (опционально)
     */
    public function __construct($ukonfig = FALSE) {
        
        $CI =& get_instance();
        self::$charset = $CI->config->item('charset');    // Значение из глобального конфига
        
        if (isset($ukonfig) AND is_array($ukonfig)) {
            self::$ukonfig = $ukonfig;
        }
        else {
            // Загрузка собственного конфига
            $CI->config->load('ukonfig', TRUE);
            self::$ukonfig = $CI->config->item('ukonfig');
        }
    }
    
    
    /**
     * Проверяет на валидность № Яндекс.Кошелька
     * @param  string $yapurse Значение для валидации
     * @return boolean Валидный или нет
     */
    public function is_yapurse($yapurse) {
        return preg_match(
            "/^\d{" .self::$ukonfig['settings']['yapurse']['minlen']. "," .self::$ukonfig['settings']['yapurse']['maxlen']. "}$/", 
            $yapurse
        );
    }
    
    /**
     * Принудительно валидирует текст на кнопке оплаты
     * @param  string $yabutton Значение для валидации
     * @return string Валидный текст для кнопки или дефолтное значение из конфига
     */
    public function yabutton($yabutton) {
        return $this->text($yabutton, 
                           self::$ukonfig['settings']['yabutton']['minlen'], 
                           self::$ukonfig['settings']['yabutton']['maxlen'], 
                           self::$ukonfig['settings']['yabutton']['text']
        );
    }
    
    /**
     * Принудительно валидирует ключ поля "Примечание к платежу"
     * @param  string $yanote Ключ для валидации
     * @return string Валидный ключ
     */
    public function yanote($yanote) {
        return in_array($yanote, array_keys(self::$ukonfig['settings']['yanotes']['formsetup'])) 
            ? $yanote 
            : self::$ukonfig['settings']['yanotes']['defkey'];
    }
    
    /**
     * Принудительно валидирует текст над формой
     * @param  string $toptext Текст для валидации
     * @return string Валидный текст или пустая строка или NULL, если текст совпадает с дефолтным
     */
    public function toptext($toptext, $maxlen = FALSE) {
        $maxlen = $maxlen ? $maxlen : self::$ukonfig['settings']['toptext']['maxlen'];
        $toptext_default = self::$ukonfig['settings']['toptext']['text'];
        $md5_ttdefault = md5($this->text($toptext_default, 0, $maxlen));

        $toptext = $this->text($toptext, 0, $maxlen, $toptext_default);
        return (md5($toptext) == $md5_ttdefault) ? NULL : $toptext;
    }
    
    /**
     * Принудительно валидирует текст под формой
     * @param  string $bottomtext Текст для валидации
     * @return string Валидный текст или пустая строка или NULL, если текст совпадает с дефолтным
     */
    public function bottomtext($bottomtext, $maxlen = FALSE) {
        $maxlen = $maxlen ? $maxlen : self::$ukonfig['settings']['bottomtext']['maxlen'];
        $bottomtext_default = self::$ukonfig['settings']['bottomtext']['text'];
        $md5_btdefault = md5($this->text($bottomtext_default, 0, $maxlen));
        
        $bottomtext = $this->text($bottomtext, 0, $maxlen, $bottomtext_default);
        return (md5($bottomtext) == $md5_btdefault) ? NULL : $bottomtext;
    }

    /**
     * Принудительно валидирует ключ режима показа текста сверху над формой оплаты
     * @param  string $ttmode Ключ режима показа текста
     * @return string Валидное символьное имя или дефолтное значение
     */
    public function ttmode($ttmode) {
        return in_array($ttmode, array_keys(self::$ukonfig['settings']['toptext']['formsetup']))
            ? $ttmode
            : self::$ukonfig['settings']['toptext']['defkey'];
    }
    
    /**
     * Принудительно валидирует ключ режим показа текста снизу под формой оплаты
     * @param  string $btmode Ключ режима показа текста
     * @return string Валидное символьное имя или дефолтное значение
     */
    public function btmode($btmode) {
        return in_array($btmode, array_keys(self::$ukonfig['settings']['bottomtext']['formsetup']))
            ? $btmode
            : self::$ukonfig['settings']['bottomtext']['defkey'];
    }
    
    /**
     * Принудительно валидирует заголовок вкладки
     * @param  string $tabtitle Заголовок вкладки для валидации
     * @return string Валидный текст или дефолтный текст
     */
    public function tabtitle($tabtitle) {
        return $this->text($tabtitle, 
                           self::$ukonfig['tabs']['title_minlen'], 
                           self::$ukonfig['tabs']['title_maxlen'], 
                           self::$ukonfig['tabs']['title_default']
        );
    }

    /**
     * Восстанавливает полученный из БД текст для редактирования в форме или для вывода на html-страницу
     * @param  string   $text         Строка, содержащая кодированные html-сущности
     * @param  boolean  $restore_mode Режим восстановления текста: для вывода на html-страницу / для редактирования в форме
     * @return string   Восстановленная строка
     */
    public function text_restore($text, $restore_mode = TRUE) {
        
        // Текст для вывода на html-страницу - остаются кодированые html-сущности, добавляется html-разметка
        if ($restore_mode == TEXT_RESTORE_HTML) {
            
            // TODO: Правильный алгоритм: 1) html_entity_decode; 2) Необх. замены; 3) Снова htmlentities, ПРОПУСКАЯ html-теги
            
            $text = html_entity_decode($text, ENT_QUOTES);
            $text = preg_replace("/\r?\n/", '<br />', $text);
            $text = preg_replace(
                // Регулярка отсюда (с моими правками) https://msdn.microsoft.com/en-us/library/ff650303.aspx
                "/(http(s?)\:\/\/[0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*(:(0-9)*)*(\/?)([a-zA-Z0-9\-\.\?\,\'\/\\\+&%\$#_=]*)?)/", 
                '<a href="$1" target="_blank">$1</a>', $text);
        }
        // Текст для редактирования в форме с [возможным] последующим сохранением в БД
        else 
            $text = html_entity_decode($text, ENT_QUOTES);
        
        return $text;
    }
    
    /**
     * Принудительно валидирует текст: фильтрует сомнительный код, кодирует html-сущности, 
     *  обрезает длину до допустимой, присваивает дефолтное значение, если строкая невалидная или пустая
     * @param  string  $text                   Входной текст
     * @param  integer [$minlen       = 0]     Минимальная валидная длина, иначе дефолтный текст
     * @param  integer [$maxlen       = 10000] Максимальная длина для обрезки
     * @param  string  [$text_default = '']    Строка по умолчанию, если входной текст пустой
     * @return string  Валидный заголовок
     */
    public function text($text, $minlen = 0, $maxlen = 10000, $text_default = '') {
        
        $text = (string) $text;
        $text = htmlentities($text, ENT_QUOTES, self::$charset);
        $text = remove_invisible_characters($text, TRUE);
        $text = trim(mb_substr($text, 0, $maxlen, self::$charset));
        $text = (mb_strlen($text, self::$charset) < $minlen) ? $text_default : $text;
        return $text;
    }

    /**
     * Принудительно валидирует id вкладки на допустимый диапазон
     * @param  integer $tid Входное целое значение
     * @return integer Валидный id вкладки
     */
    public function tid($tid) {
        $tid = (int) $tid;
        return $tid < 1 ? 1 : ($tid > self::$ukonfig['tabs']['maxtabs'] ? self::$ukonfig['tabs']['maxtabs'] : $tid);
    }

    /**
     * Принудительно валидирует id, ссылающийся на родительскую вкладку, на допустимый диапазон
     * @param  integer $parenttid Входное целое значение
     * @return integer  Валидный id вкладки
     */
    public function parenttid($parenttid) {
        $parenttid = (int) $parenttid;
        return $parenttid < 0 ? 0 : ($parenttid > self::$ukonfig['tabs']['maxtabs'] ? self::$ukonfig['tabs']['maxtabs'] : $parenttid);
    }

}
