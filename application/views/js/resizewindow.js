// Динамически подстраивает высоту окна приложения ВК под фактический размер страницы после ее загрузки 
// Это также переопределит ШИРИНУ окна, вне зависимости от установленной в настройках приложения! 

var window_minheight = 650;
var window_width = 726;
function autosize() {
    var bh = Math.max(document.body.scrollHeight, document.body.offsetHeight, document.body.clientHeight);
    bh = Math.max(bh + 30, window_minheight);
    VK.callMethod('resizeWindow', window_width, bh);
}
window.onload = (function() {
    VK.init(function() {
        setInterval('autosize()', 500);
    });
});
