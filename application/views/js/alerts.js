/* Сохраняет куку на время, которое д.б. отключен показ закрытого юзером bootstrap-алерта */
$(document).ready(function () {
    $('.close').click(function () {
        var id = $(this).parent().attr('id');
        var ct = $(this).parent().attr('data-ct');
        if(!(typeof id == "undefined" && id)) {
            if (Number(ct) > 0) 
                var exp = (new Date(new Date().getTime() + Number(ct) * 1000)).toUTCString();
            else
                var exp = 0;
            document.cookie = id + "=0; path=/; expires=" + exp;
        }
    });
});
