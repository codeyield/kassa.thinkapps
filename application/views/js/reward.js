$(document).ready(function () {

    /* Проверяет и пробует погасить промо-код */
    $('#form_promo').submit(function() {
		var form = $(this);
        $.ajax({
            type: 'POST',
            url: '/reward/push_promo',
            dataType: 'json',
            data: form.serialize(),

            beforeSend: function() {
                // Заблокируем кнопку на время отправки формы
                form.find('button[type="submit"]').prop('disabled', true);
            },
            success: function(resp) {
                // Получилось, промо-код погашен
                if ('response' in resp) {
                    $('#promo-message').html(resp.text);
                    $('#promo-group').removeClass('has-error has-danger');
                    $('#promo-group').addClass('has-success');
                    
                    // Обновляем баланс и строку 'Для оплаты выбранного тарифа не хватает...'
                    $('#balance').html(resp.balance + 'р.');
                    if (resp.balance < resp.nextprice) {
                        var enought = resp.nextprice - resp.balance;
                        $('#balance').addClass('text-danger');      // Сумма красная, если ее недостаточно для след. продления тарифа
                        $('#enought').html('Для оплаты выбранного тарифа не хватает ' + enought + 'р.');
                        $('#paysum').val(enought < 50 ? 50 : enought);    // Исправляем сумму оплаты в платёжной форме, но не менее 50р
                    }
                    else {
                        $('#balance').removeClass('text-danger');   // Убираем красную подсветку суммы, если её достаточно для продления тарифа
                        $('#enought').html('');                     // Удаляем фразу целиком
                        $('#paysum').val(50);                       // Ставим минималку в платёжной форме
                    }
                } 
                // Не удалось погасить промо-код
                else {
                    $('#promo-message').html(resp.text);
                    $('#promo-group').removeClass('has-success');
                    $('#promo-group').addClass('has-error has-danger');
                }
            },
            error: function(resp) {
                $('#promo-message').html(trouble);
                $('#promo-group').removeClass('has-success');
                $('#promo-group').addClass('has-error has-danger');
            },
            complete: function() {
                setTimeout(function() { 
                    form.find('button[type="submit"]').prop('disabled', false); // Разблокируем кнопку отправки формы
                    $('#promo-message').html('');                               // Очищаем текст сообщения
                    $('#codename').val('');                                     // Очищаем поле ввода
                    $('#promo-group').removeClass('has-success has-error has-danger');
                }, 3000);
            }
        });
        return false;
	});

});