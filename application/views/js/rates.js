$(document).ready(function () {
    
    /* Добавляет обработчик для Bootstrap Tooltip */
    $('.container').tooltip({
        selector: '[data-toggle="tooltip"]'
    });

    /* Отправляет форму с выбором тарифа */
    $('#form_plan').submit(function() {
		var form = $(this);
        var makeupd = false;
        
        // Получаем данные подтверждения операции для показа юзеру
        $.ajax({
            type: 'POST',
            url: '/rates/confirm_plan',
            dataType: 'json',
            data: form.serialize(),

            success: function(data) {
                if (('confirm' in data) && ('text' in data)) {
                    if (data.confirm) {
                        
                        // Модальное окно подтверждения операции для юзера
                        makeupd = confirm(data.text);
                        
                        if (makeupd) {      // После подтверждения операции юзером пробуем изменить тариф
                            
                            // ВЛОЖЕННЫЙ AJAX - отправка формы
                            $.ajax({
                                type: 'POST',
                                url: '/rates/save_plan',
                                dataType: 'json',
                                data: form.serialize(),

                                beforeSend: function() {
                                    // Блокируем кнопку на время отправки формы
                                    form.find('button[type="submit"]').prop('disabled', true);
                                },
                                success: function(resp) {
                                    if ('response' in resp) {
                                        var radios   = 'input[type="radio"]';                // Селектор всех радиокнопок в форме
                                        var exp      = new Date(Date.parse(resp.planset.planexpiry));
                                        var rudate   = new Intl.DateTimeFormat("ru");        // Формат даты вида ДД.ММ.ГГГГ
                                        var tridnow  = '#trid_' + resp.planset.plan;         // id тега <tr> с текущим тарифом
                                        var tridnext = '#trid_' + resp.planset.nextplan;     // id тега <tr> со следующим тарифом

                                        // Очищаем все прежние классы и атрибуты на радиокнопках
                                        form.find(radios).prop('disabled', false);
                                        form.find(radios).prop('checked', false);
                                        form.find(radios).prop('required', true);

                                        // Удаляем все подсказки-тултипы на строках таблицы
                                        form.find('tr').removeClass('success info tooltip-red');
                                        form.find('tr').removeAttr('data-toggle');
                                        form.find('tr').removeAttr('data-placement');
                                        form.find('tr').removeAttr('data-original-title');

                                        // Обновляем строчку с инфой о тарифе и сроке его использоания
                                        $('#plan-name').html(resp.planset.name);
                                        $('#plan-expiry').html(rudate.format(exp));

                                        // Обновляем баланс и делаем его "красным", если суммы недостаточно для след. продления тарифа
                                        $('#balance').html(resp.balance + 'р.');
                                        if (resp.balance < resp.nextprice) {
                                            $('#balance').addClass('text-danger');
                                        }
                                        else {
                                            $('#balance').removeClass('text-danger');
                                        }

                                        // Блокируем радиокнопки со следующим тарифом
                                        $('#radioid_' + resp.planset.nextplan).prop('disabled', true);
                                        $('#radioid_' + resp.planset.nextplan).prop('required', false);

                                        // Отмечаем строку таблицы со следующим тарифом
                                        $(tridnext).addClass('info tooltip-blue');
                                        $(tridnext).attr('data-toggle', 'tooltip');
                                        $(tridnext).attr('data-placement', 'top');
                                        $(tridnext).attr('data-original-title', 'Ваш тариф на следующий период');

                                        // Отмечаем строку таблицы с текущим тарифом. Если текущий и следующий совпадают - перезапишем поверх текущим
                                        $(tridnow).addClass('success tooltip-green');
                                        $(tridnow).attr('data-toggle', 'tooltip');
                                        $(tridnow).attr('data-placement', 'top');
                                        $(tridnow).attr('data-original-title', 'Ваш текущий тариф');

                                        // Обновим текст подсказки, если текущий и следующий тарифы совпадают
                                        if (resp.planset.plan == resp.planset.nextplan) {
                                            $(tridnow).attr('data-original-title', 'Ваш тариф сейчас и на следующий период');
                                        }

                                        // Уведомление об успешной смене тарифа
                                        $.notify({message: resp.text}, green_notify);
                                    } else {
                                        // Сервер вернул ошибку смены тарифа
                                        $.notify({message: resp.text}, red_notify);
                                    }
                                },
                                error: function(resp) {
                                    $.notify({message: trouble}, red_notify);
                                },
                                complete: function(resp) {
                                    // Разблокируем кнопку отправки формы через notifydelay
                                    setTimeout(function() { 
                                        form.find('button[type="submit"]').prop('disabled', false);
                                    }, notifydelay * 2);
                                }
                            });
                            // КОНЕЦ ВЛОЖЕННОГО AJAX
                            
                        } // endif (makeupd)
                    }
                    else {
                        
                        // Модальное окно с уведомлением юзера об ошибке
                        alert(data.text);
                    }
                }
            },
            error: function(confirm) {
                $.notify({message: trouble}, red_notify);
            }
        });
        
        return false;
	});

});