$(document).ready(function () {
    
    /* Добавляет обработчик для Bootstrap Tooltip */
    $('.setup-container').tooltip({
        selector: '[data-toggle="tooltip"]'
    });

    /* Включает и выключает приложение */
    $('#form_appon').submit(function() {
		var form = $(this);
        $.ajax({
            type: 'POST',
            url: '/setup/appon',
            dataType: 'json',
            data: form.serialize(),

            success: function(resp) {
                if (('response' in resp) && resp['response']) {
                    $('#form_appon_on').removeClass('hidden');
                    $('#form_appon_off').addClass('hidden');
                    $.notify({message: resp['text']},green_notify);
                } else if (('response' in resp) && !resp['response']) {
                    $('#form_appon_off').removeClass('hidden');
                    $('#form_appon_on').addClass('hidden');
                    $.notify({message: resp['text']}, red_notify);
                } else {
                    $.notify({message: resp['text']}, red_notify);
                }
            },
            error: function(resp) {
                $.notify({message: trouble}, red_notify);
            }
        });
        return false;
	});

    /* Отправляет форму с платёжной информацией */
    $('#form_payinfo').submit(function() {
		var form = $(this);
        $.ajax({
            type: 'POST',
            url: '/setup/save_payinfo',
            dataType: 'json',
            data: form.serialize(),

            beforeSend: function() {
                form.find('button[type="submit"]').prop('disabled', true);
            },
            success: function(resp) {
                if ('response' in resp) {
                    $.notify({message: resp['text']}, green_notify);
                } else {
                    $.notify({message: resp['text']}, red_notify);
                }
            },
            error: function(resp) {
                $.notify({message: trouble}, red_notify);
            },
            complete: function() {
                setTimeout(function() { 
                    form.find('button[type="submit"]').prop('disabled', false);
                }, notifydelay * 2);
            }
        });
        return false;
	});

    /* Отправляет форму с текстами над и под формами оплаты */
    $('#form_texts').submit(function() {
		var form = $(this);
        $.ajax({
            type: 'POST',
            url: '/setup/save_texts',
            dataType: 'json',
            data: form.serialize(),
            
            beforeSend: function() {
                form.find('button[type="submit"]').prop('disabled', true);
            },
            success: function(resp) {
                if ('response' in resp) {
                    $.notify({message: resp['text']}, green_notify);
                } else {
                    $.notify({message: resp['text']}, red_notify);
                }
            },
            error: function(resp) {
                $.notify({message: trouble}, red_notify);
            },
            complete: function() {
                setTimeout(function() { 
                    form.find('button[type="submit"]').prop('disabled', false);
                }, notifydelay * 2);
            }
        });
        return false;
	});

    /* Показывает/скрывает поле формы "Общий текст сверху на всех вкладках" в форме с текстами */
    $('#ttmode').on('change', function(){
        if ($(this).val() == 'main') {
            $('#toptext').prop('disabled', false);
            $('#toptext').removeClass('hidden');
            $('#toptext_label').removeClass('hidden');
        }
        else {
            $('#toptext').prop('disabled', true);
            $('#toptext').addClass('hidden');
            $('#toptext_label').addClass('hidden');
        }
        return true;
    });

    /* Показывает/скрывает поле формы "Общий текст снизу на всех вкладках" в форме с текстами */
    $('#btmode').on('change', function(){
        if ($(this).val() == 'main') {
            $('#bottomtext').prop('disabled', false);
            $('#bottomtext').removeClass('hidden');
            $('#bottomtext_label').removeClass('hidden');
        }
        else {
            $('#bottomtext').prop('disabled', true);
            $('#bottomtext').addClass('hidden');
            $('#bottomtext_label').addClass('hidden');
        }
        return true;
    });

});