/* Добавляет обработчик для Bootstrap Tooltip */
$(document).ready(function () {
    $('#tabeditbody').tooltip({
        selector: '[data-toggle="tooltip"]'
    });
});

/* Отправляет форму с настройками вкладки */
function SendTabEdit(mixid) {
    var form = $('#form_tabedit' + mixid);
    $.ajax({
        type: 'POST',
        url: '/tabedit/save',
        dataType: 'json',
        data: form.serialize(),

        beforeSend: function() {
            $('#submit_tabtitle' + mixid).prop('disabled', true);
        },
        success: function(resp) {
            if ('response' in resp) {
                if ('tabeditbody' in resp) {
                    $('#tabeditbody').html(resp['tabeditbody']);
                    $('#submit_tabedit' + mixid).prop('disabled', true);
                }
                $.notify({message: resp['text']}, green_notify);
            } else {
                $.notify({message: resp['text']}, red_notify);
            }
        },
        error: function(resp) {
            $.notify({message: trouble}, red_notify);
        },
        complete: function() {
            setTimeout(function() { 
                $('#submit_tabedit' + mixid).prop('disabled', false);
            }, notifydelay * 2);
        }
    });
    return false;
};

/* Отправляет форму с заголовком вкладки */
function SendTabTitle(mixid) {
    var form = $('#form_tabtitle' + mixid);
    $.ajax({
        type: 'POST',
        url: '/tabedit/savetitle',
        dataType: 'json',
        data: form.serialize(),

        beforeSend: function() {
            $('#submit_tabtitle' + mixid).prop('disabled', true);
        },
        success: function(resp) {
            if ('response' in resp) {
                if ('tabeditbody' in resp) {
                    $('#tabeditbody').html(resp['tabeditbody']);
                    $('#submit_tabtitle' + mixid).prop('disabled', true);
                }
                $.notify({message: resp['text']}, green_notify);
            } else {
                $.notify({message: resp['text']}, red_notify);
            }
        },
        error: function(resp) {
            $.notify({message: trouble}, red_notify);
        },
        complete: function() {
            setTimeout(function() { 
                $('#submit_tabtitle' + mixid).prop('disabled', false);
            }, notifydelay * 2);
        }
    });
    return false;
};

/* Отправляет форму, выполняющую действия на вкладками */
function SendTabControl(cmd, mixid) {
    var send = {};
    send['cmd'] = cmd;
    $.ajax({
        type: 'GET',
        url: '/tabedit/control',
        dataType: 'json',
        data: send,

        beforeSend: function() {
            // Отключаем кнопку только до получения ответа сервера
            $('#button_tabcontrol' + mixid).prop('disabled', true);
        },
        success: function(resp) {
            if ('response' in resp) {
                if ('tabeditbody' in resp) {
                    $('#tabeditbody').html(resp['tabeditbody']);
                }
            } else {
                $.notify({message: resp['text']}, red_notify);
            }
        },
        error: function(resp) {
            $.notify({message: trouble}, red_notify);
        },
        complete: function(data) {
            // Нет смысла в доп. задержке, т.к. фокус переключается на др. вкладку
            $('#button_tabcontrol' + mixid).prop('disabled', false);
        }
    });
    return false;
};
