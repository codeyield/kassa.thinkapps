var trouble = "Что-то не получилось, попробуйте повторить операцию.";
var notifydelay = 1000;

/* Настройки всплывающих уведомлений */
var green_notify = {
    type: 'green',
    delay: notifydelay,
    offset: 150,
    allow_dismiss: false,
    placement: {from: 'top', align: 'center'},
    animate: {enter: 'animated fadeInDown', exit: 'animated fadeOutUp'}
};
var red_notify = {
    type: 'red',
    delay: notifydelay,
    offset: 150,
    allow_dismiss: false,
    placement: {from: 'top', align: 'center'},
    animate: {enter: 'animated fadeInDown', exit: 'animated fadeOutUp'}
};
