<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vkauth extends CI_Model {

// Публичные свойства класса с данными авторизации
public $api_id		= NULL;
public $gid			= NULL;
public $uid			= NULL;
public $is_admin	= NULL;
public $is_auth		= NULL;
public $is_sign		= NULL;
public $api_url		= NULL;
public $access_token= NULL;
public $referrer	= NULL;
public $screen_name	= NULL;

// Секрет приложения для проверки подписи
protected static $appsecret;


	public function __construct() {
        parent::__construct();

        // Загрузка конфига (стандартный способ через $this дает ошибку)
        $CI =& get_instance();
        $CI->config->load('ukonfig', TRUE);
        $ukonfig = $CI->config->item('ukonfig');
        
        self::$appsecret  = $ukonfig['vkapp']['secret'];
    }
    
    
    /**
     * Получает параметры запуска приложения из сессии, если она есть, или от ВК, сохраняя затем в сессию
     * 
	 * @url https://vk.com/dev/apps_init
	 * Полный список параметров (кроме устаревших):
	 *   api_url (string)          — URL сервиса API, по которому необходимо осуществлять запросы.
	 *   api_id (integer)          — id запущенного приложения.
	 *   api_settings              — битовая маска настроек прав доступа текущего юзера в приложении.
	 *   viewer_id (integer)       — id юзера, который запустил приложение.
	 *   viewer_type               — тип юзера, который просматривает приложение
	 *   access_token              — ключ доступа для вызова методов API по классической схеме.
	 *   user_id (integer)         — id юзера, со страницы которого было запущено приложение. Если запущено не со страницы юзера - 0.
	 *   group_id (integer)        — id сообщества, со страницы которого было запущено приложение. Если запущено не со страницы сообщества - 0.
	 *   is_app_user (integer 0/1) — если пользователь установил приложение - 1, иначе — 0.
	 *   auth_key (string)         — ключ авторизации юзера на стороннем сервере: auth_key = md5(api_id + '_' + viewer_id + '_' + api_secret)
	 *   language (integer)        — id языка юзера, просматривающего приложение (см. список языков ниже).
	 *   parent_language (integer) — id языка, которым можно заменить язык юзера (возвращается для неофициальных языковых версий).
	 *   is_secure (integer 0/1)   — если пользователем используется защищенное соединение – 1, иначе — 0.
	 *   api_result (string)       — результат первого запроса к API, который выполняется при загрузке приложения.
	 *   referrer (string)         — обозначение места, откуда пользователь перешёл в приложение.
	 *   hash (string)             — хэш запроса (данные после символа # в строке адреса). 
	 *  
	 * Значение viewer_type:
	 * Если приложение запущено со страницы сообщества (group_id != 0):
	 *   4 — если пользователь является создателем или администратором сообщества;
	 *   3 — если пользователь является редактором сообщества;
	 *   2 — если пользователь является модератором сообщества;
	 *   1 — если пользователь является участником сообщества;
	 *   0 — если пользователь не состоит в сообществе.
	 * Если приложение запущено со страницы пользователя (user_id != 0):
	 *   2 — если пользователь является владельцем страницы;
	 *   1 — если пользователь является другом владельца страницы;
	 *   0 — если пользователь не состоит в друзьях владельца страницы.
	 *   
	 * @url https://vk.com/dev/community_apps_docs
	 * Дополнительные параметры для приложений сообществ:
	 *   sign — подпись запроса
	 *   
     * @return array  Массив с данными авторизации (он же массив сессии)
    */
    public function get() {
        
        $get = $this->input->get(NULL, FALSE);
        
        // Упрощенная проверка GET-параметров в режиме разработки
        // ВНИМАНИЕ! ВК НЕ ОТДАЁТ group_id, если приложение запущено по ссылке вида https://vk.com/app6088074
        //  Следовательно, НЕ НУЖНО проверять наличие group_id в параметрах - его там нет!
        if (((ENVIRONMENT == 'development') && $this->isset_get_params('viewer_id', 'viewer_type', 'group_id')) ||
            ((ENVIRONMENT == 'production') && $this->isset_get_params('api_id', 'viewer_id', 'viewer_type', 'access_token'))
           ) {
            
            // Приложение запущено со страницы сообщества И запустивший юзер является его админом
            $is_admin = (($get['group_id'] != 0) && ($get['viewer_type'] == 4));
            
            // Проверка аутентификации ВК (для режима разработки проверка считается пройденной)
            $is_auth  = 
                (ENVIRONMENT == 'development') ||
                ((ENVIRONMENT == 'production') && ($get['auth_key'] == md5($get['api_id'] .'_'. $get['viewer_id'] .'_'. self::$appsecret)));
            
            // Проверяем подпись данных sign
            $sign = '';
            foreach ($get as $param => $value) {
                if ($param == 'hash' || $param == 'sign' || $param == 'api_result') continue;
                $sign .= $value;
            }
            $is_sign = (hash_hmac('sha256', $sign, self::$appsecret) == $get['sign']); 

            // Получаем screen_name юзера [для поля примечания к платежу]
            // 1-й запрос при запуске приложения: method=users.get&user_ids={viewer_id}&format=json&fields=screen_name&v=5.65
            $resp = json_decode($get['api_result'], TRUE);
            $screen_name = isset($resp['response'][0]['screen_name']) ? $resp['response'][0]['screen_name'] : 'id'.$get['viewer_id'];

            $this->api_id        = (int) $get['api_id'];
            $this->gid           = (int) $get['group_id'];
            $this->uid           = (int) $get['viewer_id'];
            $this->is_admin      = $is_admin;
            $this->is_auth       = $is_auth;
            $this->is_sign       = $is_sign;
            $this->api_url       = $get['api_url'];
            $this->access_token  = $get['access_token'];
            $this->referrer      = $get['referrer'];
            $this->screen_name   = $screen_name;

            $authdata = array(
                'api_id'        => $this->api_id,
                'gid'           => $this->gid,
                'uid'           => $this->uid,
                'is_admin'      => $this->is_admin,
                'is_auth'       => $this->is_auth,
                'is_sign'       => $this->is_sign,
                'api_url'       => $this->api_url,
                'access_token'  => $this->access_token,
                'referrer'      => $this->referrer,
                'screen_name'   => $this->screen_name,
            );

            // Сохраняем сессию. Обычный юзер (не админ) использует _единственную_ страницу приложения и формально ему сессия не нужна.
            // Но из соображений безопасности, вероятно, есть смысл создать сессию и для него также.
            $this->session->set_userdata($authdata);
        }
        
        // Если GET-параметры не пришли и свойства класса пустые, пробуем заполнить данными из сессии
        else {

            $this->api_id       = isset($this->api_id)       ? $this->api_id       : (isset($_SESSION['api_id'])       ? $_SESSION['api_id']       : NULL);
            $this->gid          = isset($this->gid)          ? $this->gid          : (isset($_SESSION['gid'])          ? $_SESSION['gid']          : NULL);
            $this->uid          = isset($this->uid)          ? $this->uid          : (isset($_SESSION['uid'])          ? $_SESSION['uid']          : NULL);
            $this->is_admin     = isset($this->is_admin)     ? $this->is_admin     : (isset($_SESSION['is_admin'])     ? $_SESSION['is_admin']     : NULL);
            $this->is_auth      = isset($this->is_auth)      ? $this->is_auth      : (isset($_SESSION['is_auth'])      ? $_SESSION['is_auth']      : NULL);
            $this->is_sign      = isset($this->is_sign)      ? $this->is_sign      : (isset($_SESSION['is_sign'])      ? $_SESSION['is_sign']      : NULL);
            $this->api_url      = isset($this->api_url)      ? $this->api_url      : (isset($_SESSION['api_url'])      ? $_SESSION['api_url']      : NULL);
            $this->access_token = isset($this->access_token) ? $this->access_token : (isset($_SESSION['access_token']) ? $_SESSION['access_token'] : NULL);
            $this->referrer     = isset($this->referrer)     ? $this->referrer     : (isset($_SESSION['referrer'])     ? $_SESSION['referrer']     : NULL);
            $this->screen_name  = isset($this->screen_name)  ? $this->screen_name  : (isset($_SESSION['screen_name'])  ? $_SESSION['screen_name']  : NULL);

            $authdata = array(
                'api_id'        => $this->api_id,
                'gid'           => $this->gid,
                'uid'           => $this->uid,
                'is_admin'      => $this->is_admin,
                'is_auth'       => $this->is_auth,
                'is_sign'       => $this->is_sign,
                'api_url'       => $this->api_url,
                'access_token'  => $this->access_token,
                'referrer'      => $this->referrer,
                'screen_name'   => $this->screen_name,
            );
        }
        
        return $authdata;
    }
    
    
    /**
     * Проверяет сессионный groud id на валидность
     * @return boolean Валиден или нет
     */
    public function is_valid_gid() {
        return !(is_null($this->gid) OR ($this->gid === FALSE) OR ($this->gid <= 0));
    }
 
    
    /**
     * Проверяет, что каждый параметр, перечисленный в аргументах ф-ции, присутствует в $_GET массиве
     */
    protected function isset_get_params() {
        
        $params  = array_keys($this->input->get(NULL, FALSE));
        $numargs = func_num_args();
        
        $k = 0;
        foreach (func_get_args() as $arg) 
            if (in_array($arg, $params))
                ++$k;
        
        return ($numargs == $k);
    }
    
}