<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends CI_Model {

// Массив ответа $resp, возвращаемый по цепочке: модель -> контроллер -> ajax-скрипт
public $resp = [];

// Таблицы в БД
protected static $tblsettings;      // Таблица с основными настройками сообщества
protected static $tblupdpurses;     // Таблица с историей изменения кошельков
    
// Используемые параметры конфига
protected static $appon;
protected static $yapurse;
protected static $yabutton;
protected static $yanotes;
protected static $toptext;
protected static $bottomtext;
    
protected static $trialdays;
protected static $trial_id;
protected static $free_id;
protected static $plansheet;

protected static $affprog;
protected static $responses;

// Внутренний кэш экземпляра класса с настройками сообщества
protected $settings = NULL;


	public function __construct() {
        parent::__construct();

        // Загрузка конфигов в модели
        $CI =& get_instance();
        $CI->config->load('ukonfig', TRUE);
        $ukonfig = $CI->config->item('ukonfig');

        self::$tblsettings  = $ukonfig['dbtbl']['settings'];
        self::$tblupdpurses = $ukonfig['dbtbl']['updpurses'];
        
        self::$appon        = $ukonfig['settings']['appon'];
        self::$yapurse      = $ukonfig['settings']['yapurse'];
        self::$yabutton     = $ukonfig['settings']['yabutton'];
        self::$yanotes      = $ukonfig['settings']['yanotes'];
        self::$toptext      = $ukonfig['settings']['toptext'];
        self::$bottomtext   = $ukonfig['settings']['bottomtext'];
        
        self::$trialdays    = $ukonfig['plans']['trialdays'];
        self::$trial_id     = $ukonfig['plans']['trial_id'];
        self::$free_id      = $ukonfig['plans']['free_id'];
        self::$plansheet    = $ukonfig['plans']['sheet'];
        
        self::$affprog      = $ukonfig['affprog'];
        self::$responses    = $ukonfig['resp'];

        // Отключаем строгий режим транзакций
        $this->db->trans_strict(FALSE);

        // Получаем параметры запуска приложения от ВК или из сессии
        $this->load->model('vkauth');
        $this->vkauth->get();
        
        $this->load->is_loaded('validate') OR $this->load->library('validate', $ukonfig);
	}

    
    /**
     * Возврашает основные настройки сообщества, а также инициализует новый gid, если его еще нет в БД
     * @param  boolean $restore_mode Режим восстановления текста: для вывода на html-страницу / для редактирования в форме
     * @return array   Табличный массив с настройками сообщества
     */
    public function get($restore_mode = TEXT_RESTORE_FORM) {
        
        if (!$this->vkauth->is_auth OR !$this->vkauth->is_valid_gid())
            return FALSE;
        
        // SELECT * FROM settings WHERE (gid = $gid)
        $selresult = $this->db->get_where(self::$tblsettings, array('gid' => $this->vkauth->gid));
        $data = (array) $selresult->row();
        
        // Инициализация нового gid и сохранение преднастроек в БД - ТОЛЬКО если приложение запустил АДМИН!
        if ((count($data) == 0) && $this->vkauth->is_admin) {
            
            // Вычисляем дату окончания триала
            $planexpiry = date('Y-m-d', strtotime('+'.self::$trialdays.' day'));
            
            // Получаем и валидируем реферальную куку (вне зависимости от состояния партнёрки - нужно для аналитики)
            $refgid = (isset($_COOKIE[self::$affprog['cookiename']]) AND preg_match("/^\d{2,10}$/", $_COOKIE[self::$affprog['cookiename']]))
                ? (int) $_COOKIE[self::$affprog['cookiename']] : NULL;
            
            $this->db
                ->set(array(
                    'gid'       => $this->vkauth->gid, 
                    'appon'     => self::$appon,
                    'yanote'    => self::$yanotes['defkey'],
                    'ttmode'    => self::$toptext['defkey'],
                    'btmode'    => self::$bottomtext['defkey'],
                    'plan'      => self::$trial_id,
                    'nextplan'  => self::$free_id,
                    'planexpiry'=> $planexpiry,
                    'affon'     => (self::$affprog['default'] ? 1 : 0),     // Приводим булев к TINYINT
                    'refgid'    => $refgid,
                ))
                ->set('created', 'NOW()', FALSE)
                ->insert(self::$tblsettings)
            OR log_message('error', "Error DB insert purse's history for gid #{$this->vkauth->gid} at query: ".$this->db->last_query());
            
            // Сохраняем инициализированный пробный тариф в истории тарифов
            $this->load->model('plan');
            $this->plan->update_history(self::$trial_id, $planexpiry);
            
            // Получаем данные, которые только что добавили
            $selresult = $this->db->get_where(self::$tblsettings, array('gid' => $this->vkauth->gid));
            $data = (array) $selresult->row();
        }
        
        // Что-то пошло не так - данные не получены и/или инициализация не удалась
        if (count($data) == 0) {
            log_message('error', "Error empty settings data for gid #{$this->vkauth->gid}");
            return FALSE;
        }
        
        // Получаем значения настроек сообщества из БД или присваиваем дефолтные из конфига
        $data['appon']      = (isset($data['appon']) AND ($data['appon'] > 0));     // Приводим к булеву
        $data['yabutton']   = isset($data['yabutton'])   ? $data['yabutton']   : self::$yabutton['text'];
        $data['toptext']    = isset($data['toptext'])    ? $data['toptext']    : self::$toptext['text'];
        $data['bottomtext'] = isset($data['bottomtext']) ? $data['bottomtext'] : self::$bottomtext['text'];
        
        // Восстанавливаем текст для редактирования в форме или для вывода на html-страницу
        $data['yabutton']   = $this->validate->text_restore($data['yabutton'],   FALSE);    // На кнопке html-теги не нужны
        $data['toptext']    = $this->validate->text_restore($data['toptext'],    $restore_mode);
        $data['bottomtext'] = $this->validate->text_restore($data['bottomtext'], $restore_mode);
        
        return ($this->settings = $data);
    }
    
    
    /**
     * Проверяет, что приложение установлено в сообществе
     * @return boolean Установлено или нет
     */
    public function is_installed() {
        
        if (!$this->vkauth->is_auth OR !$this->vkauth->is_valid_gid())
            return FALSE;
        
        if (is_null($this->settings)) {
            // SELECT * FROM settings WHERE (gid = $gid)
            $selresult = $this->db->get_where(self::$tblsettings, array('gid' => $this->vkauth->gid));

            return (count((array) $selresult->row()) > 0);
        }
        else
            return TRUE;
    }
    
    
    /**
     * Принудительно укорачивает глобальные тексты для вкладок, превышенные для действующего тарифа
     * @return array  Массив настроек сообщества с исправленными текстами
     */
    public function limit_by_plan() {
        
        if (is_null($this->settings)) 
            $this->get();
        
        $nowplan = $this->settings['plan'];
        
        if (!isset(self::$plansheet[$nowplan]))    // При ошибке возвращаем исходный массив как есть
            return $this->settings;
        
        // Обрезаем тексты до разрешенной тарифом длины
        $this->settings['toptext']    = mb_substr($this->settings['toptext'],    0, self::$plansheet[$nowplan]['ttmax']);
        $this->settings['bottomtext'] = mb_substr($this->settings['bottomtext'], 0, self::$plansheet[$nowplan]['btmax']);

        return $this->settings;
    }


    /**
     * Обновляет настройки оплаты для сообщества
     * @param  integer $yapurse  № Яндекс.Кошелька
     * @param  string  $yabutton Надпись на кнопке оплаты
     * @param  string  $yanote   Короткое символьное имя "Примечание к платежу" (аналогично списку полей ENUM в БД)
     * @return boolean  Успешное обновление
     */
    public function update_payinfo($yapurse, $yabutton, $yanote) {
        
        if (!$this->vkauth->is_auth OR !$this->vkauth->is_valid_gid())
            return FALSE;
        
        if (is_null($this->settings)) 
            $this->get();
        
        $yapurse_new = (isset($yapurse) AND $this->validate->is_yapurse($yapurse)) ? $yapurse : NULL;

        // Сохраняем новые платёжные настройки
        // UPDATE settings SET yapurse = $yapurse, ... WHERE gid = $gid
        $result = $this->db
            ->set(array(
                'yapurse'   => $yapurse_new,
                'yabutton'  => $this->validate->yabutton($yabutton),
                'yanote'    => $this->validate->yanote($yanote),
            ))
            ->where('gid', $this->vkauth->gid)
            ->update(self::$tblsettings)
        OR log_message('error', "Error DB update payment info for gid #{$this->vkauth->gid} at query: ".$this->db->last_query());
        
        // Сохраняем каждое изменение Яндекс.Кошелька в отдельной таблице с историей кошельков
        if ($yapurse_new != $this->settings['yapurse']) {
            
            // INSERT INTO updpurses (gid, uid, yapurse) VALUES ($gid, $uid, $yapurse_new)
            $this->db
                ->insert(self::$tblupdpurses, array(
                    'gid'     => $this->vkauth->gid, 
                    'uid'     => $this->vkauth->uid, 
                    'yapurse' => $yapurse_new,
                ))
            OR log_message('error', "Error DB insert purse's history for gid #{$this->vkauth->gid} at query: ".$this->db->last_query());
        }

        // Сохраняем ответ, который контроллер вернет ajax-скрипту
        // Проверяем только сохранение платёжных настроек - ошибка сохранения истории кошельков неприятна, но некритична
        $this->resp = $result ? self::$responses['saved'] : self::$responses['error_save'];
        
        $this->settings = NULL;         // Очистим закэшированные данные после обновления БД
        
        return $result;
    }


    /**
     * Обновляет общие тексты с информацией сверху и снизу формы оплаты для сообщества
     * @param  string $toptext    Общий для всех вкладок текст сверху над формой
     * @param  string $bottomtext Общий для всех вкладок текст снизу под формой
     * @param  string $ttmode     Короткое символьное имя "Режим текста над формой" (аналогично списку полей ENUM в БД)
     * @param  string $btmode     Короткое символьное имя "Режим текста под формой" (аналогично списку полей ENUM в БД)
     * @return boolean  Успех обновления
     */
    public function update_texts($toptext, $bottomtext, $ttmode, $btmode) {
        
        if (!$this->vkauth->is_auth OR !$this->vkauth->is_valid_gid())
            return FALSE;
        
        $setdata = array(
            'ttmode' => $this->validate->ttmode($ttmode),
            'btmode' => $this->validate->btmode($btmode),
        );

        // Валидируем тексты. Если поля НЕ пришли с формой, то НЕ добавляем их и НЕ затираем прежнее содержимое в БД.
        // Для дефолтного текста валидатор вернет NULL, который пишем в БД, что будет означать дефолтный текст.
        if (isset($toptext))     $setdata['toptext']    = $this->validate->toptext($toptext);
        if (isset($bottomtext))  $setdata['bottomtext'] = $this->validate->bottomtext($bottomtext);

        // UPDATE settings SET toptext = $toptext, ... WHERE gid = $gid
        $result = $this->db
            ->set($setdata)
            ->where('gid', $this->vkauth->gid)
            ->update(self::$tblsettings)
        OR log_message('error', "Error DB update text's info for gid #{$this->vkauth->gid} at query: ".$this->db->last_query());

        // Сохраняем ответ, который контроллер вернет ajax-скрипту
        $this->resp = $result ? self::$responses['saved'] : self::$responses['error_save'];
        
        $this->settings = NULL;         // Очистим закэшированные данные после обновления БД
        
        return $result;
    }


    /**
     * Изменяет состояние приложения для сообщества "включено" / "выключено" на обратное
     * @return boolean   Успех переключения
     */
    public function turn_appon() {
        
        if (!$this->vkauth->is_auth OR !$this->vkauth->is_valid_gid())
            return FALSE;

        // UPDATE settings SET appon = 1 - appon WHERE gid = $gid
        $result = $this->db
            ->set('appon', '1 - appon', FALSE)
            ->where('gid', $this->vkauth->gid)
            ->update(self::$tblsettings)
        OR log_message('error', "Error DB application's turn on/off for gid #{$this->vkauth->gid} at query: ".$this->db->last_query());
        
        $this->settings = NULL;         // Очистим закэшированные данные после обновления БД
        
        return $result;
    }


    /**
     * Возвращает состояние приложения для сообщества: включено / выключено
     * @return boolean   Флаг состояния приложения
     */
    public function is_appon() {

        $this->get();       // Перечитаем настройки сообщества
        
        return isset($this->settings['appon']) ? $this->settings['appon'] : FALSE;
    }


    /**
     * Возвращает наличие Яндекс.Кошелька в настройках сообщества
     * @return boolean   Флаг наличия Яндекс.Кошелька
     */
    public function isset_yapurse() {

        if (is_null($this->settings)) 
            $this->get();
        
        return (isset($this->settings['yapurse']) AND ($this->settings['yapurse'] > 0));
    }

}