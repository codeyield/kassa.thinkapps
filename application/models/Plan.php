<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Plan extends CI_Model {

// Массив ответа $resp, возвращаемый по цепочке: модель -> контроллер -> ajax-скрипт
public $resp = [];

// Таблицы в БД
protected static $tblsettings;
protected static $tblupdplans;

// Используемые параметры конфига
protected static $plansheet;
protected static $trialdays;
protected static $trial_id;
protected static $free_id;
protected static $tabs_config;
protected static $responses;

// Внутренний кэш экземпляра класса с тарифными настройками сообщества
protected $planset = NULL;


	public function __construct() {
        parent::__construct();

        // Загрузка конфигов в модели
        $CI =& get_instance();
        $CI->config->load('ukonfig', TRUE);
        $ukonfig = $CI->config->item('ukonfig');

        self::$tblsettings = $ukonfig['dbtbl']['settings'];
        self::$tblupdplans = $ukonfig['dbtbl']['updplans'];
        
        self::$plansheet   = $ukonfig['plans']['sheet'];
        self::$trialdays   = $ukonfig['plans']['trialdays'];
        self::$trial_id    = $ukonfig['plans']['trial_id'];
        self::$free_id     = $ukonfig['plans']['free_id'];
        self::$tabs_config = $ukonfig['tabs'];
        self::$responses   = $ukonfig['resp'];

        // Отключаем строгий режим транзакций
        $this->db->trans_strict(FALSE);

        // Получаем параметры запуска приложения от ВК или из сессии
        $this->load->model('vkauth');
        $this->vkauth->get();
	}
    
    
    /**
     * Возвращает настройки тарифа для сообщества, а также сохраняет их во внутреннем кэше экземпляра класса
     * @return array Массив настроек сообщества
     * @protected
     */
    public function get_planset() {
        
        if (!$this->vkauth->is_valid_gid())
            return FALSE;
        
        // SELECT plan,nextplan,planexpiry FROM settings WHERE (gid = $gid)
        $selresult = $this->db
            ->select('plan,nextplan,planexpiry')
            ->where(array('gid' => $this->vkauth->gid))
            ->get(self::$tblsettings);

        $this->planset = (array) $selresult->row();

        // Дополняем массив настроек параметрами тарифа из конфига
        if (isset($this->planset['plan'])) {
            $nowplan = $this->planset['plan'];

            $this->planset['name']      = self::$plansheet[$nowplan]['name'];
            $this->planset['price']     = self::$plansheet[$nowplan]['price'];
            $this->planset['tabsum']    = self::$plansheet[$nowplan]['tabsum'];
            $this->planset['maxtabs']   = self::$plansheet[$nowplan]['maxtabs'];
            $this->planset['ttmax']     = self::$plansheet[$nowplan]['ttmax'];
            $this->planset['btmax']     = self::$plansheet[$nowplan]['btmax'];
            $this->planset['optslock']  = self::$plansheet[$nowplan]['optslock'];
        }
        
        return (!is_null($this->planset) AND (count($this->planset) > 0)) ? $this->planset : FALSE;
    }

    
    /**
     * Проверяет наступление очередного платежного периода и в случае, если она наступила:
     *  1. Списывает сумму оплаты тарифа (для платных)
     *  2. Автоматически обновляет текущий и следующий тарифы
     *  3. Заменяет тариф на бесплатный, если средств на балансе недостаточно для оплаты текущего тарифа
     *  
     * @return boolean/NULL Успешная смена тарифа или NULL, если время смены тарифа еще не пришло
     */
    public function update_auto() {
        
        if (!$this->vkauth->is_auth OR !$this->vkauth->is_valid_gid())
            return FALSE;
        
        if (is_null($this->planset))    // Получаем настройки тарифа во внутренний кэш $this->planset
            $this->get_planset();
            
        $nowplan   = $this->planset['plan'];                // Текущий тариф
        $nextplan  = $this->planset['nextplan'];            // Тариф на следующий период
        $nextprice = self::$plansheet[$nextplan]['price'];  // Сумма оплаты за следующий тариф

        // Наступило время смены тарифа (КРОМЕ free - его не продляем и счета по нему не выставляем)
        if ($nowplan AND ($nowplan != self::$free_id) AND (time() >= strtotime($this->planset['planexpiry']))) {

            $this->db->trans_start();                   // DB start transaction

                // Юзер заказал ПЛАТНЫЙ тариф на следующий (по факту наступивший) период
                if ($nextprice > 0) {
                    $this->load->model('billing');

                    if ($this->billing->is_amount_enough($nextprice)) 
                        // Достаточно средств для оплаты тарифа - списываем сумму с баланса
                        $this->billing->debit_auto_plan($nextprice, $this->planset['planexpiry']);
                    else
                        // Недостаточно средств для оплаты - принудительно заменяем тариф на free
                        $nextplan = self::$free_id;
                }

                // Добавляем 1 месяц к предыдущей дате обновления тарифа
                $planexpiry = $this->date_add_1month($this->planset['planexpiry']);

                // Обновляем текущий и следующий тариф, продляем дату на 1 месяц
                // Также однократный переход с ТРИАЛА на бесплатный или платный
                $this->update($nextplan, $nextplan, $planexpiry);

                // Сохраняем данные об обновлении в истории смены тарифов
                $this->update_history($nextplan, $planexpiry);

            $result = $this->db->trans_complete()      // DB commit
                OR log_message('error', "Error DB update plan's data for gid #{$this->vkauth->gid}: {$nowplan} => {$nextplan} at bill date {$planexpiry}");
            
            return $result;
        }

        // Еще не пришло время смены тарифа
        return NULL;
    }


    /**
     * Реализует логику обновления тарифа на основании запроса пользователя
     * 
     * ВВЕРХ - НЕМЕДЛЕННОЕ ОБНОВЛЕНИЕ ТЕКУЩЕГО ТАРИФА:
     *  ТРИАЛ   => _ЛЮБОЙ_: plan = [НЕ_ОБНОВЛЯЕМ]  / nextplan = $newplan  / planexpiry = [НЕ_ОБНОВЛЯЕМ]
     *  ФРИ     => ПЛАТНЫЙ: plan = $newplan  / nextplan = $newplan  / planexpiry = now() +1 month
     *  ПЛАТНЫЙ => ПЛАТНЫЙ: plan = $newplan  / nextplan = $newplan  / planexpiry = [НЕ_ОБНОВЛЯЕМ]
     *  
     * ВНИЗ - ЗАМЕНА ТАРИФА НА СЛЕДУЮЩИЙ ПЕРИОД:
     *  ПЛАТНЫЙ => ФРИ:     plan = [НЕ_ОБНОВЛЯЕМ]  / nextplan = $newplan  / planexpiry = [НЕ_ОБНОВЛЯЕМ]
     *  ПЛАТНЫЙ => ПЛАТНЫЙ: plan = [НЕ_ОБНОВЛЯЕМ]  / nextplan = $newplan  / planexpiry = [НЕ_ОБНОВЛЯЕМ]
     * 
     * @param  string  [$newplan = FALSE] Код тарифа на замену
     * @return boolean Успех обновления
     */
    public function update_by_user($newplan = FALSE) {
       
        if (!$this->vkauth->is_auth OR !$this->vkauth->is_valid_gid() OR !$this->is_valid_id($newplan))
            return FALSE;
        
        if (is_null($this->planset))    // Получаем настройки тарифа во внутренний кэш $this->planset
            $this->get_planset();
        
        $nowplan  = $this->planset['plan'];                 // Текущий тариф
        $nextplan = $this->planset['nextplan'];             // Тариф на следующий период
        $nowprice = $this->planset['price'];                // Сумма оплаты за текущий тариф
        $newprice = self::$plansheet[$newplan]['price'];    // Сумма оплаты за НОВЫЙ тариф
        $newname  = self::$plansheet[$newplan]['name'];     // Кириллическое название НОВОГО тарифа
        $updprice = $newprice - $nowprice;                  // Сумма доплаты при смене тарифа

        // Нельзя изменить тариф на триальный ИЛИ сам на на себя (но только когда текущий тариф = следующий)
        if (($newplan == self::$trial_id) || (($nextplan == $nowplan) && ($newplan == $nowplan)) ) {
            $this->resp = self::$responses['error_plan'];
            return FALSE;
        }
        
        // Обновления тарифа на СЛЕДУЮЩИЙ период без финансовых операций
        if (($nowplan == self::$trial_id)                           // ТРИАЛ => ЛЮБОЙ
            || (($nowprice > 0) && ($newplan == self::$free_id))    // ВНИЗ ПЛАТНЫЙ => ФРИ
            || (($newprice < $nowprice) && ($newprice > 0))         // ВНИЗ ПЛАТНЫЙ => ПЛАТНЫЙ
            || (($nextplan != $nowplan) && ($newplan == $nowplan))  // ЛЮБОЙ СЛЕДУЮЩИЙ => ТЕКУЩИЙ (возврат к текущему на след. период)
           ) {
            $planexpiry = $this->planset['planexpiry'];
            $result = $this->update_next($newplan);
        }
        // Обновляем ФРИ => ПЛАТНЫЙ
        elseif (($newprice > 0) && ($nowplan == self::$free_id)) {
            $this->load->model('billing');
            
            // Достаточно средств для оплаты тарифа - списываем сумму с баланса
            if ($this->billing->is_amount_enough($newprice)) {
                
                $planexpiry = $this->date_add_1month('now');
                $note = "Оплата тарифа {$newname}";
                
                $this->db->trans_start();                   // DB start transaction
                    $this->billing->debit_by_user($newprice, $note);
                    $this->update($newplan, $newplan, $planexpiry);
                $result = $this->db->trans_complete();      // DB commit
            }
            // Недостаточно средств для оплаты - возвращаем ошибку
            else {
                $this->resp = self::$responses['error_notenough'];
                return FALSE;
            }
        }
        // Обновляем ВВЕРХ ПЛАТНЫЙ => ПЛАТНЫЙ
        elseif (($newprice > $nowprice) && ($nowprice > 0)) {
            $this->load->model('billing');
            
            // Достаточно средств для смены тарифа - списываем сумму с баланса
            if ($this->billing->is_amount_enough($updprice)) {
                
                $planexpiry = $this->planset['planexpiry'];
                $note = "Доплата за смену тарифа {$this->planset['name']} на {$newname}";
                
                $this->db->trans_start();                   // DB start transaction
                    $this->billing->debit_by_user($updprice, $note);
                    $this->update($newplan, $newplan);
                $result = $this->db->trans_complete();      // DB commit
            }
            // Недостаточно средств для оплаты - возвращаем ошибку
            else {
                $this->resp = self::$responses['error_notenough'];
                return FALSE;
            }
        }
        // Нераспознанный вариант смены тарифа
        else { 
            $this->resp = self::$responses['error_something'];
            $result = FALSE;
        }
        
        // Сохраняем данные об обновлении в истории смены тарифов
        if ($result)
            $this->update_history($newplan, $planexpiry);
        
        $this->resp = $result ? self::$responses['saved_plan'] : self::$responses['error_plan'];
        
        return $result;
    }


    /**
     * Реализует логику обновления тарифа на основании запроса пользователя
     * 
     * @param  string  [$newplan = FALSE] Код тарифа на замену
     * @return boolean Успех обновления
     */
    public function confirm_plan($newplan = FALSE) {
       
        if (!$this->vkauth->is_auth OR !$this->vkauth->is_valid_gid() OR !$this->is_valid_id($newplan))
            return FALSE;
        
        if (is_null($this->planset))    // Получаем настройки тарифа во внутренний кэш $this->planset
            $this->get_planset();
        
        $data = array();
        
        $nowplan  = $this->planset['plan'];                 // Текущий тариф
        $nextplan = $this->planset['nextplan'];             // Тариф на следующий период
        $nowprice = $this->planset['price'];                // Сумма оплаты за текущий тариф
        $newprice = self::$plansheet[$newplan]['price'];    // Сумма оплаты за НОВЫЙ тариф
        $updprice = $newprice - $nowprice;                  // Сумма доплаты при смене тарифа

        // Нельзя изменить тариф на триальный ИЛИ сам на на себя (но только когда текущий тариф = следующий)
        if (($newplan == self::$trial_id) || (($nextplan == $nowplan) && ($newplan == $nowplan)) ) {
            $data['confirm'] = FALSE;
            $data['text']    = "Недопустимая смена тарифа!";
        }
        // Обновления тарифа на СЛЕДУЮЩИЙ период
        elseif (($nowplan == self::$trial_id) ||                    // ТРИАЛ => ЛЮБОЙ
            (($nowprice > 0) && ($newplan == self::$free_id)) ||    // ВНИЗ ПЛАТНЫЙ => ФРИ
            (($newprice < $nowprice) && ($newprice > 0))            // ВНИЗ ПЛАТНЫЙ => ПЛАТНЫЙ
           ) {
            $data['confirm'] = TRUE;
            $data['text']    = "Выбранный тариф вступит в действие с ".date('d.m.Y', strtotime($this->planset['planexpiry'])).". Продолжить?";
        }
        // Обновляем ЛЮБОЙ СЛЕДУЮЩИЙ => ТЕКУЩИЙ (т.е. возвращаемся к текущему на след. период)
        elseif (($nextplan != $nowplan) && ($newplan == $nowplan)) {
            $data['confirm'] = TRUE;
            $data['text']    = "Желаете вернуться к текущему тарифу?";
        }
        // Обновляем ФРИ => ПЛАТНЫЙ
        elseif (($newprice > 0) && ($nowplan == self::$free_id)) {
            $this->load->model('billing');
            
            if ($this->billing->is_amount_enough($newprice)) {      // Достаточно средств для оплаты тарифа
                $data['confirm'] = TRUE;
                $data['text']    = "С вашего баланса будет списано {$newprice}р. за выбранный тариф.";
            }
            else {                                                  // Недостаточно средств для оплаты
                $data['confirm'] = FALSE;
                $data['text']    = "Недостаточно средств! Пополните баланс и повторите операцию.";
            }
        }
        // Обновляем ВВЕРХ ПЛАТНЫЙ => ПЛАТНЫЙ
        elseif (($newprice > $nowprice) && ($nowprice > 0)) {
            $this->load->model('billing');
            
            if ($this->billing->is_amount_enough($updprice)) {  // Достаточно средств для смены тарифа
                $data['confirm'] = TRUE;
                $data['text']    = "С вашего баланса будет списано {$updprice}р. за смену тарифа. Продолжить?";
            }
            else {                                                  // Недостаточно средств для оплаты
                $data['confirm'] = FALSE;
                $data['text']    = "Недостаточно средств! Пополните баланс и повторите операцию.";
            }
        }
        
        return $data;
    }
    
    
    /**
     * Универсальный метод обновления тарифа. Пропущенные или невалидные аргументы НЕ будут изменены в БД!
     * @param  string      $newplan Код нового тарифа
     * @return $this->resp Ответ для ajax-скрипта в формате массива $config['resp'] 
     * @return boolean     Успех обновления
     */
    public function update($nowplan = NULL, $newplan = NULL, $planexpiry = NULL) {
        
        if (!$this->vkauth->is_auth OR !$this->vkauth->is_valid_gid())
            return FALSE;

        // Заполняем массив для БД только валидными значениями
        $plandata = [];
        if ($this->is_valid_id($nowplan)) $plandata['plan']       = $nowplan;
        if ($this->is_valid_id($newplan)) $plandata['nextplan']   = $newplan;
        if (strtotime($planexpiry))       $plandata['planexpiry'] = $planexpiry;
        
        if (count($plandata) == 0)
            return FALSE;

        // Обновляем тарифные данные в настройках сообщества
        // UPDATE settings SET (...) WHERE gid = $gid
        $updresult = $this->db
            ->set($plandata)
            ->where('gid', $this->vkauth->gid)
            ->update(self::$tblsettings)
        OR log_message('error', "Error DB update plan's data for gid #{$this->vkauth->gid} at query: ".$this->db->last_query());
        
        $this->planset = NULL;         // Очистим закэшированные данные после обновления БД

        return $updresult;
    }
    
    
    /**
     * Заменяет тариф на следующий период (немедленного обновления тарифа не происходит)
     * @param  string      [$newplan = FALSE] Код тарифа на следующий платёжный период
     * @return $this->resp Ответ для ajax-скрипта в формате массива $config['resp'] 
     * @return boolean     Успешное обновление в БД
     */
    public function update_next($newplan = FALSE) {
        
        if (!$this->vkauth->is_auth OR !$this->vkauth->is_valid_gid() OR !$this->is_valid_id($newplan))
            return FALSE;

        // Обновляем тарифные данные в настройках сообщества
        // UPDATE settings SET nextplan = $newplan WHERE gid = $gid
        $updresult = $this->db
            ->set('nextplan', $newplan)
            ->where('gid', $this->vkauth->gid)
            ->update(self::$tblsettings)
        OR log_message('error', "Error DB update plan's data for gid #{$this->vkauth->gid} at query: ".$this->db->last_query());
       
        $this->planset = NULL;         // Очистим закэшированные данные после обновления БД

        return $updresult;
    }

    
    /**
     * Продлевает срок окончания текущего тарифа на указанное число дней
     * @param  integer [$days = 0] Число дней продления тарифа
     * @return boolean Успешное обновление в БД
     */
    public function prolong($days = 0) {
        
        if (!$this->vkauth->is_auth OR !$this->vkauth->is_valid_gid())
            return FALSE;
        
        // Ограничиваем 1 год на всякий случай. В конфиг этот лимит не вынесен, т.к. его превышение явный форс-мажор
        $days = (int) $days;
        $days = $days < 0 ? 0 : ($days > 365 ? 365 : $days);
        
        // UPDATE settings SET planexpiry = DATE_ADD(planexpiry, INTERVAL N DAY) WHERE gid = $gid
        $updresult = $this->db
            ->set('planexpiry', "DATE_ADD(planexpiry, INTERVAL {$days} DAY)", FALSE)
            ->where('gid', $this->vkauth->gid)
            ->update(self::$tblsettings)
        OR log_message('error', "Error DB update plan's data for gid #{$this->vkauth->gid} at query: ".$this->db->last_query());
        
        $this->planset = NULL;         // Очистим закэшированные данные после обновления БД
        
        // Переполучаем актуальные данные из настроек сообщества и сохраняем запись о продлении в истории тарифов
        $this->update_history();
        
        return $updresult;
    }
    
    
    /**
     * Сохраняет данные в истории обновлений тарифов
     * @param  string  $nowplan    Код тарифа
     * @param  string  $planexpiry Строка даты в формате MySQL
     * @return boolean Успешное обновление в БД
     */
    public function update_history($nowplan = FALSE, $planexpiry = FALSE) {
        
        if (!$this->vkauth->is_auth OR !$this->vkauth->is_valid_gid())
            return FALSE;
        
        if (is_null($this->planset))    // Получаем настройки тарифа во внутренний кэш $this->planset
            $this->get_planset();
        
        // Если $nowplan не задан (невалиден), получаем его из настроек сообщества
        if (!$this->is_valid_id($nowplan))
            $nowplan = $this->planset['plan'];
        
        // Если $planexpiry не задан (невалиден), получаем его из настроек сообщества
        if (strtotime($planexpiry) === FALSE) 
            $planexpiry = $this->planset['planexpiry'];
        
        // Сохраняем данные об обновлении тарифа в истории тарифов
        // INSERT INTO updplans (gid, uid, plan, nextupdate) VALUES ($gid, $uid, $nowplan, $planexpiry)
        $insresult = $this->db
            ->insert(self::$tblupdplans, array(
                'gid'        => $this->vkauth->gid, 
                'uid'        => $this->vkauth->uid, 
                'plan'       => $nowplan,
                'nextupdate' => $planexpiry,
            ))
        OR log_message('error', "Error DB insert plan's data for gid #{$this->vkauth->gid} at query: ".$this->db->last_query());
        
        return $insresult;
    }
    
    
    /**
     * Возвращает тарифозависимые настройки для вкладок. Или дефолтные параметры, если биллинг выключен
     * @return array Массив с настройками для вкладок
     */
    public function tablimits() {
        
        if (is_null($this->planset))    // Получаем настройки тарифа во внутренний кэш $this->planset
            $this->get_planset();
        
        return array(
            'maxtabs'  => self::$tabs_config['maxtabs'],
            'mintitle' => self::$tabs_config['title_minlen'],
            'maxtitle' => self::$tabs_config['title_maxlen'],
            'optslock' => (BILLING ? $this->planset['optslock'] : FALSE),   // Если биллинг выключен, то все опции доступны
            'maxsum'   => (BILLING ? $this->planset['tabsum']   : self::$tabs_config['maxsum']),
            'ttmax'    => (BILLING ? $this->planset['ttmax']    : self::$tabs_config['ttmax']),
            'btmax'    => (BILLING ? $this->planset['btmax']    : self::$tabs_config['btmax']),
        );
    }
    
    
    /***** Короткие проверки, получение тарифных списков для валидации, константы тарифов *****/
    
    /**
     * Возвращает стоимость оплаты тарифа на следующий период
     * @param  string $plan_id Код тарифа
     * @return integer Стоимость оплаты тарифа
     */
    public function get_next_price() {
        
        if (is_null($this->planset))    // Получаем настройки тарифа во внутренний кэш $this->planset
            $this->get_planset();
        
        return self::$plansheet[$this->planset['nextplan']]['price'];
    }
    
    /**
     * Проверяет код тарифа на валидность
     * @param  string $plan_id Код тарифа
     * @return boolean Валидный или нет
     */
    public function is_valid_id($plan_id) {
        return in_array($plan_id, array_keys(self::$plansheet));
    }

    /**
     * Возвращает список кодов всех тарифов, КРОМЕ триального (юзается для валидации формы)
     * @return array Линейный массив со списком кодов
     */
    public function get_all_ids_exclude_trial() {
        return array_diff(array_keys(self::$plansheet), array(self::$trial_id));
    }
    
    /**
     * Возвращает код бесплатного тарифа
     * @return string Код тарифа
     */
    public function free_id() {
        return self::$free_id;
    }

    /**
     * Возвращает код пробного тарифа
     * @return string Код тарифа
     */
    public function trial_id() {
        return self::$trial_id;
    }

    
    /***** Внутренние методы класса *****/
    
    /**
     * Добавляет 1 месяц к исходной дате
     * @param  string $fromdate Входящая дата
     * @return string Отформатированная дата
     */
    protected function date_add_1month($fromdate) {
        
        // Добавляем 1 месяц к предыдущей дате обновления тарифа
        $expdate = new DateTime($fromdate);
        $expdate->add(new DateInterval('P1M'));     // +1 месяц
        return $expdate->format('Y-m-d');
    }
    
}