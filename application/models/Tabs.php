<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tabs extends CI_Model {
    
// Параметры, используемые вызывающим контроллером
public $resp = [];              // Массив ответа $resp, возвращаемый по цепочке: модель -> контроллер -> ajax-скрипт
public $newtid;                 // Новый id вкладки, на которую переключимся после операций со вкладками
public $newparenttid;           // Новый id родительской вкладки, на которую переключимся после операций со вкладками

// Таблица БД со вкладками оплаты для всех сообществ
protected static $tbltabs;

// Используемые параметры конфига
protected static $responses;
protected static $maxtabs;
protected static $mintitle;
protected static $maxtitle;
protected static $qlimit;
protected static $maxsum;
protected static $ttmax;
protected static $btmax;
    
// Внутренний массив с выборкой вкладок для protected-методов (валидация и получение данных о вкладках)
protected $rawtabs = [];

// Внутренний кэш экземпляра класса с данными вкладок
protected $tabs = NULL;


	public function __construct() {
        parent::__construct();

        // Загрузка конфигов в модели
        $CI =& get_instance();
        $CI->config->load('ukonfig', TRUE);
        $ukonfig = $CI->config->item('ukonfig');

        self::$tbltabs    = $ukonfig['dbtbl']['tabs'];
        
        self::$responses  = $ukonfig['resp'];
        self::$maxtabs    = $ukonfig['tabs']['maxtabs'];
        self::$mintitle   = $ukonfig['tabs']['title_minlen'];
        self::$maxtitle   = $ukonfig['tabs']['title_maxlen'];
        self::$qlimit     = $ukonfig['tabs']['qlimit'];
        self::$maxsum     = $ukonfig['tabs']['maxsum'];
        self::$ttmax      = $ukonfig['tabs']['ttmax'];
        self::$btmax      = $ukonfig['tabs']['btmax'];

        // Отключаем строгий режим транзакций
        $this->db->trans_strict(FALSE);

        // Получаем параметры запуска приложения от ВК или из сессии
        $this->load->model('vkauth');
        $this->vkauth->get();
        
        $this->load->is_loaded('validate') OR $this->load->library('validate', $ukonfig);
	}

    
    /**
     * Возврашает древовидный массив с содержимым вкладок для шаблона отображения.
     * Также инициализирует демо-пример вкладок для нового gid, если они еще не существуют в БД.
     * @param  integer $restore_mode     Режим восстановления текста: для вывода на html-страницу / для редактирования в форме
     * @param  integer $tid_active       id вкладки для пометки атрибутом active в bootstrap-коде
     * @param  integer $parenttid_active id родительской вкладки для пометки атрибутом active в bootstrap-коде
     * @return array   Древовидный массив с содержимым вкладок
     */
    public function get($restore_mode = TEXT_RESTORE_FORM, $tid_active = 1, $parenttid_active = 1) {
        
        if (!$this->vkauth->is_auth OR !$this->vkauth->is_valid_gid())
            return FALSE;
        
        $tid_active       = $this->validate->tid($tid_active);
        $parenttid_active = $this->validate->parenttid($parenttid_active);

            // Анонимная функция для usort для сортировки массива по возрастанию значения tid
            $cmp_by_tids_asc = function($item2, $item1) {
                $diff = $item2['tid'] - $item1['tid'];
                return $diff == 0 ? 0 : ($diff > 0 ? 1 : -1);
            };
        
        // Получаем набор вкладок для сообщества
        // SELECT * FROM tabs WHERE (gid = $gid) LIMIT $qlimit
        $selresult = $this->db->get_where(self::$tbltabs, array('gid' => $this->vkauth->gid), self::$qlimit);
        $items = $selresult->result_array();
        
        // Инициализация нового gid и создание демо-набора вкладок - но только если приложение запустил админ!
        if ((count($items) == 0) && $this->vkauth->is_admin) {
            
            // Обязательно переполучать массив $rawtabs, если идет несколько методов добавления вкладок подряд
            $this->_get_rawtabs(); $this->init_tab('Создание сайта [демо]');
            $this->_get_rawtabs(); $this->add_tab_right(1, 0, 'Дизайн [демо]');
            $this->_get_rawtabs(); $this->add_tab_right(2, 0, 'Копирайтинг [демо]');
            $this->_get_rawtabs(); $this->add_level(1, 0, 'Медленно и дорого');
            $this->_get_rawtabs(); $this->add_tab_right(1, 1, 'Дешёво и быстро');
            
            // Повторная попытка прочитать то, что только что добавили
            $selresult = $this->db->get_where(self::$tbltabs, array('gid' => $this->vkauth->gid), self::$qlimit);
            $items = $selresult->result_array();
        }
        
        // Что-то пошло не так, инициализация не удалась
        if (count($items) == 0) {
            log_message('error', "Error DB create tabs for gid #{$this->vkauth->gid} at query: ".$this->db->last_query());
            return FALSE;
        }

        // Пересобираем полученную из БД таблицу в древовидный массив для шаблона отображения
        $this->tabs = array();
        foreach ($items as $parent) {
            if ($parent['parenttid'] == 0) {
                $parenttid = $parent['tid'];
                
                $childdata = [];
                foreach ($items as $child) {
                    if (($child['parenttid'] > 0) && ($child['parenttid'] == $parenttid)) {
                        $childdata[] = array(
                            'tid'       => $child['tid'],
                            'parenttid' => $child['parenttid'],
                            'title'     => $child['title'],
                            'toptext'   => $this->validate->text_restore($child['toptext'],    $restore_mode),
                            'bottomtext'=> $this->validate->text_restore($child['bottomtext'], $restore_mode),
                            'sum'       => $child['sum'],
                            'editsum'   => $child['editsum'],
                            'paylock'   => $child['paylock'],
                            // Динамически формируемые поля для вкладки 2-го уровня
                            'target'    => $parent['title']." - ".$child['title'],                               // Название перевода в Яндекс-форме
                            'mixtitle'  => $parent['title']." - ".$child['title']." - ".$child['sum']."р.",               // Заголовок формы оплаты
                            'paynote1'  => $parent['title']." - ".$child['title']." - от ".$this->vkauth->screen_name,    // Примечание к платежу #1
                        );
                    }
                }

                // Сортируем вкладки 2-го уровня по возрастанию значения tid
                usort($childdata, $cmp_by_tids_asc);

                $this->tabs[$parenttid] = array(
                    'tid'       => $parent['tid'],
                    'parenttid' => 0,
                    'title'     => $parent['title'],
                    'toptext'   => $this->validate->text_restore($parent['toptext'],    $restore_mode),
                    'bottomtext'=> $this->validate->text_restore($parent['bottomtext'], $restore_mode),
                    'sum'       => $parent['sum'],
                    'editsum'   => $parent['editsum'],
                    'paylock'   => $parent['paylock'],
                    // Динамически формируемые поля для вкладки 1-го уровня
                    'target'    => $parent['title'],                                            // Название перевода в Яндекс-форме
                    'mixtitle'  => $parent['title']." - ".$parent['sum']."р.",                  // Заголовок формы оплаты
                    'paynote1'  => $parent['title']." - от ".$this->vkauth->screen_name,        // Примечание к платежу #1
                );
                
                // Добавляем вложенный массив с дочерними вкладками (если есть)
                if (count($childdata) > 0)
                    $this->tabs[$parenttid]['child'] = $childdata;
            }
        }

        // Сортируем вкладки 1-го уровня по возрастанию значения tid
        usort($this->tabs, $cmp_by_tids_asc);
        
        
        // Добавляем атрибуты 'active' для вкладок, которые д.б. помечены им в Bootstrap-коде
        foreach ($this->tabs as $pkey => $parent) {
            if ( (($parent['tid'] == $tid_active)       && ($parenttid_active == 0)) ||
                 (($parent['tid'] == $parenttid_active) && ($parenttid_active > 0))
               )
                $this->tabs[$pkey]['active'] = TRUE;

            if (isset($parent['child'])) {
                foreach ($parent['child'] as $ckey => $child) {
                    if ( (($child['tid'] == 1)           && ($child['parenttid'] != $parenttid_active)) ||
                         (($child['tid'] == $tid_active) && ($child['parenttid'] == $parenttid_active))
                       )
                        $this->tabs[$pkey]['child'][$ckey]['active'] = TRUE;
                }
            }
        }
        
        return (count($this->tabs) > 0) ? $this->tabs : FALSE;
    }


    /**
     * Ставит отметки на вкладках и принудительно переопределяет значения, превышенные для указанного тарифа
     * @param  integer $restore_mode     Режим восстановления текста: для вывода на html-страницу / для редактирования в форме
     * @param  integer $tid_active       id вкладки для пометки атрибутом active в bootstrap-коде
     * @param  integer $parenttid_active id родительской вкладки для пометки атрибутом active в bootstrap-коде
     * @return array  Исправленный массив с содержимым вкладок
     */
    public function limit_by_plan($restore_mode = TEXT_RESTORE_FORM, $tid_active = 1, $parenttid_active = 1) {
        
        $this->load->is_loaded('plan') OR $this->load->model('plan');
        
        // Получаем настройки тарифа для сообщества, включая данные конфига с тарифными ограничениями
        $planset = $this->plan->get_planset();
        
        // Аргументы restore_mode, tid_active, parenttid_active юзаются только для сквозной передачи в $this->get
        if (is_null($this->tabs)) 
            $this->get($restore_mode, $tid_active, $parenttid_active);      // Результат в кэше $this->tabs
        
        // При невалидном коде тарифа возвращаем вкладки как есть. При ошибке $this->get возвращаем FALSE
        if (($planset === FALSE) OR ($this->tabs === FALSE))
            return $this->tabs;
        
        $qtytabs = 0;                              // Сквозной счетчик вкладок
        
        // Проверяем превышения параметров и ставим соотв. отметки на вкладках
        foreach ($this->tabs as $pkey => $parent) {
            
            // Проверяем вкладку верхнего уровня
            // Обрезаем тексты до разрешенной тарифом длины
            $parent['toptext']    = mb_substr($parent['toptext'],    0, $planset['ttmax']);
            $parent['bottomtext'] = mb_substr($parent['bottomtext'], 0, $planset['btmax']);

            // Принудительно переопределяем опции вкладки (если тариф ограничивает их использование)
            if ($planset['optslock']) {
                $parent['editsum'] = TRUE;
                $parent['paylock'] = FALSE;
            }

            // Проверяем параметры тарифа на предмет превышения разрешенных значений
            if ($parent['sum'] > $planset['tabsum']) 
                $parent['exceed'] = TRUE;
            else
                ++$qtytabs;     // Считаем валидные вкладки, на которых НЕ превышены ограничения тарифа

            // Ставим отметки на всех оставшихся вкладках, если превышено их разрешенное тарифом кол-во
            if ($qtytabs > $planset['maxtabs']) 
                $parent['exceed'] = TRUE;

            $this->tabs[$pkey] = $parent;        // Сохраняем измененённую вкладку в исходный массив
            
            // Проходим по вложенному уровню вкладок (если он есть)
            if (isset($parent['child'])) {
                foreach ($parent['child'] as $ckey => $child) {
            
                    // Обрезаем тексты до разрешенной тарифом длины
                    $child['toptext']    = mb_substr($child['toptext'],    0, $planset['ttmax']);
                    $child['bottomtext'] = mb_substr($child['bottomtext'], 0, $planset['btmax']);

                    // Принудительно переопределяем опции вкладки (если тариф ограничивает их использование)
                    if ($planset['optslock']) {
                        $child['editsum'] = TRUE;
                        $child['paylock'] = FALSE;
                    }

                    // Проверяем параметры тарифа на предмет превышения разрешенных значений
                    if ($child['sum'] > $planset['tabsum']) 
                        $child['exceed'] = TRUE;
                    else
                        ++$qtytabs;     // Считаем валидные вкладки, на которых НЕ превышены ограничения тарифа

                    // Ставим отметки на всех оставшихся вкладках, если превышено их разрешенное тарифом кол-во
                    if ($qtytabs > $planset['maxtabs'])
                        $child['exceed'] = TRUE;

                    $this->tabs[$pkey]['child'][$ckey] = $child;    // Сохраняем измененённую вкладку в исходный массив
                }
            
            }
        }
     
        return $this->tabs;
    }
    
    
    /**
     * Проверяет наличие вкладок с превышениями разрешенных значений тарифа
     * @param  array   $tabs Массив вкладок, полученный из метода get
     * @return boolean Превышено или нет
     */
    public function is_exceeded($tabs) {
        
        foreach ($tabs as $parent) {
            if (isset($parent['exceed']))
                return TRUE;

            if (isset($parent['child'])) {
                foreach ($parent['child'] as $child) {
                    if (isset($child['exceed']))
                        return TRUE;
                }
            }
        }
        
        return FALSE;
    }
    
    
    /**
     * Обновляет настройки вкладки
     * @param  integer $tid        id вкладки
     * @param  integer $parenttid  id родительской вкладки (0 для вкладки 1-го уровня)
     * @param  integer $sum        Сумма оплаты
     * @param  integer $editsum    Разрешает плательщику изменить сумму оплаты
     * @param  integer $paylock    Блокировка кнопки оплаты
     * @param  string  $toptext    Текст сверху над формой оплаты
     * @param  string  $bottomtext Текст снизу под формой оплаты
     * @return boolean Успешный результат операции
     */
    public function update($tid = FALSE, $parenttid = FALSE, $sum = 1, $editsum = 0, $paylock = 0, $toptext = FALSE, $bottomtext = FALSE) {
        
        if ( !$this->_is_valid_tids($tid, $parenttid) OR !$this->_is_valid_tab($tid, $parenttid)) 
            return !($this->resp = self::$responses['error_something']);    // return FALSE;
        
        // Начинаем заполнять SET-массив для UPDATE
        $setdata = array();
        $setdata['sum']     = isset($sum) ? (int) $sum : 1;
        $setdata['editsum'] = isset($editsum) ? 1 : 0;      // Приводим к 0 или 1, т.к. в БД поле не булево, а TINYINT
        $setdata['paylock'] = isset($paylock) ? 1 : 0;      // Приводим к 0 или 1, т.к. в БД поле не булево, а TINYINT
        
        // Сумма оплаты не может быть равна нулю
        if ($setdata['sum'] == 0) 
            return !($this->resp = self::$responses['error_zerosum']);      // return FALSE;
        
        // Если текстовые поля НЕ пришли с формой, то НЕ добавляем их в $setdata и НЕ затираем прежнее содержимое в БД
        if (isset($toptext))     $setdata['toptext']    = $this->validate->toptext($toptext);
        if (isset($bottomtext))  $setdata['bottomtext'] = $this->validate->bottomtext($bottomtext);

        // UPDATE tabs SET sum = $sum, ... WHERE (gid = $gid) AND ('tid' = $tid) AND ('parenttid' = $parenttid)
        $result = $this->db
            ->set($setdata)
            ->where(array(
                'gid'       => $this->vkauth->gid, 
                'tid'       => $tid, 
                'parenttid' => $parenttid
            ))
            ->update(self::$tbltabs)
        OR log_message('error', "Error DB update tabs for gid #{$this->vkauth->gid} at query: ".$this->db->last_query());

        $this->resp = $result ? self::$responses['saved'] : self::$responses['error_save'];
        
        return $result;
    }

    
    /**
     * Обновляет заголовок вкладки
     * @param  array $post $_POST-массив
     * @return boolean Успешный результат операции
     */
    public function update_title($tid = FALSE, $parenttid = FALSE, $title = FALSE) {
        
        if ( !$this->_is_valid_tids($tid, $parenttid) OR !$this->_is_valid_tab($tid, $parenttid)) 
            return !($this->resp = self::$responses['error_something']);    // return FALSE;

        // UPDATE tabs SET title = $title WHERE (gid = $gid) AND ('tid' = $tid) AND ('parenttid' = $parenttid)
        $result = $this->db
            ->set(array('title' => $this->validate->tabtitle($title)))
            ->where(array(
                'gid'       => $this->vkauth->gid, 
                'tid'       => $tid, 
                'parenttid' => $parenttid
            ))
            ->update(self::$tbltabs)
        OR log_message('error', "Error DB update title for gid #{$this->vkauth->gid} at query: ".$this->db->last_query());

        $this->resp = $result ? self::$responses['saved'] : self::$responses['error_save'];
        
        return $result;
    }


    /***********************************************************************************************
     * Набор методов для операций со вкладками
	 * 
	 * Во всех методах (чтобы не повторяться):
	 * @param  integer [$tid = FALSE]       id вкладки
	 * @param  integer [$parenttid = FALSE] id родительской вкладки (0 для вкладки 1-го уровня)
     */

	/**
	 * Создаёт самую первую вкладку
	 * @param  string  [$title = FALSE]     Заголовок первой вкладки
	 * @return integer id новой вкладки или FALSE при неудаче
	 */
	public function init_tab($title = FALSE) {
        
        if ($this->_count_tabs(1, 0) > 0)    // Инициализация невозможна, если уже есть вкладки
            return !($this->resp = self::$responses['error_something']);    // return FALSE;

        // Невалидный или неуказанный title будет преобразован в дефолтный текст
        $title  = $this->validate->tabtitle($title);

        // Вставляем/добавляем новую вкладку
        // INSERT INTO tabs (gid, tid, parenttid, title)  VALUES ($gid, 1, 0, $title)
        $result = $this->db
            ->insert(self::$tbltabs, array(
                'gid'       => $this->vkauth->gid, 
                'tid'       => 1, 
                'parenttid' => 0, 
                'title'     => $title
            ), TRUE)
        OR log_message('error', "Error DB insert tab for gid #{$this->vkauth->gid} at query: ".$this->db->last_query());

        $this->newtid       = 1;
        $this->newparenttid = 0;
        $this->resp = $result ? self::$responses['made'] : self::$responses['error_make'];
        
        return $result;
    }


	/**
	 * Добавляет/вставляет вкладку слева от указанной
	 * @param  string  [$title = FALSE]     Заголовок добавляемой вкладки
	 * @return boolean Успех/неудача
	 */
	public function add_tab_left($tid = FALSE, $parenttid = FALSE, $title = FALSE) {

        // Крайний аргумент TRUE превращает add_tab_right в add_tab_left
        return $this->add_tab_right($tid, $parenttid, $title, TRUE);
    }


	/**
	 * Добавляет/вставляет вкладку справа от указанной
	 * @param  string  [$title = FALSE]     Заголовок добавляемой вкладки
	 * @param  boolean [$addleft = FALSE]   "Волшебный" аргумент, превращающий add_tab_right в add_tab_left =)
	 * @return boolean Успех/неудача
	 */
	public function add_tab_right($tid = FALSE, $parenttid = FALSE, $title = FALSE, $addleft = FALSE) {
        
        if ( !$this->_is_valid_tids($tid, $parenttid) OR !$this->_is_valid_tab($tid, $parenttid)) 
            return !($this->resp = self::$responses['error_something']);    // return FALSE;
        
        // Нельзя добавить, т.к. достигнут максимум вкладок на данном уровне
        if ($this->_count_tabs($tid, $parenttid) >= self::$maxtabs) 
            return !($this->resp = self::$responses['error_maxtabs']);      // return FALSE;

        // Невалидный или неуказанный title будет преобразован в дефолтный текст
        $title  = $this->validate->tabtitle($title);

        // У новой вкладки будет: Текущий №, если добавляем слева; Инкрементный №, если добавляем справа
        $newtid = $addleft ? $tid : $tid + 1;
        
        // Получаем вкладки данного уровня, подлежащие сдвигу вправо (если $parenttid == 0, то 1-й уровень вкладок)
        $ikeys1 = $addleft
            ? $this->_ikeys_greater_equal_than($tid, $parenttid)
            : $this->_ikeys_greater_than($tid, $parenttid);
        
        // Получаем зависимые вкладки 2-го уровня, подлежащие сдвигу вправо (ТОЛЬКО если вставка на 1-м уровне)
        $ikeys2 = $addleft
            ? $this->_child_ikeys_greater_equal_than($tid, $parenttid)
            : $this->_child_ikeys_greater_than($tid, $parenttid);

        $this->db->trans_start();

            // Сдвигаем вправо вкладки данного уровня, освобождая один № в середине для вставки новой вкладки
            if (count($ikeys1) > 0) {
                // UPDATE tabs SET tid = tid+1 WHERE ikey IN (1,2,3, ...);
                $this->db
                    ->set('tid', 'tid+1', FALSE)
                    ->where_in('ikey', $ikeys1)
                    ->update(self::$tbltabs)
                OR log_message('error', "Error DB update tabs for gid #{$this->vkauth->gid} at query: ".$this->db->last_query());
            }

            // Сдвигаем вправо зависимые вкладки 2-го уровня (если применимо, иначе $ikeys2 будет пустой)
            if (count($ikeys2) > 0) {
                // UPDATE tabs SET parenttid = parenttid+1 WHERE ikey IN (1,2,3, ...);
                $this->db
                    ->set('parenttid', 'parenttid+1', FALSE)
                    ->where_in('ikey', $ikeys2)
                    ->update(self::$tbltabs)
                OR log_message('error', "Error DB update tabs for gid #{$this->vkauth->gid} at query: ".$this->db->last_query());
            }

            // Вставляем/добавляем новую вкладку
            // INSERT INTO tabs (gid, tid, parenttid, title)  VALUES ($gid, $newtid, $parenttid, $title)
            $this->db
                ->insert(self::$tbltabs, array(
                    'gid'       => $this->vkauth->gid, 
                    'tid'       => $newtid, 
                    'parenttid' => $parenttid, 
                    'title'     => $title
                ), TRUE)
            OR log_message('error', "Error DB insert tabs for gid #{$this->vkauth->gid} at query: ".$this->db->last_query());

        $result = $this->db->trans_complete();

        $this->newtid       = $newtid;
        $this->newparenttid = $parenttid;
        $this->resp = $result ? self::$responses['made'] : self::$responses['error_make'];
        
        return $result;
    }


    /**
     * Удаляет указанную вкладку
     * При удалении последней вкладки на 2-м уровне будет удален сам этот уровень
	 * @return boolean Успех/неудача
     */
    public function del_tab($tid = FALSE, $parenttid = FALSE) {
        
        if ( !$this->_is_valid_tids($tid, $parenttid) OR !$this->_is_valid_tab($tid, $parenttid)) 
            return !($this->resp = self::$responses['error_something']);    // return FALSE;
        
        // Нельзя удалить единственную вкладку на 1-м уровне (на 2-м будет удален вложенный уровень)
        if (($this->_count_tabs($tid, $parenttid) <= 1) && ($parenttid == 0)) 
            return !($this->resp = self::$responses['error_lasttab']);      // return FALSE;
        
        // Нельзя удалить вкладку, у которой есть вложенный уровень - сначала нужно удалить уровень
        if ($this->_child_count_tabs($tid, $parenttid) > 0) 
            return !($this->resp = self::$responses['error_1stdellvl']);    // return FALSE;
        
        // Получаем вкладки данного уровня, подлежащие сдвигу влево (если $parenttid == 0, то 1-й уровень вкладок)
        $ikeys1 = $this->_ikeys_greater_than($tid, $parenttid);
        
        // Получаем зависимые вкладки 2-го уровня, подлежащие сдвигу влево (ТОЛЬКО если удаление на 1-м уровне)
        $ikeys2 = $this->_child_ikeys_greater_than($tid, $parenttid);

        $this->db->trans_start();
        
            // Удаляем данную вкладку
            // DELETE FROM tabs WHERE (gid = $gid) AND (tid = $tid) AND (parenttid = $parenttid)
            $this->db
                ->where(array(
                    'gid'       => $this->vkauth->gid, 
                    'tid'       => $tid, 
                    'parenttid' => $parenttid
                ))
                ->delete(self::$tbltabs)
            OR log_message('error', "Error DB delete tabs for gid #{$this->vkauth->gid} at query: ".$this->db->last_query());

            // Сдвигаем влево вкладки данного уровня, освобождая один № в середине для вставки новой вкладки
            if (count($ikeys1) > 0) {
                // UPDATE tabs SET tid = tid-1 WHERE ikey IN (1,2,3, ...);
                $this->db
                    ->set('tid', 'tid-1', FALSE)
                    ->where_in('ikey', $ikeys1)
                    ->update(self::$tbltabs)
                OR log_message('error', "Error DB update tabs for gid #{$this->vkauth->gid} at query: ".$this->db->last_query());
            }

            // Сдвигаем влево зависимые вкладки 2-го уровня (если применимо, иначе $ikeys2 будет пустой)
            if (count($ikeys2) > 0) {
                // UPDATE tabs SET parenttid = parenttid-1 WHERE ikey IN (1,2,3, ...);
                $this->db
                    ->set('parenttid', 'parenttid-1', FALSE)
                    ->where_in('ikey', $ikeys2)
                    ->update(self::$tbltabs)
                OR log_message('error', "Error DB update tabs for gid #{$this->vkauth->gid} at query: ".$this->db->last_query());
            }

        $result = $this->db->trans_complete();
        
        $newtid = $tid - 1;

        // Декрементный № у вкладки при обычном удалении, или 
        //   если удален вложенный уровень, то переключение на вкладку верхнего уровня
        $this->newtid       = ($newtid > 0) ? $newtid : ($parenttid <= 0 ? 1 : $parenttid);
        $this->newparenttid = ($newtid > 0) ? $parenttid : 0;
        
        $this->resp = $result ? self::$responses['made'] : self::$responses['error_make'];
        
        return $result;
    }


    /**
     * Добавляет вкладку на 2-й уровень (если его нет), делая данную вкладку родителем
	 * @param  string  [$title = FALSE]     Заголовок добавляемой вкладки
	 * @return boolean Успех/неудача
     */
    public function add_level($tid = FALSE, $parenttid = FALSE, $title = FALSE) {
        
        if ( !$this->_is_valid_tids($tid, $parenttid) OR !$this->_is_valid_tab($tid, $parenttid)) 
            return !($this->resp = self::$responses['error_something']);    // return FALSE;
        
        // Нельзя добавить вложенный уровень на 2-м уровне (можно только на 1-м уровне)
        if ($parenttid > 0) 
            return !($this->resp = self::$responses['error_addlevel']);     // return FALSE;
        
        // Нельзя добавить вложенный уровень, если он уже есть
        if ($this->_child_count_tabs($tid, $parenttid) > 0) 
            return !($this->resp = self::$responses['error_addlevel']);     // return FALSE;

        // Невалидный или неуказанный title будет преобразован в дефолтный текст
        $title  = $this->validate->tabtitle($title);

        // Вставляем/добавляем новую вкладку
        // INSERT INTO tabs (gid, tid, parenttid, title) VALUES ($gid, 1, $tid, $title)
        $result = $this->db
            ->insert(self::$tbltabs, array(
                'gid'       => $this->vkauth->gid, 
                'tid'       => 1, 
                'parenttid' => $tid, 
                'title'     => $title
            ), TRUE)
        OR log_message('error', "Error DB insert tab for gid #{$this->vkauth->gid} at query: ".$this->db->last_query());

        $this->newtid       = 1;
        $this->newparenttid = $tid;
        $this->resp = $result ? self::$responses['made'] : self::$responses['error_make'];
        
        return $result;
    }


    /**
     * Удаляет все вкладки 2-го уровня, у которых данная вкладка является родителем
	 * @return boolean Успех/неудача
     */
    public function del_level($tid = FALSE, $parenttid = FALSE) {
        
        if ( !$this->_is_valid_tids($tid, $parenttid) OR !$this->_is_valid_tab($tid, $parenttid)) 
            return !($this->resp = self::$responses['error_something']);    // return FALSE;
        
        // Нельзя удалить вложенный уровень на 2-м уровне
        if ($parenttid > 0) 
            return !($this->resp = self::$responses['error_dellevel']);     // return FALSE;
        
        // Нельзя удалить вложенный уровень, если его нет
        if ($this->_child_count_tabs($tid, $parenttid) == 0) 
            return !($this->resp = self::$responses['error_dellevel']);     // return FALSE;

        // Удаляем все вкладки 2-го уровня, которые ссылаются на данную
        // DELETE FROM tabs WHERE (gid = $gid) AND (parenttid = $tid)
        $result = $this->db
            ->where(array(
                'gid'       => $this->vkauth->gid, 
                'parenttid' => $tid
            ))
            ->delete(self::$tbltabs)
        OR log_message('error', "Error DB delete tab for gid #{$this->vkauth->gid} at query: ".$this->db->last_query());

        $this->newtid       = $tid;
        $this->newparenttid = 0;
        $this->resp = $result ? self::$responses['made'] : self::$responses['error_make'];
        
        return $result;
    }


    /***********************************************************************************************
     * Набор protected методов для валидации и получения данных о вкладках.
	 * Суть набора - сделать один select-запрос к БД с выборкой всех вкладок, сохранить данные в массив
	 * $rawtabs, и далее, при вызове других методов набора, выполнять вычислительный анализ данных вкладок
	 * без повторных обращений к БД.
	 * 
	 * Во всех методах (чтобы не повторяться):
	 * @param  integer [$tid = FALSE]       id вкладки
	 * @param  integer [$parenttid = FALSE] id родительской вкладки (0 для вкладки 1-го уровня)
     */
    
    /**
     * Получает выборку вкладок из БД и сохраняет её в $rawtabs для дальнейших операций
     * @private
     * @return boolean Успешная выборка
     */
    protected function _get_rawtabs() {
        
        $selresult = $this->db->select('ikey,tid,parenttid')
            ->where(array('gid' => $this->vkauth->gid))
            ->get(self::$tbltabs);
        
        $this->rawtabs = $selresult->result_array();
            
        return ($selresult && is_array($this->rawtabs));
    }
    
    /**
     * Проверяет параметры вкладки на валидность
     * @private
     * @return boolean Валидные параметры
     */
    protected function _is_valid_tids($tid, $parenttid) {
        $tid_pre       = $tid;
        $parenttid_pre = $parenttid;
        
        $tid       = (int) $tid;
        $parenttid = (int) $parenttid;
        $tid       = $tid       < 1 ? 1 : ($tid       > self::$maxtabs ? self::$maxtabs : $tid);
        $parenttid = $parenttid < 0 ? 0 : ($parenttid > self::$maxtabs ? self::$maxtabs : $parenttid);
        
        return (($tid == $tid_pre) && ($parenttid == $parenttid_pre));
    }
    
    /**
     * Проверяет факт существованя вкладки
     * @private
     * @return boolean Вкладка существует
     */
    protected function _is_valid_tab($tid, $parenttid) {
        if (count($this->rawtabs) == 0) $this->_get_rawtabs();
        
        foreach ($this->rawtabs as $item)
            if (($item['tid'] == $tid) && ($item['parenttid'] == $parenttid))
                return TRUE;
        
        return FALSE;
    }
    
    /**
     * Считает число вкладок на ТЕКУЩЕМ уровне
     * @private
     * @return integer Число вкладок или ноль
     */
    protected function _count_tabs($tid, $parenttid) {
        if (count($this->rawtabs) == 0) $this->_get_rawtabs();
        
        $k = 0;
        foreach ($this->rawtabs as $item)
            if ($item['parenttid'] == $parenttid)
                ++$k;
        
        return $k;
    }
    
    /**
     * Считает число вкладок на ВЛОЖЕННОМ уровне (если применимо)
     * @private
     * @return integer Число вкладок или ноль
     */
    protected function _child_count_tabs($tid, $parenttid) {
        if (count($this->rawtabs) == 0) $this->_get_rawtabs();
        
        $k = 0;
        if ($parenttid == 0) {      // Применимо только для 1-го уровня
            foreach ($this->rawtabs as $item)
                if ($item['parenttid'] == $tid)
                    ++$k;
        }
        return $k;
    }
    
    /**
     * Возвращает список уникальных ключей ikeys для вкладок ТЕКУЩЕГО уровня, лежащих правее данной
     * @private
     * @return array Линейный массив со списком ключей
     */
    protected function _ikeys_greater_than($tid, $parenttid) {
        if (count($this->rawtabs) == 0) $this->_get_rawtabs();

        $ikeys = [];
        foreach ($this->rawtabs as $item) {
            if (($item['parenttid'] == $parenttid) && ($item['tid'] > $tid))
                $ikeys[] = $item['ikey'];
        }
        
        return $ikeys;
    }
    
    /**
     * Возвращает список уникальных ключей ikeys для вкладок ТЕКУЩЕГО уровня, включая данную вкладку и правее
     * @private
     * @return array Линейный массив со списком ключей
     */
    protected function _ikeys_greater_equal_than($tid, $parenttid) {
        if (count($this->rawtabs) == 0) $this->_get_rawtabs();

        $ikeys = [];
        foreach ($this->rawtabs as $item) {
            if (($item['parenttid'] == $parenttid) && ($item['tid'] >= $tid))
                $ikeys[] = $item['ikey'];
        }
        
        return $ikeys;
    }
    
    /**
     * Возвращает список уникальных ключей ikeys для ВСЕХ вкладок ВСЕХ ВЛОЖЕННЫХ уровней, лежащих правее данной
     * @private
     * @return array Линейный массив со списком ключей
     */
    protected function _child_ikeys_greater_than($tid, $parenttid) {
        if (count($this->rawtabs) == 0) $this->_get_rawtabs();

        $ikeys = [];
        if ($parenttid == 0) {      // Применимо только для 1-го уровня
            foreach ($this->rawtabs as $item) {
                if ($item['parenttid'] > $tid)
                    $ikeys[] = $item['ikey'];
            }
        }
        return $ikeys;
    }
    
    /**
     * Возвращает список уникальных ключей ikeys для ВСЕХ вкладок ВСЕХ ВЛОЖЕННЫХ уровней, включая данную вкладку и правее
     * @private
     * @return array Линейный массив со списком ключей
     */
    protected function _child_ikeys_greater_equal_than($tid, $parenttid) {
        if (count($this->rawtabs) == 0) $this->_get_rawtabs();

        $ikeys = [];
        if ($parenttid == 0) {      // Применимо только для 1-го уровня
            foreach ($this->rawtabs as $item) {
                if ($item['parenttid'] >= $tid)
                    $ikeys[] = $item['ikey'];
            }
        }
        return $ikeys;
    }

}
