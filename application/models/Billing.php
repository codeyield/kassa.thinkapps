<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Billing extends CI_Model {
    
// № инвойса (он же AUTO_INCREMENT) последней операции пополнения или списания
public $last_insert_id;

// Таблица с зачислениями и списаниями для всех сообществ
protected static $tblpayments;

// Используемые параметры конфига
protected static $paytypes;
protected static $moneyintypes;


	public function __construct() {
        parent::__construct();

        // Загрузка конфигов в модели
        $CI =& get_instance();
        $CI->config->load('ukonfig', TRUE);
        $ukonfig = $CI->config->item('ukonfig');

        self::$tblpayments  = $ukonfig['dbtbl']['payments'];

        self::$paytypes     = $ukonfig['reward']['paytypes'];
        self::$moneyintypes = $ukonfig['reward']['moneyintypes'];

        // Отключаем строгий режим транзакций
        $this->db->trans_strict(FALSE);

        // Получаем параметры запуска приложения от ВК или из сессии
        $this->load->model('vkauth');
        $this->vkauth->get();
	}

    
    /**
     * Зачисляет сумму на баланс сообщества
     * @param  integer [$gid = FALSE]         id сообщества
     * @param  integer [$uid = FALSE]         id пользователя
     * @param  string  [$type = NULL]         Тип платежа и/или платёжная система
     * @param  string  [$operation_id = NULL] Идентификатор операции
     * @param  float   [$amount = 0]          Сумма зачисления
     * @param  string  [$datetime = FALSE]    Дата и время операции в платёжной системе или NOW(), если FALSE
     * @param  boolean [$accepted = TRUE]     Платёж зачислен. FALSE для переводов с протекцией или если переполнен кошелёк
     * @param  integer [$yasender = NULL]     № яндекс-кошелька отправителя (если применимо)
     * @param  string  [$note = NULL]         Примечание к платежу
     * @return boolean Успех операции
     */
    public function credit($gid = FALSE, $uid = FALSE, $type = NULL, $operation_id = NULL, 
                           $amount = 0, $datetime = FALSE, $accepted = TRUE, $yasender = NULL, $note = NULL) {
        
        if ($amount <= 0)
            return FALSE;
        
        // Если gid/uid не указаны явно, то получаем их из сессии
        $gid = (is_null($gid) OR ($gid === FALSE)) ? $this->vkauth->gid : $gid;
        $uid = (is_null($uid) OR ($uid === FALSE)) ? $this->vkauth->uid : $uid;
        
        // Приводим строковую дату к формату MySQL. Или NOW(), если дата невалидная
        $datetime = ($tmp = strtotime($datetime)) ? date('Y-m-d H:i:s', $tmp) : date('Y-m-d H:i:s');
        
        // INSERT INTO payments (gid, uid, ...) VALUES ($gid, $uid, ...)
        $result = $this->db
            ->insert(self::$tblpayments, array(
                'gid'           => $gid, 
                'uid'           => $uid, 
                'type'          => $type, 
                'operation_id'  => $operation_id, 
                'amount'        => $amount,
                'date_time'     => $datetime,
                'accepted'      => ($accepted ? 1 : 0),     // Приводим булев к TINYINT
                'yasender'      => $yasender,
                'note'          => $note,
            ))
        OR log_message('error', "Error DB credit payment for gid #{$gid} at query: ".$this->db->last_query());
        
        $this->last_insert_id = $this->db->insert_id();

        return $result;
    }
    

    /**
     * Зачисляет подарочную сумму по промо-коду
     * @param  float   $amount   Сумма зачисления
     * @param  string  $codename Промо-код
     * @return boolean Успех операции
     */
    public function credit_promo($amount, $codename) {
        
        return $this->credit(
            $this->vkauth->gid,
            $this->vkauth->uid,
            self::$paytypes['promo'],
            NULL,                       // operation_id
            $amount,
            FALSE,                      // datetime = NOW()
            TRUE,                       // accepted
            "Зачисление по промо-коду ".$codename
        );
    }


    /**
     * Списывает сумму с баланса сообщества
     * @param  integer [$gid = FALSE]         id сообщества
     * @param  integer [$uid = FALSE]         id пользователя
     * @param  string  [$type = NULL]         Тип платежа и/или платёжная система
     * @param  string  [$operation_id = NULL] Идентификатор операции
     * @param  float   [$amount = 0]          Сумма списания
     * @param  string  [$datetime = FALSE]    Дата и время операции. Или NOW(), если FALSE
     * @param  boolean [$accepted = TRUE]     Платёж зачислен. FALSE для переводов с протекцией или когда переполнен кошелёк
     * @param  string  [$note = NULL]         Примечание к платежу
     * @return boolean Успех операции
     */
    public function debit($gid = FALSE, $uid = FALSE, $type = NULL, $operation_id = NULL, 
                          $amount = 0, $datetime = FALSE, $accepted = TRUE, $note = NULL) {
    
        if ($amount == 0)
            return FALSE;
        
        // Если gid/uid не указаны явно, то получаем их из сессии
        $gid = (is_null($gid) OR ($gid === FALSE)) ? $this->vkauth->gid : $gid;
        $uid = (is_null($uid) OR ($uid === FALSE)) ? $this->vkauth->uid : $uid;
        
        // Приводим строковую дату к формату MySQL. Или NOW(), если дата невалидная
        $datetime = ($tmp = strtotime($datetime)) ? date('Y-m-d H:i:s', $tmp) : date('Y-m-d H:i:s');
        
        // Сумма всегда отрицательная для дебетовых операций
        $amount = ($amount < 0 ) ? $amount : -$amount;
        
        // INSERT INTO payments (gid, uid, ...) VALUES ($gid, $uid, ...)
        $result = $this->db
            ->insert(self::$tblpayments, array(
                'gid'           => $gid, 
                'uid'           => $uid, 
                'type'          => $type, 
                'operation_id'  => $operation_id, 
                'amount'        => $amount,
                'date_time'     => $datetime,
                'accepted'      => ($accepted ? 1 : 0),     // Приводим булев к TINYINT
                'note'          => $note,
            ))
        OR log_message('error', "Error DB debit payment for gid #{$gid} at query: ".$this->db->last_query());
        
        $this->last_insert_id = $this->db->insert_id();

        return $result;
    }
    

    /**
     * Списывает оплату за продление тарифа в автоматическом режиме (без действий юзера)
     * @param  float   $amount   Сумма списания
     * @param  string  $datetime Дата продления тарифа (предполагается значение planexpiry из БД)
     * @return boolean Успех операции
     */
    public function debit_auto_plan($amount, $datetime) {
        
        return $this->debit(
            $this->vkauth->gid,
            $this->vkauth->uid,
            self::$paytypes['auto'],
            NULL,                       // operation_id
            $amount,
            $datetime,
            TRUE,                       // accepted
            NULL                        // note
        );
    }
    

    /**
     * Списывает оплату тарифа при его апгрейде юзером или переключении на платный тариф
     * @param  float   $amount Сумма списания
     * @param  string  $note   Примечание к платежу
     * @return boolean Успех операции
     */
    public function debit_by_user($amount, $note) {
        
        return $this->debit(
            $this->vkauth->gid,
            $this->vkauth->uid,
            self::$paytypes['user'],
            NULL,                       // operation_id
            $amount,
            FALSE,                      // datetime
            TRUE,                       // accepted
            $note
        );
    }
    
    
    /**
     * Возвращает сумму последнего пополнения баланса _деньгами_ (исключая бонусные и прочие начисления)
     * @return float Сумма оплаты
     */
    public function last_income_amount() {
        
        // SELECT amount FROM payments WHERE (gid = $gid) AND (accepted > 0) AND (type IN ('ym','card','tel','qiwi','wm')) ORDER BY date_time DESC LIMIT 1
        $selresult = $this->db
            ->select('amount')
            ->where('gid', $this->vkauth->gid)
            ->where('accepted >', 0)
            ->where_in('type', self::$moneyintypes)
            ->order_by('date_time', 'DESC')
            ->limit(1)
            ->get(self::$tblpayments);
        
        $result = (array) $selresult->row();
        
        return isset($result['amount']) ? $result['amount'] : 0;
    }


    /**
     * Возвращает сумму на балансе сообщества (исключая незачисленные платежи)
     * @return float Сумма на балансе
     */
    public function get_amount() {
        
        //SELECT SUM(amount) FROM payments WHERE (gid = $gid) AND (accepted > 0)
        $selresult = $this->db
            ->select_sum('amount', 'amount')
            ->where('gid', $this->vkauth->gid)
            ->where('accepted >', 0)
            ->get(self::$tblpayments);
        
        $result = (array) $selresult->row();
        
        return isset($result['amount']) ? $result['amount'] : 0;
    }

    
    /**
     * Проверяет наличие (достаточность) необходимой суммы на балансе сообщества
     * @param boolean Достаточно или нет
     */
    public function is_amount_enough($amount) {
        return ($this->get_amount() >= $amount);
    }

    
    /**
     * Проверяет наличие незачисленных платежей, отправленных с кодом протекции (или если переполнен кошелёк)
     * @return boolean Есть незачисленные платежи
     */
    public function has_unaccepted() {
        
        //SELECT `accepted` FROM payments WHERE (gid = $gid) AND (accepted = 0)
        $selresult = $this->db
            ->select('accepted')
            ->where('gid', $this->vkauth->gid)
            ->where('accepted', 0)
            ->get(self::$tblpayments);
        
        $result = (array) $selresult->row();
        
        return (count($result) > 0);
    }

}
