<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promo extends CI_Model {

// Массив ответа $resp, возвращаемый по цепочке: модель -> контроллер -> ajax-скрипт
public $resp = [];

// Таблицы в БД
protected static $tblpromos;
protected static $tblupdpromos;

// Используемые параметры конфига
protected static $responses;


	public function __construct() {
        parent::__construct();

        // Загрузка конфигов в модели
        $CI =& get_instance();
        $CI->config->load('ukonfig', TRUE);
        $ukonfig = $CI->config->item('ukonfig');

        self::$tblpromos    = $ukonfig['dbtbl']['promos'];
        self::$tblupdpromos = $ukonfig['dbtbl']['updpromos'];
        
        self::$responses   = $ukonfig['resp'];

        // Отключаем строгий режим транзакций
        $this->db->trans_strict(FALSE);

        // Получаем параметры запуска приложения от ВК или из сессии
        $this->load->model('vkauth');
        $this->vkauth->get();
	}

    /**
     * Пробует применить/погасить промо-код (если он валиден)
     * @param  string  $codename Промо-код
     * @return boolean Промо-код принят или нет
     */
    public function push($codename) {
        
        if (!$this->vkauth->is_auth OR !$this->vkauth->is_valid_gid())
            return FALSE;
        
        $codename = trim($codename);
        
        // SELECT * FROM promos WHERE (codename = $codename) AND (on > 0)
        $selresult = $this->db->get_where(self::$tblpromos, array('codename' => $codename, 'on >' => 0));
        $codeset = (array) $selresult->row();
        
        
        // Проверка промо-кода
        
        // Нет такого промо-кода или он выключен
        if (count($codeset) == 0) 
            return !($this->resp = self::$responses['prerr_noexists']);       // return FALSE;
        
        // Просрочена дата действия промо-кода
        if (isset($codeset['tilldate']) AND (time() > strtotime($codeset['tilldate']))) 
            return !($this->resp = self::$responses['prerr_expiry']);         // return FALSE;
        
        // Истекли указанное число дней после установки приложения
        if (isset($codeset['frominit'])) {
            $this->load->model('settings');
            $settings = $this->settings->get();
            
            if (time() > (strtotime($settings['created']) + 60*60*24 * $codeset['frominit']))
                return !($this->resp = self::$responses['prerr_expiry']);     // return FALSE;
        }
        
        // Эти промо-коды закончились
        if (isset($codeset['quantity']) AND ($codeset['quantity'] <= 0)) 
            return !($this->resp = self::$responses['prerr_ended']);          // return FALSE;
        
        // Проверяем, использовался ли раньше этот промо-код в сообществе
        if ($this->is_used($codename))
            return !($this->resp = self::$responses['prerr_used']);           // return FALSE;
        
        // Промо-код зависит от текущего тарифа
        if (($codeset['inpaid'] > 0) || isset($codeset['inplans'])) {

            $this->load->is_loaded('plan') OR $this->load->model('plan');
            $planset = $this->plan->get_planset();
            
            // Промо-код можно использовать только на платных тарифах
            if (($codeset['inpaid'] > 0) AND ($planset['price'] == 0))
                return !($this->resp = self::$responses['prerr_inpaid']);     // return FALSE;
            
            // Промо-код нельзя использовать с текущим тарифом
            if (isset($codeset['inplans']) AND !in_array($planset['plan'], explode(',', $codeset['inplans'])))
                return !($this->resp = self::$responses['prerr_inplans']);    // return FALSE;
        }
        

        // Применение/погашение промо-кода
        
        $this->resp = [];
        $this->db->trans_start();       // DB transaction start
        
            // Продлеваем текущий тариф на указанное число дней (что-то одно - или продление или деньги)
            if (isset($codeset['add_days'])) {

                $this->load->is_loaded('plan') OR $this->load->model('plan');
                
                if ($this->plan->prolong($codeset['add_days'])) {
                    $planset = $this->plan->get_planset();

                    $this->resp = self::$responses['prok_prolong'];
                    // Дописываем новую дату окончания тарифа в конец строки ответа
                    $this->resp['text'] = $this->resp['text'] . date("d.m.Y", strtotime($planset['planexpiry']));
                }
            }
            // Добавляем на баланс фиксированную сумму или в % от последнего платежа (что-то одно, приоритет фикс. суммы)
            elseif (isset($codeset['add_amount']) || isset($codeset['add_percent'])) {

                $this->load->is_loaded('billing') OR $this->load->model('billing');

                $amount = isset($codeset['add_amount']) 
                    ? $codeset['add_amount']                                                           // Фиксированная сумма пополнения
                    : round($this->billing->last_income_amount() * $codeset['add_percent'] / 100, 0);  // Сумма в % от последнего платежа

                if ($this->billing->credit_promo($amount, $codename)) {
                    $this->resp = self::$responses['prok_addbalance'];
                    $this->resp['text'] = $this->resp['text'] . "{$amount}р.";      // Дописываем добавленную сумму в конец строки ответа
                }
            }

            // Декрементируем кол-во лимитированного промо-кода (если он только что был использован)
            if (isset($this->resp['response']) AND isset($codeset['quantity'])) {
                // UPDATE promos SET quantity = quantity - 1 WHERE codename = $codename
                $this->db
                    ->set('quantity', 'quantity - 1', FALSE)
                    ->where('codename', $codename)
                    ->update(self::$tblpromos)
                OR log_message('error', "Error DB decrement promo code {$codename} at query: ".$this->db->last_query());
            }

            // Сохраняем инфу об использовании промо-кода (если он только что был использован)
            if (isset($this->resp['response'])) {
                // INSERT INTO updpromos (gid, uid, codename) VALUES ($gid, $uid, $codename)
                $this->db
                    ->insert(self::$tblupdpromos, array(
                        'gid'      => $this->vkauth->gid, 
                        'uid'      => $this->vkauth->uid, 
                        'codename' => $codename,
                    ))
                OR log_message('error', "Error DB promo code using history for gid #{$this->vkauth->gid} at query: ".$this->db->last_query());
            }
        
        $result = $this->db->trans_complete();  // DB commit
        
        // Ошибка! Ответ response не записан, п.ч. ни один бонус для промо-кода не задан в $codeset
        if (!isset($this->resp['response']))
            $this->resp = self::$responses['prerr_something'];
        
        return $result;
    }
    
    
    /**
     * Проверяет, были ли раньше использован этот промо-код в сообществе
     * @param  string  $codename Промо-код
     * @return boolean Промо-код уже использовался
     */
    public function is_used($codename) {
        
        if (!$this->vkauth->is_auth OR !$this->vkauth->is_valid_gid())
            return FALSE;
        
        // SELECT ikey FROM updpromos WHERE (gid = $gid) AND (codename = $codename)
        $selresult = $this->db
            ->select('ikey')
            ->where('gid', $this->vkauth->gid)
            ->where('codename', $codename)
            ->get(self::$tblupdpromos);
        
        $result = (array) $selresult->row();
        
        return (count($result) > 0);
    }

}