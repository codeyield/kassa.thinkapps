<?php

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Продлевает текущий тариф на указанное число дней
        if (!is_null($code['add_days'])) {
        
            if ($this->plan->prolong($code['add_days'])) {
                $planset = $this->plan->get_planset();
                
                $this->resp = self::$responses['prok_prolong'];
                // Дописываем новую дату окончания тарифа в конец строки ответа
                $this->resp['text'] = $this->resp['text'] . date("d.m.Y", $planset['planexpiry']);
                return TRUE;
            }
            else
                return !($this->resp = self::$responses['prerr_something']);        // return FALSE;
        }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/**
	 * Тестовый метод для отладки
	 * http://thinkapps.kassa/income/testincome?addbalance=100
	 *  addbalance - тестовый параметр с суммой платежа для отладки
	 */
	public function testincome() {

        // Получаем содержимое $_GET
        $get = $this->input->get(NULL, FALSE);

        if (isset($get['addbalance'])) {
            
            $amount = (int) $get['addbalance'];
            
            $result = $this->billing->credit(FALSE, FALSE, self::$paytypes['test'], 'test_payment_'.rand(10000,99999), 
                                   $amount, FALSE, TRUE, '41001000000012345', "Тестовый платеж");
            
            echo $result ? "Зачислено {$amount}р." : "Ошибка!";
        }
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /**
     * Проверяет наличие куки о закрытии юзером Bootstrap-алерта, переданного в аргументе 
     *   JS-обработчик по закрытию алерта сохраняет куку в формате: alert_id = 0
     *   Там же в js задаётся срок действия куки. Алерт должен содержать alert id в конфиге.
     *   
     * @param  array $alert Выбранный алерт из конфига вида $config['alerts']['alert_name']
     * @return array/NULL   Входной массив или NULL, если алерт был закрыт юзером и кука не истекла
     */
    public function check($alert) {

        foreach($_COOKIE as $name => $value) {
            if (isset($alert['id']) AND ($alert['id'] == $name) AND ($value == 0))
                return NULL;
        }
        
        return $alert;
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
     * Для проверки подлинности уведомления рассчитайте хэш и сравните данные со значением sha1_hash.
     * 1. Сформировать строку из параметров уведомления в кодировке UTF-8 (notification_secret — это секрет для проверки).
     *    Формат строки:
     *    notification_type&operation_id&amount&currency&datetime&sender&codepro&notification_secret&label
     *    Пример строки параметров:
     *    p2p-incoming&1234567&300.00&643&2011-07-01T09:00:00.000+04:00&41001XXXXXXXX&false&01234567890ABCDEF01234567890&
     *    Пример строки параметров с меткой платежа:
     *    p2p-incoming&1234567&300.00&643&2011-07-01T09:00:00.000+04:00&41001XXXXXXXX&false&01234567890ABCDEF01234567890&YM.label.12345
     * 2. Вычислить значение хэш-функции SHA-1 от полученной строки.
     * 3. Отформатировать полученный результат в HEX-кодированном виде.
     *    Пример рассчитанного значения параметра sha1_hash: 090a8e7ebb6982a7ad76f4c0f0fa5665d741aafa

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /***** Методы получения данных для произвольного тарифа *****/

    /**
     * Возвращает кириллическое имя указанного тарифа
     * @param  string $plan_id Код тарифа
     * @return string Название тарифа или FALSE
     */
    public function get_name($plan_id) {
        return isset(self::$plansheet[$plan_id]['name']) ? self::$plansheet[$plan_id]['name'] : FALSE;
    }
    
    /**
     * Возвращает стоимость оплаты указанного тарифа
     * @param  string $plan_id Код тарифа
     * @return integer Стоимость оплаты тарифа или FALSE
     */
    public function get_price($plan_id) {
        return isset(self::$plansheet[$plan_id]['price']) ? self::$plansheet[$plan_id]['price'] : FALSE;
    }
    
    /***** Методы получения данных и проверок тарифных настроек сообщества *****/
    
    /**
     * Возвращает текстовый код (айди) текущего тарифа для сообщества
     * @return string Код тарифа или FALSE при ошибке
     */
    public function get_now_id() {
        $this->get_planset();
        return isset($this->planset['plan']) ? $this->planset['plan'] : FALSE;
    }

    /**
     * Возвращает текстовый код (айди) тарифа на следующий платёжный период
     * @return string Код тарифа или FALSE при ошибке
     */
    public function get_next_id() {
        $this->get_planset();
        return isset($this->planset['nextplan']) ? $this->planset['nextplan'] : FALSE;
    }
    
    /**
     * Возвращает временную метку истечения срока действия тарифа для сообщества
     * @return integer Временная метка смены тарифа unix timestamp
     */
    public function get_expiry() {
        $this->get_planset();
        return isset($this->planset['planexpiry']) ? $this->planset['planexpiry'] : FALSE;
    }

    /**
     * Проверяет наступление следующего платёжного периода (момента смены тарифа)
     * @return boolean Наступил момент - да/нет
     */
    public function is_expiry() {
        $this->get_planset();
        return isset($this->planset['planexpiry']) ? (time() >= strtotime($this->planset['planexpiry'])) : FALSE;
    }
    

    /**
     * Возвращает список кодов всех тарифов
     * @return array Линейный массив со списком кодов
     */
    public function get_all_ids() {
        return array_keys(self::$plansheet);
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        $nowplan = isset($this->planset['plan']) ? $this->planset['plan'] : NULL;
        
        // Дополняем массив настроек параметрами тарифа из конфига
        $this->planset['name']      = isset($nowplan) ? self::$plansheet[$nowplan]['name']     : NULL;
        $this->planset['price']     = isset($nowplan) ? self::$plansheet[$nowplan]['price']    : NULL;
        $this->planset['tabsum']    = isset($nowplan) ? self::$plansheet[$nowplan]['tabsum']   : NULL;
        $this->planset['maxtabs']   = isset($nowplan) ? self::$plansheet[$nowplan]['maxtabs']  : NULL;
        $this->planset['ttmax']     = isset($nowplan) ? self::$plansheet[$nowplan]['ttmax']    : NULL;
        $this->planset['btmax']     = isset($nowplan) ? self::$plansheet[$nowplan]['btmax']    : NULL;
        $this->planset['optslock']  = isset($nowplan) ? self::$plansheet[$nowplan]['optslock'] : NULL;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /**
     * Возвращает массив ограничивающих параметров для проверки форм на вкладках, в т.ч. тарифо-зависимые (если биллинг включен)
     * @param  string $plan_id Символьный код тарифа
     * @return array  Массив параметров для вкладок
     */
    public function get_limits($plan_id) {
        
        if (!$this->load->is_loaded('plan'))
            $this->load->model('plan');
        
        // Получаем настройки тарифа для сообщества, включая данные конфига с тарифными ограничениями
        $planset = $this->plan->get_planset();
        
        if ($planset === FALSE)
            return FALSE;
        
        // Формируем данные для ограничения форм на вкладках, включая тарифо-зависимые (если биллинг включен)
        $tablimits = array(
            'maxtabs'  => self::$maxtabs,
            'mintitle' => self::$mintitle,
            'maxtitle' => self::$maxtitle,
            'optslock' => (self::$billing_on ? $planset['optslock'] : FALSE),   // Если биллинг выключен, то все опции доступны
            'maxsum'   => (self::$billing_on ? $planset['tabsum']   : self::$maxsum),
            'ttmax'    => (self::$billing_on ? $planset['ttmax']    : self::$ttmax),
            'btmax'    => (self::$billing_on ? $planset['btmax']    : self::$btmax),
        );

        return $tablimits;
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /**
     * Возвращает тарифозависимые размеры "единых" текстов для вкладок. Или дефолтные параметры, если биллинг выключен
     * @return array Массив с размерами текстов для вкладок
     */
    public function textlimits() {
        $this->get_planset();
        return array(
            'ttmax' => (self::$billing_on ? $this->planset['ttmax'] : self::$tabs_config['ttmax']),
            'btmax' => (self::$billing_on ? $this->planset['btmax'] : self::$tabs_config['btmax'])
        );
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        // Получаем тарифные данные для сообщества
        if (self::$billing_on) {
            $this->load->model('plan');
            $planset = $this->plan->get_planset();
        }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    if (($tid_active > 0) && ($parenttid_active >= 0))
        $tabs = $this->set_active_tabs($tabs, $tid_active, $parenttid_active);
    
    /**
     * Добавляет атрибуты 'active' для вкладок, которые д.б. помечены им в Bootstrap-коде
     * @param  array   $tabs      Массив вкладок, полученный из метода get
     * @param  integer $tid       id вкладки
     * @param  integer $parenttid id родительской вкладки (0 для вкладки 1-го уровня)
     * @return array   Массив вкладок с добавленными ключами 'active'
     */
    public function set_active_tabs($tabs, $tid = 1, $parenttid = 1) {
        
        $tid       = $this->validate->tid($tid);
        $parenttid = $this->validate->parenttid($parenttid);
        
        foreach ($tabs as $pkey => $parent) {
            if ( (($parent['tid'] == $tid)       && ($parenttid == 0)) ||
                 (($parent['tid'] == $parenttid) && ($parenttid > 0))
               )
                $tabs[$pkey]['active'] = TRUE;

            if (isset($parent['child'])) {
                foreach ($parent['child'] as $ckey => $child) {
                    if ( (($child['tid'] == 1)    && ($child['parenttid'] != $parenttid)) ||
                         (($child['tid'] == $tid) && ($child['parenttid'] == $parenttid))
                       )
                        $tabs[$pkey]['child'][$ckey]['active'] = TRUE;
                }
            }
        }
        
        $this->is_success = TRUE;
        return $tabs;
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Выкинуто из конструктора tabedit

// Данные для валидации форм на вкладках, включая тарифо-зависимые (если биллинг включен)
protected $tablimits;
        
        // Формируем данные для ограничения форм на вкладках, в т.ч. тарифо-зависимые (если биллинг включен)
        $this->tablimits = array(
            'maxtabs'  => $ukonfig['tabs']['maxtabs'],
            'mintitle' => $ukonfig['tabs']['title_minlen'],
            'maxtitle' => $ukonfig['tabs']['title_maxlen'],
            'optslock' => (self::$billing_on ? $planset['optslock'] : FALSE),   // Опции доступны, если биллинг выключен
            'maxsum'   => (self::$billing_on ? $planset['tabsum']   : $ukonfig['tabs']['maxsum']),
            'ttmax'    => (self::$billing_on ? $planset['ttmax']    : $ukonfig['settings']['toptext']['maxlen']),
            'btmax'    => (self::$billing_on ? $planset['btmax']    : $ukonfig['settings']['bottomtext']['maxlen']),
        );

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Инициализирует пробный тариф при первичной установке приложения в сообщество
     * @return boolean Успешное обновление в БД
     */
    public function init() {
        
        if (!$this->vkauth->is_auth OR !$this->vkauth->is_valid_gid())
            return FALSE;

        // Вычисляем дату окончания триала
        $expdate = new DateTime(date('Y-m-d'));
        $expdate->add(new DateInterval('P'.self::$trialdays.'D'));
        $planexpiry = $expdate->format('Y-m-d');                     // Округляем до целых суток
            
        // Обновляем тарифные данные в таблице настроек сообщества
        // UPDATE settings SET (...) WHERE gid = $gid
        $updresult = $this->db
            ->set(array(
                'plan'       => self::$trial_id,
                'nextplan'   => self::$free_id,
                'planexpiry' => $planexpiry,
            ))
            ->where('gid', $this->vkauth->gid)
            ->update(self::$tblsettings);
        
        if (!$updresult)
            log_message('error', "Error DB initialize plan's data for gid #{$this->vkauth->gid} at query: ".$this->db->last_query());
        
        // Сохраняем данные о начальном (триальном) тарифе в таблице с историей тарифов
        $this->update_history(self::$trial_id, $planexpiry);
        
        $this->planset = NULL;         // Очистим закэшированные данные после обновления БД
        
        return $updresult;
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /**
     * Возвращает массив с данными текущего тарифа для сообщества
     * @return array Массив с данными тарифа или FALSE при ошибке
     */
    public function get() {
        $this->get_planset();
        return isset(self::$planparams[$this->planset['plan']]) ? self::$planparams[$this->planset['plan']] : FALSE;
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /**
     * Немедленно изменяет текущий тариф, а также рассчитывает новую дату окончания тарифа
     * @param  string  [$newplan = FALSE] Код тарифа на текущий и следующий платёжный период
     * @return boolean Успешное обновление в БД
     */
    public function update_now($newplan = FALSE) {
       
        if (!$this->vkauth->is_auth OR !$this->vkauth->is_valid_gid() OR !$this->is_valid_id($newplan))
            return FALSE;
        
        $nowplan = $this->get_now_id();
        
        // Смена тарифа ФРИ => ПЛАТНЫЙ: planexpiry = now() +1 month. Иначе planexpiry = planexpiry +1 month
        $planexpiry = $this->date_add_1month($nowplan == $this->free_id() ? 'now' : $this->get_expiry());
        
        // Обновляем значение в БД только если апдейт с триала/фри на платный (NULL - не обновляем)
        $planexpiry = (($this->price($nowplan) == 0) AND ($this->price($newplan) > 0)) ? $planexpiry : NULL;

        // Обновляем текущий и следующий тариф на указанный
        $updresult = $this->update($newplan, $newplan, $planexpiry);
        
        // Сохраняем данные об обновлении в истории тарифов
        $this->update_history($newplan, $planexpiry);

        $this->resp = $updresult ? self::$responses['saved_plan'] : self::$responses['error_plan'];
        
        return $updresult;
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
<tr id="trid_{{id}}" 
    {{id == planset.plan ? 'class="success tooltip-green" title="Ваш текущий тариф"' : (id == planset.nextplan ? 'class="info tooltip-blue" title="Ваш тариф на следующий период"')}} 
    {{id == (planset.plan or planset.nextplan) ? 'data-toggle="tooltip" data-placement="top"'}}>

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    form.find('input[name=plan]:checked').prop('disabled', true);
    form.find('input[name=plan]:checked').prop('required', false);
    var newplan = $('input[name=plan]:checked').val();
    var s = form.serializeArray();

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Временная заглушка, пока в БД есть поля `planexpiry` = NULL
        $plan['expiry'] = $plan['expiry'] ? $plan['expiry'] : strtotime($settings['created']) + (self::$trialdays * 60 * 60 *24);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        // Второй проход, если задан $kick - убиваем отмеченные вкладки
        // (лучше сделать второй проход, иначе много путаницы и повторяющегося кода)
        if ($kick) {
            foreach ($tabs as $pkey => $parent) {

                // Убиваем отмеченную вкладку 1-го уровня
                if (isset($parent['exceed'])) {
                    unset($tabs[$pkey]);
                    continue;
                }

                // Проходим по вложенному уровню вкладок (если он есть)
                if (isset($parent['child'])) {
                    foreach ($parent['child'] as $ckey => $child) {
                        
                        // Убиваем отмеченную вкладку 2-го уровня
                        if (isset($child['exceed'])) {
                            unset($tabs[$pkey]['child'][$ckey]);
                            continue;
                        }
                    }
                }

                // Убиваем отмеченную вкладку 1-го уровня   // а также "пустого" родителя без дочерних вкладок
                // if (isset($parent['exceed']))               // OR (count($tabs[$pkey]['child']) == 0)) 
                //    unset($tabs[$pkey]);        // Здесь continue не нужен, т.к. цикл закончился
            }
            
            // Вкладки убиваем только для welcome-страницы - для нее метки 'active' ставим в начало на 1/1
            $tabs = $this->set_active_tabs($tabs, 1, 1);
        }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
<script type="text/javascript">
    window.onload = (function() {
        VK.init(function() {
            var bh = Math.max(document.body.scrollHeight, document.body.offsetHeight, document.body.clientHeight);
            bh = Math.max(bh + 50, {{window_minheight}});
            VK.callMethod('resizeWindow', {{window_width}}, bh);
        });
    });
</script>
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
<script src="https://vk.com/js/api/xd_connection.js?2" type="text/javascript"></script>
<script type="text/javascript">
    VK.init(function(){});
    function autosize() {
        if (typeof VK.callMethod != 'undefined') {
            var bodyHeight = Math.max(document.body.scrollHeight, document.body.offsetHeight, document.body.clientHeight);
            VK.callMethod("resizeWindow", 726, bodyHeight + 50);
        }
    }
    $(document).ready( function(){
        setInterval('autosize()', 1500);
    });
    
//    $(document).ready(function () {
//        VK.init(function() {
//            var bodyHeight = Math.max(document.body.scrollHeight, document.body.offsetHeight, document.body.clientHeight);
//            VK.callMethod("resizeWindow", 726, bodyHeight + 50);
//        }, function() {
//        }, '5.67');
//    });
</script>

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Заглушка из Vkauth.php
    // ************************************************************************************
    // DEBUG! - Убрать после того как ВК начнёт отдавать в GET-параметрах group_id
    if (!isset($get['group_id']))
        $get['group_id'] = 0;

    if ($get['viewer_id'] == 9674014)
        $get['group_id'] = 148845544;
    // DEBUG! - Убрать после того как ВК начнёт отдавать в GET-параметрах group_id
    // ************************************************************************************

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
<img src="images/screenshot02.png" class="img-responsive" width="500" heigh="400" />
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
.button {
    background: #e5ebf1;
    padding: 9px 15px;
    -webkit-tap-highlight-color: transparent;
    border-radius: 2px;
    font-size: 13px;
    color: #55677d;
    letter-spacing: .1px;
    border: none;
    zoom: 1;
    cursor: pointer;
    white-space: nowrap;
    outline: none;
    font-family: -apple-system, BlinkMacSystemFont, Roboto, Open Sans, Helvetica Neue, sans-serif;
    text-decoration: none;
    box-sizing: border-box;
    font-style: normal;
    font-weight: 400
}

.button.smallest {	
    padding: 7px 15px;
    font-weight: 100
}

.button:active, .button:hover {
    text-decoration: none;
    background-color: #dfe6ed
}

.button-primary {
    background: #5e81a8;
    color: #fff;
}

.button-primary:hover {
    color: #fff;
    background: #6888ad
}

.button-primary:active {
    color: #fff;
    background: #5d7ea4
}

.button-primary:visited {
    color: #fff
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/* VK style Button Primary */
.btn-primary {
    background: #5e81a8;
    border: none;
    padding: 6px 15px;
    max-height: 30px;
    color: #fff;
    font-size: 1em;
    font-family: -apple-system, BlinkMacSystemFont, Roboto, Open Sans, Helvetica Neue, sans-serif;
    font-style: normal;
    font-weight: 400;
    letter-spacing: .1px;
    text-decoration: none;
    zoom: 1;
    cursor: pointer;
    white-space: nowrap;
    outline: none;
    box-sizing: border-box;
    border-radius: 2px;
    -webkit-tap-highlight-color: transparent;
}
.btn-primary:hover, 
.btn-primary:focus, 
.btn-primary:active, 
.btn-primary.active, 
.open .dropdown-toggle.btn-primary {
    background: #6888ad;
    color: #fff;
    text-decoration: none;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // DEBUG!
        echo $this->vkdata['is_auth'] ? "auth = TRUE / " : "auth = FALSE / ";
        echo $this->vkdata['is_admin'] ? "is_admin = TRUE / " : "is_admin = FALSE / ";
        echo "gid = ".$this->vkdata['gid']." / ";
        echo "uid = ".$this->vkdata['uid']." / ";

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Анонимная функция для поиска и замены ссылок
        $replace = function($text, $text2html = FALSE) {
            $text = html_entity_decode($text, ENT_QUOTES);
            if ($text2html) {
                $text = preg_replace("/\r?\n/", '<br />', $text);
//                $text = preg_replace("/(https?:\/\/)?([\w\.]+)\.([a-z]{2,6}\.?)(\/[\w\.]*)*\/?/", '<a href="$1" target="_blank">$1</a>', $text);
            }
            return $text;
        };

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * Делает ссылки в текстах над и под формой кликабельными, заключая их в html-тег <a>
     * @param  array   $tabs      Массив вкладок, полученный из метода get
     * @return array   Массив вкладок с кликабельными ссылками в текстах
     */
    public function make_urls($tabs) {

        // Анонимная функция для поиска и замены ссылок
        $replace = function($text) {
            return preg_replace("/(https?:\/\/)?([\w\.]+)\.([a-z]{2,6}\.?)(\/[\w\.]*)*\/?/", '<a href="$1" target="_blank">$1</a>', $text);
        };
        
        foreach ($tabs as $keyparent => $parent) {
            $tabs[$keyparent]['toptext']    = $replace($parent['toptext']);
            $tabs[$keyparent]['bottomtext'] = $replace($parent['bottomtext']);

            if (isset($parent['child'])) {
                foreach ($parent['child'] as $keychild => $child) {
                    $tabs[$keyparent]['child'][$keychild]['toptext']    = $replace($child['toptext']);
                    $tabs[$keyparent]['child'][$keychild]['bottomtext'] = $replace($child['bottomtext']);
                }
            }
        }
        
        $this->is_success = TRUE;
        return $tabs;
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        if (self::$maintenance)                         
            $critical_alerts = array(self::$alerts['maintenance']);
        elseif (($tabs === FALSE) || ($settings === FALSE)) 
            $critical_alerts = array(self::$alerts['something']);
        elseif (!$is_auth) 
            $critical_alerts = array(self::$alerts['something']);
        
        if (count($critical_alerts) > 0) {
            $this->twig->display('trouble', array(
                'alerts' => $critical_alerts
            ));
        }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
{% set someproblem = "Что-то не получилось. Попробуйте обновить страницу и повторите операцию." %}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//        echo "tid = $tid / parenttid = $parenttid / ";
//        echo $this->_is_valid_tids($tid, $parenttid) ? "valid tids / " : "not valid tids / ";
//        echo $this->_is_valid_tab($tid, $parenttid) ? "valid tab / " : "not valid tab / ";
//        echo "\r\n<br>";

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * ОТЛАДОЧНАЯ ЗАГЛУШКА!!!
     */
    public function deltab($tid, $parenttid) {
        $this->load->model('tabs');
        
        echo "tid = $tid / parenttid = $parenttid / ";
        $newtid = $this->tabs->del_tab((int) $tid, (int) $parenttid);
        
        echo "newtid = $newtid";
    }

    /**
     * ОТЛАДОЧНАЯ ЗАГЛУШКА!!!
     * http://app.ukassa/tabedit/dellevel/4/0
     */
    public function dellevel($tid, $parenttid) {
        $this->load->model('tabs');
        
        echo "tid = $tid / parenttid = $parenttid / ";
        $newtid = $this->tabs->del_level((int) $tid, (int) $parenttid);
        
        echo "newtid = $newtid";
    }

    /**
     * ОТЛАДОЧНАЯ ЗАГЛУШКА!!!
     * http://app.ukassa/tabedit/addright/2/0
     */
    public function addright($tid, $parenttid) {
        $this->load->model('tabs');
        
        echo "tid = $tid / parenttid = $parenttid / ";
        $newtid = $this->tabs->add_tab_right((int) $tid, (int) $parenttid);
        
        echo "newtid = $newtid";
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/**
	 * Добавляет/вставляет вкладку слева от $beforetid
	 * @param  string  [$title = FALSE]     Заголовок добавляемой вкладки
	 * @param  integer [$level = 1]         № уровня вложенности вкладок
	 * @param  integer [$parenttid = FALSE] id вкладки-родителя
	 * @param  integer [$beforetid = FALSE] id вкладки, перед которой будет вставлена новая вкладка
	 * @return integer/FALSE id вкладки при успехе или FALSE
	 */
	public function add_tab_left($title = FALSE, $level = 1, $parenttid = FALSE, $beforetid = FALSE) {

        // Валидация входных аргументов
        if (! ( ($level === self::validate_level($level)) &&
                (($parenttid === FALSE) OR ($parenttid === self::validate_parenttid($parenttid))) &&
                (($beforetid === FALSE) OR ($beforetid === self::validate_tid($beforetid)))
              )
            )
            return FALSE;

        // Невалидный title будет преобразован в "Новая вкладка"
        $this->load->library('validate', self::$ukonfig);
        $title = $this->validate->text($title, self::$title_minlen, self::$title_maxlen, self::$title_default);
        
        // Дополнительная часть в запросе WHERE для вкладки 2-го уровня
        $qparenttid = (($level === 2) && ($parenttid !== FALSE)) ? ['parenttid' => $parenttid] : [];

        // SELECT ikey,tid FROM tabs WHERE (gid = self::$gid) AND ($level = $level)
        // SELECT ikey,tid FROM tabs WHERE (gid = self::$gid) AND ($level = $level) AND (parenttid = $parenttid)
        $selresult = $this->db->select('ikey,tid')
            ->where(array('gid' => self::$gid, 'level' => $level) + $qparenttid)
            ->limit(self::$qlimit)
            ->get(self::$tbltabs);

        $items = $selresult->result_array();       // Массив, включающий ikey,tid
        $tids = array_column($items, 'tid');       // Линейный (скалярный) массив со значениями tid
        sort($tids);

        // Проверяем, достигнуто ли допустимое число вкладок
        if (count($tids) >= self::$maxtabs)
            return FALSE;

        // Если $beforetid опущен, то добавляем вкладку как крайнюю слева (слева от первой)
        $beforetid = ($beforetid === FALSE) ? 1 : $beforetid;

        // Добавляем вкладку слева от $beforetid
        // if (($beforetid === self::validate_tid($beforetid)) AND in_array($beforetid, $tids, TRUE)) {
        if (in_array($beforetid, $tids, TRUE)) {

            $updresult = $insresult = FALSE;

            // Получаем ключи записей БД для среза массива с теми вкладками, которые нужно сдвинуть вправо
            $ikeys2update = [];
            foreach ($items as $item) {
                if ($item['tid'] >= $beforetid)
                    $ikeys2update[] = $item['ikey'];
            }

            $this->db->trans_start();

                // UPDATE tabs SET tid = tid+1 WHERE ikey IN (1,2,3);
                $updresult = $this->db->set('tid', 'tid+1')
                    ->where_in('ikey', $ikeys2update)
                    ->update(self::$tbltabs);

                // Если UPDATE завершился корректно - вставляем вкладку слева от $beforetid
                if ($updresult) {
                    // INSERT INTO tabs (gid, tid, level, title, parenttid)  VALUES (self::$gid, $newtid, $level, $title, $qparenttid)
                    $insresult = $this->db->insert(self::$tbltabs,
                        array('gid' => self::$gid, 'tid' => $beforetid, 'level' => $level, 'title' => $title) + $qparenttid, TRUE);
                }
            
            $this->db->trans_complete();

            return ($updresult && $insresult) ? $beforetid : FALSE;
        }

        // Невалидный $beforetid или не удалось выполнить UPDATE
        return FALSE;
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    $resp['title'] = $this->tabs->get_title($post['tid'], $post['parenttid']);
    
    /**
     * Возвращает текстовый заголовок вкладки или дефолтное значение (если невалидные агрументы или неудачный запрос к БД)
     * @param  integer  tid        id вкладки
     * @param  integer  $parenttid id родительской вкладки
     * @return string Заголовок вкладки
     */
    public function get_title($tid, $parenttid) {
        
        $this->load->library('validate', self::$ukonfig);
        
        if ( ($tid != $this->validate->tid($tid)) || ($parenttid != $this->validate->parenttid($parenttid)) )
            return self::$title_default;
        
        // SELECT * FROM tabs WHERE (gid = self::$gid) AND ('tid' = $tid) AND ('parenttid' = $parenttid)
        $selresult = $this->db->get_where(self::$tbltabs, array('gid' => self::$gid, 'tid' => $tid, 'parenttid' => $parenttid));
        $row = (array) $selresult->row();
        
        return ($this->query_success = isset($row['title'])) ? $row['title'] : self::$title_default;
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Для всех НЕАКТИВНЫХ вторых уровней делаем активной 1-ю вкладку.
// Для АКТИВНОГО второго уровня делаем активной вкладку с совпавшими tid и parenttid.

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$(document).ready(function () {

    $("form.formtabedit").submit(function() {
		var form = $(this);
        
        $.ajax({
            type: 'POST',
            url: '/tabedit/save',
            dataType: 'json',
            data: form.serialize(),

            success: function(resp) {
                if ('response' in resp) {
//                    if ('tabeditbody' in resp) {
//                        $("#tabeditbody").html(resp['tabeditbody']);
//                    }
                    alert(resp['text']);
                } else {
                    alert(resp['text']);    // "Красное" сообщение
                }
            },
            error: function(resp) {
                alert("{{someproblem}}");
            }
        });

        return false;
	});

    $("form.formtabtitle").submit(function() {
		var form = $(this);
        var mixid = form.find('input[name="tid"]').val() + "-" + form.find('input[name="parenttid"]').val();
        
        $.ajax({
            type: 'POST',
            url: '/tabedit/savetitle',
            dataType: 'json',
            data: form.serialize(),

            success: function(resp) {
                if ('response' in resp) {
                    $("#tabtitle" + mixid).text(resp['title']);
//                    if ('tabeditbody' in resp) {
//                        $("#tabeditbody").html(resp['tabeditbody']);
//                    }
                    alert(resp['text']);
                } else {
                    alert(resp['text']);    // "Красное" сообщение
                }
            },
            error: function(resp) {
                alert("{{someproblem}}");
            }
        });

        return false;
	});

    $(".tabcontrol").click(function(event) {
        event.preventDefault();
        var send = {};
        send['cmd'] = $(this).attr('href');
        
//        var tmp = cmd.split('_');
//        var titleid = "title" + tmp[1];

        $.ajax({
            type: 'GET',
            url: '/tabedit/control',
            dataType: 'json',
            data: send,

            success: function(resp) {
                if ('response' in resp) {
                    alert(resp['text']);
                } else {
                    alert(resp['text']);
                }
            },
            error: function(resp) {
                alert("{{someproblem}}");
            }
        });

        return false;
	});

});
</script>

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
<script src="/assets/js/jquery.form.min.js"></script>
    
        var SendTabTitleParams = {
        type: 'POST',
        url: '/tabedit/savetitle',
        dataType: 'json',
//        data: form.serialize(),

        success: function(resp) {
            if ('response' in resp) {
//                $("#tabtitle" + mixid).text(resp['title']);
//                    if ('tabeditbody' in resp) {
//                        $("#tabeditbody").html(resp['tabeditbody']);
//                    }
                alert(resp['text']);
            } else {
                alert(resp['text']);    // "Красное" сообщение
            }
        },
        error: function(resp) {
            alert("{{someproblem}}");
        }
    }
    function SendTabTitle(mixid) {
        $("#form_tabtitle" + mixid).ajaxSubmit(SendTabTitleParams);
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    <div class="form-group">
        <label class="control-label" for="title{{mixid}}">Заголовок этой вкладки</label>
        <input type="text" class="form-control" name="title" id="title{{mixid}}" value="{{thistab.title}}" minlength="{{tabparams.title_minlen}}" maxlength="{{tabparams.title_maxlen}}" data-error="Введите заголовок вкладки (до {{tabparams.title_maxlen}} знаков)" required>
        <div class="help-block with-errors"></div>
    </div>
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        <a id="tabtitle{{mixid}}" href="#tab{{mixid}}" data-toggle="tab">{# {thistab.title} #}

                            <form role="form" id="form_tabtitle{{mixid}}" data-toggle="validator">
                                <div class="input-group input-group-sm">
                                    <input type="hidden" class="sr-only" name="{{csrf.name}}" value="{{csrf.hash}}">
                                    <input type="hidden" class="sr-only" name="tid" value="{{thistab.tid}}">
                                    <input type="hidden" class="sr-only" name="parenttid" value="{{thistab.parenttid}}">
                                    <input type="text" class="form-control" name="title" id="title{{mixid}}" value="{{thistab.title}}" minlength="{{tabparams.title_minlen}}" maxlength="{{tabparams.title_maxlen}}" required>
                                    <span class="input-group-btn">
                                        <button type="submit" class="btn btn-primary">Ок</button>
                                    </span>
                                </div>
                            </form>
                        
                        </a>
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            <ul class="nav nav-tabs">
                {% for parent in tabs %}
                    <li class="{% if parent.tid == 1 %}active{% endif %}">
                        <a style="display:inline-block;" href="#tab{{parent.tid}}" data-toggle="tab">{{parent.title}}</a>
                        <div class="btn-group">
                            <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Действие</a></li>
                                <li><a href="#">Другое действие</a></li>
                                <li><a href="#">Что-то иное</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Отдельная ссылка</a></li>
                            </ul>
                        </div>
                    </li>
                {% endfor %}
            </ul>

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    {% if settings.yanote == 'custom' %}    {% set yanote = "Укажет плательщик" %}
    {% elseif settings.yanote == 'off' %}   {% set yanote = "Ничего, пустое поле" %}
    {% elseif settings.yanote == 'title' %} {% set yanote = thistab.mixtitle %}
    {% endif %}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * Принудительно валидирует заголовок вкладки: фильтрует мусор и html-теги, обрезает длину до допустимой, 
     *   присваивает дефолтное значение, если строкая невалидная или пустая
     * @param  string $title Входной текст
     * @return string Валидный заголовок
     */
    protected static function validate_title($title) {
        if (is_string($title)) {
            $title = remove_invisible_characters(htmlentities($title, ENT_QUOTES, self::$mbcharset), TRUE);
            $title = mb_substr($title, 0, self::$title_maxlen, self::$mbcharset);
            $title = trim($title) == '' ? self::$title_default : $title;
        }
        else
            $title = self::$title_default;

        return $title;
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// @url http://stackoverflow.com/questions/3408482/codeigniter-set-global-variable#3408482

class Getgid extends CI_Controller {

public $gid = NULL;

    function __construct() {
        $this->gid = $this->session->userdata('gid');
    }

    function getGID() {
        return $this->gid;
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//            'toptext'   => (isset($post['toptext'])    ? $this->validate->toptext($post['toptext'],       $settings['ttmaxlen']) : NULL),
//            'bottomtext'=> (isset($post['bottomtext']) ? $this->validate->bottomtext($post['bottomtext'], $settings['btmaxlen']) : NULL),
        
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // ВНИМАНИЕ! Query Builder НЕПРАВИЛЬНО маскирует конструкцию '1 - val' - это не работает в кавычках!
        $this->db->query("UPDATE ".self::$tblsettings." SET `appon` = 1 - `appon` WHERE `gid` = {$this->gid}");
        $updresult = ($this->db->affected_rows() > 0);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    $("#form_appon").submit(function () {
		var form = $(this);

        $.ajax({
            type: 'POST',
            url: '/setup/form_appon',
            dataType: 'json',
            data: form.serialize(),

            success: function(resp) {
                if (('response' in resp) && resp['response']['on']) {
                    $('#form_appon_on').removeClass('sr-only');
                    $('#form_appon_off').addClass('sr-only');
                    alert(resp['response']['text']);
//                    $('.message_appon_on').fadeIn(500,function(){
//                        setTimeout("$('.message_appon_on').fadeOut(500);",poptime_ok);    
//                    });
                } else if (('response' in resp) && resp['response']['off']) {
                    $('#form_appon_off').removeClass('sr-only');
                    $('#form_appon_on').addClass('sr-only');
                    alert(resp['response']['text']);
//                    $('.message_appon_off').fadeIn(500,function(){
//                        setTimeout("$('.message_appon_off').fadeOut(500);",poptime_ok);    
//                    });
                } else {
                    alert(resp['error']['text']);
//                    $('.message_err').fadeIn(500,function(){
//                        setTimeout("$('.message_err').fadeOut(500);",poptime_err);    
//                    });
                }
            },
            error: function(resp) {
                alert("Что-то не получилось. Попробуйте обновить страницу и повторите операцию.");
//                $('.message_err').fadeIn(500,function(){
//                    setTimeout("$('.message_err').fadeOut(500);",poptime_err);    
//                });
            }
        });
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//        $this->db->where('gid', $this->gid);
//        $updresult = $this->db->update(self::$tblsettings, $setdata);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $formrules = array(
            array(  'field' => 'yandexpurse',
                    'label' => 'yandexpurse',
                    'rules' => "trim|min_length[" .self::$yapurse['minlen']. "]|max_length[" .self::$yapurse['maxlen']. "]",
            ),
            array(  'field' => 'yandexbutton',
                    'label' => 'yandexbutton',
                    'rules' => "trim|required|min_length[" .self::$yabutton['minlen']. "]|max_length[" .self::$yabutton['maxlen']. "]",
            ),
            array(  'field' => 'toptext',
                    'label' => 'toptext',
                    'rules' => "trim|max_length[" .$settings['ttmaxlen']. "]",
            ),
            array(  'field' => 'bottomtext',
                    'label' => 'bottomtext',
                    'rules' => "trim|max_length[" .$settings['btmaxlen']. "]",
            ),
            array(  'field' => 'tton',
                    'label' => 'tton',
                    'rules' => '',
            ),
            array(  'field' => 'bton',
                    'label' => 'bton',
                    'rules' => '',
            ),
            array(  'field' => 'yanote',
                    'label' => 'yanote',
                    'rules' => "required|in_list[off,title,custom]",
            ),
        );

        $this->load->library('form_validation');
        $this->form_validation->set_rules($formrules);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $updresult = $getresult = FALSE;
        $appon = (((int) $this->input->post('appon')) > 0) ? 1 : 0;     // Валидация
        $appon = 1 - $appon;                                            // Изменяем состояние на обратное

        if ($this->form_validation->run()) {
            $updresult = $this->settings->update_appon($appon);
            
            // Получаем текстовую строку состояния приложения: on / off / error
            $getresult = $this->settings->get_appon();
        }
        
        // Отправляем строку ответа в ajax-скрипт
        echo $updresult ? $getresult : 'error';


//		$this->config->load('ukconfig');
//        $params = array(
//            'tabs'      => $data,
//            'yabutton'  => $this->config->item('yabutton_text_default'),
//            'toptext'   => $this->config->item('toptext_default'),
//            'bottomtext'=> $this->config->item('bottomtext_default'),
//            'tton'      => $this->config->item('toptext_default_on'),
//            'bton'      => $this->config->item('bottomtext_default_on'),
//        );

//        $selresult = $this->db->select('tid,parenttid,level,title,toptext,bottomtext,sum,paylock')
//            ->get($this->tbltabs)
//            ->where(array('gid' => $this->gid))
//            ->limit($this->maxlimit);

    protected static function validate_string($text) {
        return remove_invisible_characters(htmlentities($text, ENT_QUOTES|ENT_SUBSTITUTE|ENT_HTML5), TRUE);
    }

        $qparenttid = (($level === 2) && ($parenttid === self::validate_tid($parenttid)))
            ? array('parenttid' => $parenttid)
            : array();

// https://toster.ru/q/98121
// http://stackoverflow.com/questions/11664684/how-to-bulk-update-mysql-data-with-one-query

            // Сортируем массив по убыванию значения tid
            $cmp_by_tids_desc = function($item1, $item2) {
                $diff = $item2['tid'] - $item1['tid'];
                return $diff == 0 ? 0 : ($diff > 0 ? 1 : -1);
            };

            usort($items_tids_update, $cmp_by_tids_desc);

            // Обновляем записи вкладок в БД, инкрементируя каджый tid (сдвигаем срез вкладок вправо)
            foreach ($itemtids_update as $item) {
                // UPDATE tabs SET tid = tid+1 WHERE ikey = $item['ikey']
                $result = $this->db->update($this->tbltabs)
                    ->set('tid', 'tid+1', FALSE)
                    ->where('ikey', $item['ikey']);
            }

        // Валидируем параметр $level
        $level = (int) $level;
        if (($level < 1) || ($level > self::$ukconfig['tab_max_levels']))
            return FALSE;

    $aftertid = self::validate_tid($paste_after_tid);
    if ($level !== self::validate_level($level))
    $result = $this->db->query($query);

    $query = $this->db->query("ВАШ ЗАПРОС");

    foreach ($query->result_array() as $row)
    {
            echo $row['title'];
            echo $row['name'];
            echo $row['body'];
    }
        return is_string($title)
            ? mb_substr(self::validate_string($title), 0, self::$ukconfig['tab_max_title_len'], self::$config['charset'])
            : self::$ukconfig['tab_title_default'];
